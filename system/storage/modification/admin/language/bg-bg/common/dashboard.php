<?php

        $_['text_warning_facebook_sync']           = 'Warning: Encountered problem updating product(s) to Facebook. Please retry again. Check the error log file for more details';
        $_['text_warning_facebook_delete']           = 'Warning: Encountered problem deleting product(s) to Facebook. Check the error log file for more details.';
        $_['text_warning_facebook_access_token_problem'] = 'Warning: There is a problem with your Facebook access token. Please go to Facebook Ads Extension to refresh the access token.';
        $_['text_warning_facebook_fae_install_problem'] = 'Warning: There is a problem with your Facebook Ads Extension installation. Please go to Facebook Ads Extension to view more details.';
      
/*
 * OpenCart 2.3.0.2 Bulgarian translation
 * Author: Veselin Totev (veselin.totev@gmail.com)
 * License: Free to use
 * Donate:
 *      PayPal: veselin.totev@gmail.com
 *      Bitcoin: 1P3Zk93fhKURWT1rXiYxiLGuJ59oETxuTi
 */
// Heading
$_['heading_title']                = 'Табло';

// Error
$_['error_install']                = 'Внимание: Инсталационната папка все още съществува и трябва да бъде изтрита от съображения за сигурност!';