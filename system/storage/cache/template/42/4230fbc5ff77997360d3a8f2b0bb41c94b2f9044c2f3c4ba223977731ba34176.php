<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/account/voucher.twig */
class __TwigTemplate_610efdedc8a2d50c1762bfea9d84e4a6bf8d9fd221b2adff8255f77c5821e02c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $this->loadTemplate("kofi/template/new_elements/wrapper_top.twig", "kofi/template/account/voucher.twig", 2)->display($context);
        // line 3
        echo "
<p>";
        // line 4
        echo ($context["text_description"] ?? null);
        echo "</p>
<form action=\"";
        // line 5
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">
  <div class=\"form-group required\">
    <label class=\"col-sm-2 control-label\" for=\"input-to-name\">";
        // line 7
        echo ($context["entry_to_name"] ?? null);
        echo "</label>
    <div class=\"col-sm-10\">
      <input type=\"text\" name=\"to_name\" value=\"";
        // line 9
        echo ($context["to_name"] ?? null);
        echo "\" id=\"input-to-name\" class=\"form-control\" />
      ";
        // line 10
        if (($context["error_to_name"] ?? null)) {
            // line 11
            echo "      <div class=\"text-danger\">";
            echo ($context["error_to_name"] ?? null);
            echo "</div>
      ";
        }
        // line 13
        echo "    </div>
  </div>
  <div class=\"form-group required\">
    <label class=\"col-sm-2 control-label\" for=\"input-to-email\">";
        // line 16
        echo ($context["entry_to_email"] ?? null);
        echo "</label>
    <div class=\"col-sm-10\">
      <input type=\"text\" name=\"to_email\" value=\"";
        // line 18
        echo ($context["to_email"] ?? null);
        echo "\" id=\"input-to-email\" class=\"form-control\" />
      ";
        // line 19
        if (($context["error_to_email"] ?? null)) {
            // line 20
            echo "      <div class=\"text-danger\">";
            echo ($context["error_to_email"] ?? null);
            echo "</div>
      ";
        }
        // line 22
        echo "    </div>
  </div>
  <div class=\"form-group required\">
    <label class=\"col-sm-2 control-label\" for=\"input-from-name\">";
        // line 25
        echo ($context["entry_from_name"] ?? null);
        echo "</label>
    <div class=\"col-sm-10\">
      <input type=\"text\" name=\"from_name\" value=\"";
        // line 27
        echo ($context["from_name"] ?? null);
        echo "\" id=\"input-from-name\" class=\"form-control\" />
      ";
        // line 28
        if (($context["error_from_name"] ?? null)) {
            // line 29
            echo "      <div class=\"text-danger\">";
            echo ($context["error_from_name"] ?? null);
            echo "</div>
      ";
        }
        // line 31
        echo "    </div>
  </div>
  <div class=\"form-group required\">
    <label class=\"col-sm-2 control-label\" for=\"input-from-email\">";
        // line 34
        echo ($context["entry_from_email"] ?? null);
        echo "</label>
    <div class=\"col-sm-10\">
      <input type=\"text\" name=\"from_email\" value=\"";
        // line 36
        echo ($context["from_email"] ?? null);
        echo "\" id=\"input-from-email\" class=\"form-control\" />
      ";
        // line 37
        if (($context["error_from_email"] ?? null)) {
            // line 38
            echo "      <div class=\"text-danger\">";
            echo ($context["error_from_email"] ?? null);
            echo "</div>
      ";
        }
        // line 40
        echo "    </div>
  </div>
  <div class=\"form-group required\">
    <label class=\"col-sm-2 control-label\">";
        // line 43
        echo ($context["entry_theme"] ?? null);
        echo "</label>
    <div class=\"col-sm-10\">
     ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["voucher_themes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["voucher_theme"]) {
            // line 46
            echo "      ";
            if ((twig_get_attribute($this->env, $this->source, $context["voucher_theme"], "voucher_theme_id", [], "any", false, false, false, 46) == ($context["voucher_theme_id"] ?? null))) {
                // line 47
                echo "      <div class=\"radio\">
        <label>
          <input type=\"radio\" name=\"voucher_theme_id\" value=\"";
                // line 49
                echo twig_get_attribute($this->env, $this->source, $context["voucher_theme"], "voucher_theme_id", [], "any", false, false, false, 49);
                echo "\" checked=\"checked\" />
          ";
                // line 50
                echo twig_get_attribute($this->env, $this->source, $context["voucher_theme"], "name", [], "any", false, false, false, 50);
                echo "</label>
      </div>
      ";
            } else {
                // line 53
                echo "      <div class=\"radio\">
        <label>
          <input type=\"radio\" name=\"voucher_theme_id\" value=\"";
                // line 55
                echo twig_get_attribute($this->env, $this->source, $context["voucher_theme"], "voucher_theme_id", [], "any", false, false, false, 55);
                echo "\" />
          ";
                // line 56
                echo twig_get_attribute($this->env, $this->source, $context["voucher_theme"], "name", [], "any", false, false, false, 56);
                echo "</label>
      </div>
      ";
            }
            // line 59
            echo "      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher_theme'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "      ";
        if (($context["error_theme"] ?? null)) {
            // line 61
            echo "      <div class=\"text-danger\">";
            echo ($context["error_theme"] ?? null);
            echo "</div>
      ";
        }
        // line 63
        echo "    </div>
  </div>
  <div class=\"form-group\">
    <label class=\"col-sm-2 control-label\" for=\"input-message\"><span data-toggle=\"tooltip\" title=\"";
        // line 66
        echo ($context["help_message"] ?? null);
        echo "\">";
        echo ($context["entry_message"] ?? null);
        echo "</span></label>
    <div class=\"col-sm-10\">
      <textarea name=\"message\" cols=\"40\" rows=\"5\" id=\"input-message\" class=\"form-control\">";
        // line 68
        echo ($context["message"] ?? null);
        echo "</textarea>
    </div>
  </div>
  <div class=\"form-group\">
    <label class=\"col-sm-2 control-label\" for=\"input-amount\"><span data-toggle=\"tooltip\" title=\"";
        // line 72
        echo ($context["help_amount"] ?? null);
        echo "\">";
        echo ($context["entry_amount"] ?? null);
        echo "</span></label>
    <div class=\"col-sm-10\">
     
      <select class=\"form-control\" id=\"input-amount\" name=\"amount\">
          <option value=\"50\">50</option>
          <option value=\"100\">100</option>
      </select>
      ";
        // line 79
        if (($context["error_amount"] ?? null)) {
            // line 80
            echo "      <div class=\"text-danger\">";
            echo ($context["error_amount"] ?? null);
            echo "</div>
      ";
        }
        // line 82
        echo "    </div>
  </div>
  <div class=\"buttons clearfix\">
    <div class=\"pull-right\"> ";
        // line 85
        echo ($context["text_agree"] ?? null);
        echo "
      ";
        // line 86
        if (($context["agree"] ?? null)) {
            // line 87
            echo "      <input type=\"checkbox\" name=\"agree\" value=\"1\" checked=\"checked\" />
      ";
        } else {
            // line 89
            echo "      <input type=\"checkbox\" name=\"agree\" value=\"1\" />
      ";
        }
        // line 91
        echo "      &nbsp;
      <input type=\"submit\" value=\"";
        // line 92
        echo ($context["button_continue"] ?? null);
        echo "\" class=\"btn btn-primary\" />
    </div>
  </div>
</form>
      
";
        // line 97
        $this->loadTemplate("kofi/template/new_elements/wrapper_bottom.twig", "kofi/template/account/voucher.twig", 97)->display($context);
        // line 98
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "kofi/template/account/voucher.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  269 => 98,  267 => 97,  259 => 92,  256 => 91,  252 => 89,  248 => 87,  246 => 86,  242 => 85,  237 => 82,  231 => 80,  229 => 79,  217 => 72,  210 => 68,  203 => 66,  198 => 63,  192 => 61,  189 => 60,  183 => 59,  177 => 56,  173 => 55,  169 => 53,  163 => 50,  159 => 49,  155 => 47,  152 => 46,  148 => 45,  143 => 43,  138 => 40,  132 => 38,  130 => 37,  126 => 36,  121 => 34,  116 => 31,  110 => 29,  108 => 28,  104 => 27,  99 => 25,  94 => 22,  88 => 20,  86 => 19,  82 => 18,  77 => 16,  72 => 13,  66 => 11,  64 => 10,  60 => 9,  55 => 7,  50 => 5,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/account/voucher.twig", "");
    }
}
