<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/account/login.twig */
class __TwigTemplate_19a98d14ec9f6ca9eb6bea140c863e50670cc18a8479566b4795098b590cd2a5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $this->loadTemplate("kofi/template/new_elements/wrapper_top.twig", "kofi/template/account/login.twig", 2)->display($context);
        // line 3
        echo "
<div class=\"row\">
  <div class=\"col-sm-6\">
    <div class=\"well\">
      <h2>";
        // line 7
        echo ($context["text_new_customer"] ?? null);
        echo "</h2>
      <p><strong>";
        // line 8
        echo ($context["text_register"] ?? null);
        echo "</strong></p>
      <p style=\"padding-bottom: 10px\">";
        // line 9
        echo ($context["text_register_account"] ?? null);
        echo "</p>
      <a href=\"";
        // line 10
        echo ($context["register"] ?? null);
        echo "\" class=\"btn btn-primary\">";
        echo ($context["button_continue"] ?? null);
        echo "</a></div>
  </div>
  <div class=\"col-sm-6\">
    <div class=\"well\">
      <h2>";
        // line 14
        echo ($context["text_returning_customer"] ?? null);
        echo "</h2>
      <p><strong>";
        // line 15
        echo ($context["text_i_am_returning_customer"] ?? null);
        echo "</strong></p>
      <form action=\"";
        // line 16
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
        <div class=\"form-group\">
          <label class=\"control-label\" for=\"input-email\">";
        // line 18
        echo ($context["entry_email"] ?? null);
        echo "</label>
          <input type=\"text\" name=\"email\" value=\"";
        // line 19
        echo ($context["email"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_email"] ?? null);
        echo "\" id=\"input-email\" class=\"form-control\" />
        </div>
        <div class=\"form-group\" style=\"padding-bottom: 10px\">
          <label class=\"control-label\" for=\"input-password\">";
        // line 22
        echo ($context["entry_password"] ?? null);
        echo "</label>
          <input type=\"password\" name=\"password\" value=\"";
        // line 23
        echo ($context["password"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_password"] ?? null);
        echo "\" id=\"input-password\" class=\"form-control\" />
          <a href=\"";
        // line 24
        echo ($context["forgotten"] ?? null);
        echo "\">";
        echo ($context["text_forgotten"] ?? null);
        echo "</a></div>
        <input type=\"submit\" value=\"";
        // line 25
        echo ($context["button_login"] ?? null);
        echo "\" class=\"btn btn-primary\" />
        ";
        // line 26
        if (($context["redirect"] ?? null)) {
            echo " 
        <input type=\"hidden\" name=\"redirect\" value=\"";
            // line 27
            echo ($context["redirect"] ?? null);
            echo "\" />
        ";
        }
        // line 28
        echo " 
      </form>
    </div>
  </div>
</div>

";
        // line 34
        $this->loadTemplate("kofi/template/new_elements/wrapper_bottom.twig", "kofi/template/account/login.twig", 34)->display($context);
        // line 35
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "kofi/template/account/login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 35,  132 => 34,  124 => 28,  119 => 27,  115 => 26,  111 => 25,  105 => 24,  99 => 23,  95 => 22,  87 => 19,  83 => 18,  78 => 16,  74 => 15,  70 => 14,  61 => 10,  57 => 9,  53 => 8,  49 => 7,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/account/login.twig", "");
    }
}
