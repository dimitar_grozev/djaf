<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/extension/module/megamenu.twig */
class __TwigTemplate_46ba9dc10bfbde49400eae383199d82764a37b49d6dbbccd5103062d53ceadfe extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
\t";
            // line 2
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 2);
            // line 3
            echo "\t";
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 3);
            // line 4
            echo "\t";
            $context["page_direction"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_direction"], "method", false, false, false, 4);
            echo " ";
            $context["language_id"] = twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 4);
            // line 5
            echo "\t";
            $context["id"] = (twig_random($this->env, 5000) * twig_random($this->env, 500000));
            echo " 

\t<div id=\"megamenu_";
            // line 7
            echo ($context["id"] ?? null);
            echo "\" class=\"container-megamenu ";
            if (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["ustawienia"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["full_width"] ?? null) : null) != "1")) {
                echo " ";
                echo "container";
                echo " ";
            }
            echo " ";
            if (((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["ustawienia"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["orientation"] ?? null) : null) == 1)) {
                echo " ";
                echo "vertical";
                echo " ";
            } elseif (((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["ustawienia"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["orientation"] ?? null) : null) == 2)) {
                echo " ";
                echo "horizontal horizontal-type-2";
                echo " ";
            } elseif (((($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["ustawienia"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["orientation"] ?? null) : null) == 3)) {
                echo " ";
                echo "horizontal horizontal-type-3";
                echo " ";
            } else {
                echo " ";
                echo "horizontal";
                echo " ";
            }
            echo " ";
            if (((($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["ustawienia"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["display_on_mobile"] ?? null) : null) == 1)) {
                echo " ";
                echo " mobile-disabled";
                echo " ";
            }
            echo "\">
\t\t";
            // line 8
            if (((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["ustawienia"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["orientation"] ?? null) : null) == 1)) {
                echo " 
\t\t<div id=\"menuHeading\">
\t\t\t<div class=\"megamenuToogle-wrapper\">
\t\t\t\t<div class=\"megamenuToogle-pattern\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t";
                // line 13
                echo ($context["navigation_text"] ?? null);
                echo " 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t";
            } else {
                // line 18
                echo " 
\t\t<div class=\"megaMenuToggle\">
\t\t\t<div class=\"megamenuToogle-wrapper\">
\t\t\t\t<div class=\"megamenuToogle-pattern\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div><span></span><span></span><span></span></div>
\t\t\t\t\t\t";
                // line 24
                echo ($context["navigation_text"] ?? null);
                echo " 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t";
            }
            // line 29
            echo " 
\t\t<div class=\"megamenu-wrapper\">
\t\t\t<div class=\"megamenu-pattern\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<ul class=\"megamenu ";
            // line 33
            if (((($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["ustawienia"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["animation"] ?? null) : null) != "")) {
                echo " ";
                echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["ustawienia"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["animation"] ?? null) : null);
                echo " ";
            }
            echo "\">
\t\t\t\t\t\t";
            // line 34
            if ((((($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["ustawienia"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["home_item"] ?? null) : null) == "icon") || ((($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["ustawienia"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["home_item"] ?? null) : null) == "text"))) {
                echo " 
\t\t\t\t\t\t<li class=\"home\"><a href=\"";
                // line 35
                echo ($context["home"] ?? null);
                echo "\">";
                if (((($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["ustawienia"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["home_item"] ?? null) : null) == "icon")) {
                    echo "<i class=\"fa fa-home\"></i>";
                } else {
                    echo " ";
                    echo (("<span><strong>" . ($context["home_text"] ?? null)) . "</strong></span>");
                    echo " ";
                }
                echo "</a></li>
\t\t\t\t\t\t";
            }
            // line 36
            echo " 
\t\t\t\t\t\t";
            // line 37
            if (((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["ustawienia"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["search_bar"] ?? null) : null) == 1)) {
                echo " 
\t\t\t\t\t\t<li class=\"search pull-right\">
\t\t\t\t\t\t\t<!-- Search -->
\t\t\t\t\t\t\t<div class=\"search_form\">
\t\t\t\t\t\t\t\t<div class=\"button-search2\"></div>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"input-block-level search-query\" name=\"search2\" placeholder=\"";
                // line 42
                echo ($context["text_search"] ?? null);
                echo "\" id=\"search_query";
                echo ($context["id"] ?? null);
                echo "\" value=\"";
                echo ($context["search"] ?? null);
                echo "\" />
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
                // line 44
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quick_search_autosuggest"], "method", false, false, false, 44) != "0")) {
                    echo " 
\t\t\t\t\t\t\t\t<div id=\"autocomplete-results";
                    // line 45
                    echo ($context["id"] ?? null);
                    echo "\" class=\"autocomplete-results\"></div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\$(document).ready(function() {
\t\t\t\t\t\t\t\t\t\$('#search_query";
                    // line 49
                    echo ($context["id"] ?? null);
                    echo "').autocomplete({
\t\t\t\t\t\t\t\t\t\tdelay: 0,
\t\t\t\t\t\t\t\t\t\tappendTo: \"#autocomplete-results";
                    // line 51
                    echo ($context["id"] ?? null);
                    echo "\",
\t\t\t\t\t\t\t\t\t\tsource: function(request, response) {\t\t
\t\t\t\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t\t\t\t\turl: 'index.php?route=search/autocomplete&filter_name=' +  encodeURIComponent(request.term),
\t\t\t\t\t\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\t\t\t\t\t\tsuccess: function(json) {
\t\t\t\t\t\t\t\t\t\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\t\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tlabel: item.name,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tvalue: item.product_id,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\thref: item.href,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tthumb: item.thumb,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tdesc: item.desc,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tprice: item.price
\t\t\t\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t\t}));
\t\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\tselect: function(event, ui) {
\t\t\t\t\t\t\t\t\t\t\tdocument.location.href = ui.item.href;
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\tfocus: function(event, ui) {
\t\t\t\t\t\t\t\t\t      \treturn false;
\t\t\t\t\t\t\t\t\t   \t},
\t\t\t\t\t\t\t\t\t   \tminLength: 2
\t\t\t\t\t\t\t\t\t})
\t\t\t\t\t\t\t\t\t.data( \"ui-autocomplete\" )._renderItem = function( ul, item ) {
\t\t\t\t\t\t\t\t\t  return \$( \"<li>\" )
\t\t\t\t\t\t\t\t\t    .append( \"<a>\" + item.label + \"</a>\" )
\t\t\t\t\t\t\t\t\t    .appendTo( ul );
\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t";
                }
                // line 87
                echo " 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
            }
            // line 90
            echo " 
\t\t\t\t\t\t";
            // line 91
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["menu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
                // line 93
                $context["class"] = "";
                // line 94
                echo "\t\t\t\t\t\t\t\t";
                $context["class_link"] = "clearfix";
                // line 95
                echo "\t\t\t\t\t\t\t\t";
                $context["title"] = twig_constant("false");
                // line 96
                echo "\t\t\t\t\t\t\t\t";
                $context["target"] = twig_constant("false");
                // line 97
                echo "\t\t\t\t\t\t\t\t";
                if (((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = $context["row"]) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["description"] ?? null) : null) != "")) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 98
                    $context["class_link"] = (($context["class_links"] ?? null) . " description");
                    echo " 
\t\t\t\t\t\t\t\t";
                }
                // line 100
                echo "\t\t\t\t\t\t\t\t";
                if ((twig_test_iterable((($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["row"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["submenu"] ?? null) : null)) &&  !twig_test_empty((($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = $context["row"]) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["submenu"] ?? null) : null)))) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 101
                    $context["class"] = (($context["class"] ?? null) . " with-sub-menu");
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 102
                    if (((($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["row"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["submenu_type"] ?? null) : null) == 1)) {
                        echo " 
\t\t\t\t\t\t\t\t\t\t";
                        // line 103
                        $context["class"] = (($context["class"] ?? null) . " click");
                        echo " 
\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 104
                        echo " 
\t\t\t\t\t\t\t\t\t\t";
                        // line 105
                        $context["class"] = (($context["class"] ?? null) . " hover");
                        echo " 
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 106
                    echo " 
\t\t\t\t\t\t\t\t";
                }
                // line 108
                echo "\t\t\t\t\t\t\t\t";
                if (((($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["row"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["display_on_mobile"] ?? null) : null) == 1)) {
                    // line 109
                    echo "\t\t\t\t\t\t\t\t\t";
                    $context["class"] = (($context["class"] ?? null) . " mobile-disabled");
                    echo " 
\t\t\t\t\t\t\t\t";
                }
                // line 111
                echo "\t\t\t\t\t\t\t\t";
                if (((($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = $context["row"]) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["position"] ?? null) : null) == 1)) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 112
                    $context["class"] = (($context["class"] ?? null) . " pull-right");
                    echo " 
\t\t\t\t\t\t\t\t";
                }
                // line 114
                echo "\t\t\t\t\t\t\t\t";
                if (((($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = $context["row"]) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["submenu_type"] ?? null) : null) == 2)) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 115
                    $context["title"] = "title=\"hover-intent\"";
                    echo " 
\t\t\t\t\t\t\t\t";
                }
                // line 117
                echo "\t\t\t\t\t\t\t\t";
                if (((($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = $context["row"]) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["new_window"] ?? null) : null) == 1)) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 118
                    $context["target"] = "target=\"_blank\"";
                    echo " 
\t\t\t\t\t\t\t\t";
                }
                // line 120
                echo "\t\t\t\t\t\t\t\t";
                if (((($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = $context["row"]) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["custom_class"] ?? null) : null) != "")) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 121
                    $context["class"] = ((($context["class"] ?? null) . "") . (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["row"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["custom_class"] ?? null) : null));
                    echo " 
\t\t\t\t\t\t\t\t";
                }
                // line 123
                echo "\t\t\t\t\t\t\t\t";
                $context["label"] = twig_constant("false");
                echo " 
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
                // line 125
                if (((($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = $context["row"]) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["label"] ?? null) : null) != "")) {
                    echo " ";
                    $context["label"] = (((((((((("<span class=\"megamenu-label\" style=\"background: " . (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = $context["row"]) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["label_background_color"] ?? null) : null)) . ";color: ") . (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = $context["row"]) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["label_text_color"] ?? null) : null)) . ";\"><span style=\"background: ") . (($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = $context["row"]) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["label_background_color"] ?? null) : null)) . ";border-color: ") . (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = $context["row"]) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["label_background_color"] ?? null) : null)) . "\"></span>") . (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = $context["row"]) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["label"] ?? null) : null)) . "</span>");
                }
                // line 126
                echo "\t\t\t\t\t\t\t\t";
                echo (((("<li class='" . ($context["class"] ?? null)) . "' ") . ($context["title"] ?? null)) . "><p class='close-menu'></p><p class='open-menu'></p>");
                echo " 
\t\t\t\t\t\t\t\t";
                // line 127
                echo ((((((((((("<a href='" . (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = $context["row"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["link"] ?? null) : null)) . "' class='") . ($context["class_link"] ?? null)) . "' ") . ($context["target"] ?? null)) . "><span><strong>") . (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = $context["row"]) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["icon"] ?? null) : null)) . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "html_entity_decode", [0 => (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = $context["row"]) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["name"] ?? null) : null)) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd[($context["lang_id"] ?? null)] ?? null) : null)], "method", false, false, false, 127)) . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "html_entity_decode", [0 => (($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = $context["row"]) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["description"] ?? null) : null)], "method", false, false, false, 127)) . ($context["label"] ?? null)) . "</strong></span></a>");
                echo " 
\t\t\t\t\t\t\t\t\t";
                // line 128
                if ((twig_test_iterable((($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = $context["row"]) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["submenu"] ?? null) : null)) &&  !twig_test_empty((($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = $context["row"]) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["submenu"] ?? null) : null)))) {
                    // line 129
                    echo "\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t";
                    // line 130
                    if ((((($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = ($context["ustawienia"] ?? null)) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["orientation"] ?? null) : null) == "1") && ((($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = $context["row"]) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["submenu_width"] ?? null) : null) == "100%"))) {
                        echo " ";
                        $context["row"] = twig_array_merge($context["row"], ["submenu_width" => "350%"]);
                        echo " ";
                    }
                    // line 131
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    $context["background_image"] = twig_constant("false");
                    // line 132
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    if (((($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = $context["row"]) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["submenu_background"] ?? null) : null) != "")) {
                        // line 133
                        echo "\t\t\t\t\t\t\t\t\t\t\t";
                        $context["background_image"] = (((((("style=\"background-image:url(image/" . (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = $context["row"]) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["submenu_background"] ?? null) : null)) . ");background-position: ") . (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = $context["row"]) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["submenu_background_position"] ?? null) : null)) . ";background-repeat: ") . (($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = $context["row"]) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["submenu_background_repeat"] ?? null) : null)) . ";\"");
                        // line 134
                        echo "\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 135
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    $context["full_width_class"] = twig_constant("false");
                    // line 136
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    if ((((((($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = $context["row"]) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["submenu_width"] ?? null) : null) == "100%") || ((($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = $context["row"]) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["submenu_width"] ?? null) : null) == "300%")) || ((($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = $context["row"]) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54["submenu_width"] ?? null) : null) == "338%")) || ((($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = $context["row"]) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f["submenu_width"] ?? null) : null) == "1230px"))) {
                        echo " ";
                        $context["full_width_class"] = "full-width-sub-menu";
                    }
                    // line 137
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    echo (((("<div class=\"sub-menu " . ($context["full_width_class"] ?? null)) . "\" style=\"width:") . (($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 = $context["row"]) && is_array($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327) || $__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 instanceof ArrayAccess ? ($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327["submenu_width"] ?? null) : null)) . "\">");
                    echo "
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 138
                    echo (("<div class=\"content\" " . ($context["background_image"] ?? null)) . "><p class=\"arrow\"></p>");
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 139
                    echo "<div class=\"row\">";
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 140
                    $context["row_fluid"] = 0;
                    // line 141
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 = $context["row"]) && is_array($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412) || $__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 instanceof ArrayAccess ? ($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412["submenu"] ?? null) : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["submenu"]) {
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 143
                        if (((($context["row_fluid"] ?? null) + (($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 = $context["submenu"]) && is_array($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9) || $__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 instanceof ArrayAccess ? ($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9["content_width"] ?? null) : null)) > 12)) {
                            // line 144
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            $context["row_fluid"] = (($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e = $context["submenu"]) && is_array($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e) || $__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e instanceof ArrayAccess ? ($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e["content_width"] ?? null) : null);
                            // line 145
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            echo "</div><div class=\"border\"></div><div class=\"row\">";
                            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 146
                            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            // line 147
                            $context["row_fluid"] = (($context["row_fluid"] ?? null) + (($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 = $context["submenu"]) && is_array($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5) || $__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 instanceof ArrayAccess ? ($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5["content_width"] ?? null) : null));
                            // line 148
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 149
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-";
                        echo (($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a = $context["submenu"]) && is_array($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a) || $__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a instanceof ArrayAccess ? ($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a["content_width"] ?? null) : null);
                        echo " ";
                        echo ((((($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 = $context["submenu"]) && is_array($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4) || $__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 instanceof ArrayAccess ? ($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4["display_on_mobile"] ?? null) : null) == "1")) ? ("mobile-disabled") : ("mobile-enabled"));
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 150
                        if (((($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d = $context["submenu"]) && is_array($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d) || $__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d instanceof ArrayAccess ? ($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d["content_type"] ?? null) : null) == "0")) {
                            // line 151
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            echo (($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 = $context["submenu"]) && is_array($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5) || $__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 instanceof ArrayAccess ? ($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5["html"] ?? null) : null);
                            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (((($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a =                         // line 152
$context["submenu"]) && is_array($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a) || $__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a instanceof ArrayAccess ? ($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a["content_type"] ?? null) : null) == "1")) {
                            // line 153
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (twig_test_iterable((($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da = $context["submenu"]) && is_array($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da) || $__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da instanceof ArrayAccess ? ($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da["product"] ?? null) : null))) {
                                // line 154
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                echo "<div class=\"product\">";
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 155
                                echo (((("<div class=\"image\"><a href=\"" . (($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 = (($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec = $context["submenu"]) && is_array($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec) || $__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec instanceof ArrayAccess ? ($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec["product"] ?? null) : null)) && is_array($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38) || $__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 instanceof ArrayAccess ? ($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38["link"] ?? null) : null)) . "\"><img src=\"") . (($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574 = (($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c = $context["submenu"]) && is_array($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c) || $__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c instanceof ArrayAccess ? ($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c["product"] ?? null) : null)) && is_array($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574) || $__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574 instanceof ArrayAccess ? ($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574["image"] ?? null) : null)) . "\" alt=\"\"></a></div>");
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 156
                                echo (((("<div class=\"name\"><a href=\"" . (($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0 = (($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc = $context["submenu"]) && is_array($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc) || $__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc instanceof ArrayAccess ? ($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc["product"] ?? null) : null)) && is_array($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0) || $__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0 instanceof ArrayAccess ? ($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0["link"] ?? null) : null)) . "\">") . (($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd = (($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81 = $context["submenu"]) && is_array($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81) || $__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81 instanceof ArrayAccess ? ($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81["product"] ?? null) : null)) && is_array($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd) || $__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd instanceof ArrayAccess ? ($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd["name"] ?? null) : null)) . "</a></div>");
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 157
                                echo "<div class=\"price\">";
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 158
                                if ( !(($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007 = (($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d = $context["submenu"]) && is_array($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d) || $__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d instanceof ArrayAccess ? ($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d["product"] ?? null) : null)) && is_array($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007) || $__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007 instanceof ArrayAccess ? ($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007["special"] ?? null) : null)) {
                                    // line 159
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    echo (($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba = (($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49 = $context["submenu"]) && is_array($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49) || $__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49 instanceof ArrayAccess ? ($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49["product"] ?? null) : null)) && is_array($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba) || $__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba instanceof ArrayAccess ? ($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba["price"] ?? null) : null);
                                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                } else {
                                    // line 160
                                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 161
                                    echo (($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639 = (($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf = $context["submenu"]) && is_array($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf) || $__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf instanceof ArrayAccess ? ($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf["product"] ?? null) : null)) && is_array($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639) || $__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639 instanceof ArrayAccess ? ($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639["special"] ?? null) : null);
                                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 163
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                echo "</div>";
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 164
                                echo "</div>";
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 166
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (((($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921 = $context["submenu"]) && is_array($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921) || $__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921 instanceof ArrayAccess ? ($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921["content_type"] ?? null) : null) == "2")) {
                            // line 167
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            echo (($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a = $context["submenu"]) && is_array($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a) || $__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a instanceof ArrayAccess ? ($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a["categories"] ?? null) : null);
                            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (((($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4 =                         // line 168
$context["submenu"]) && is_array($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4) || $__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4 instanceof ArrayAccess ? ($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4["content_type"] ?? null) : null) == "3")) {
                            // line 169
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (twig_test_iterable((($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985 = $context["submenu"]) && is_array($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985) || $__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985 instanceof ArrayAccess ? ($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985["products"] ?? null) : null))) {
                                // line 170
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                $context["id"] = (twig_random($this->env, 5000) * twig_random($this->env, 500000));
                                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 171
                                echo "<div class=\"products-carousel-overflow clearfix\">";
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 172
                                echo (("<div class=\"next\" id=\"productsCarousel" . ($context["id"] ?? null)) . "_next\"><span></span></div>");
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 173
                                echo (("<div class=\"prev\" id=\"productsCarousel" . ($context["id"] ?? null)) . "_prev\"><span></span></div>");
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 174
                                echo (("<div class=\"box-heading\">" . (($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51 = (($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a = $context["submenu"]) && is_array($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a) || $__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a instanceof ArrayAccess ? ($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a["heading"] ?? null) : null)) && is_array($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51) || $__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51 instanceof ArrayAccess ? ($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51[($context["lang_id"] ?? null)] ?? null) : null)) . "</div>");
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 175
                                echo "<div class=\"strip-line\"></div>";
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 176
                                echo "<div class=\"clear\"></div>";
                                echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 177
                                echo (("<div class=\"products-carousel owl-carousel\" id=\"productsCarousel" . ($context["id"] ?? null)) . "\">");
                                echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 178
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable((($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762 = $context["submenu"]) && is_array($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762) || $__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762 instanceof ArrayAccess ? ($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762["products"] ?? null) : null));
                                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                                    echo " 
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 179
                                    echo "<div class=\"item\"><div class=\"product\">";
                                    echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 180
                                    echo "<div class=\"image\">";
                                    echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 181
                                    if (((($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053 = $context["product"]) && is_array($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053) || $__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053 instanceof ArrayAccess ? ($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053["special"] ?? null) : null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_text_sale"], "method", false, false, false, 181) != "0"))) {
                                        // line 182
                                        echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        $context["text_sale"] = "Sale";
                                        // line 183
                                        echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 183)], "method", false, false, false, 183) != "")) {
                                            // line 184
                                            echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            $context["text_sale"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 184)], "method", false, false, false, 184);
                                            // line 185
                                            echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        }
                                        // line 186
                                        echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        // line 187
                                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "type_sale"], "method", false, false, false, 187) == "1")) {
                                            echo " 
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            // line 188
                                            $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c = $context["product"]) && is_array($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c) || $__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c instanceof ArrayAccess ? ($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c["id"] ?? null) : null)], "method", false, false, false, 188);
                                            // line 189
                                            echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            $context["roznica_ceny"] = ((($__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c = ($context["product_detail"] ?? null)) && is_array($__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c) || $__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c instanceof ArrayAccess ? ($__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c["price"] ?? null) : null) - (($__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030 = ($context["product_detail"] ?? null)) && is_array($__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030) || $__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030 instanceof ArrayAccess ? ($__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030["special"] ?? null) : null));
                                            // line 190
                                            echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            $context["procent"] = ((($context["roznica_ceny"] ?? null) * 100) / (($__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8 = ($context["product_detail"] ?? null)) && is_array($__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8) || $__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8 instanceof ArrayAccess ? ($__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8["price"] ?? null) : null));
                                            echo " 
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            // line 191
                                            echo (("<div class=\"sale\">-" . twig_round(($context["procent"] ?? null))) . "%</div>");
                                            echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        } else {
                                            // line 192
                                            echo " 
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            // line 193
                                            echo (("<div class=\"sale\">" . ($context["text_sale"] ?? null)) . "</div>");
                                            echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        }
                                        // line 195
                                        echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 196
                                    echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    echo (((("<a href=\"" . (($__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86 = $context["product"]) && is_array($__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86) || $__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86 instanceof ArrayAccess ? ($__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86["link"] ?? null) : null)) . "\"><img src=\"") . (($__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9 = $context["product"]) && is_array($__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9) || $__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9 instanceof ArrayAccess ? ($__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9["image"] ?? null) : null)) . "\" alt=\"\"></a></div>");
                                    echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 197
                                    echo (((("<div class=\"name\"><a href=\"" . (($__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac = $context["product"]) && is_array($__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac) || $__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac instanceof ArrayAccess ? ($__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac["link"] ?? null) : null)) . "\">") . (($__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768 = $context["product"]) && is_array($__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768) || $__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768 instanceof ArrayAccess ? ($__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768["name"] ?? null) : null)) . "</a></div>");
                                    echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 198
                                    echo "<div class=\"price\">";
                                    echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 199
                                    if ( !(($__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57 = $context["product"]) && is_array($__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57) || $__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57 instanceof ArrayAccess ? ($__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57["special"] ?? null) : null)) {
                                        // line 200
                                        echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        echo (($__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898 = $context["product"]) && is_array($__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898) || $__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898 instanceof ArrayAccess ? ($__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898["price"] ?? null) : null);
                                        echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    } else {
                                        // line 201
                                        echo " 
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        // line 202
                                        echo (($__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283 = $context["product"]) && is_array($__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283) || $__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283 instanceof ArrayAccess ? ($__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283["special"] ?? null) : null);
                                        echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 204
                                    echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    echo "</div>";
                                    echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 205
                                    echo "</div></div>";
                                    echo "
\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 207
                                echo "\t \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                echo "</div>";
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 208
                                echo "</div>";
                                echo " 

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">\$(document).ready(function() {
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tvar owl";
                                // line 211
                                echo ($context["id"] ?? null);
                                echo " = \$(\"#productsCarousel";
                                echo ($context["id"] ?? null);
                                echo "\");
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#productsCarousel";
                                // line 213
                                echo ($context["id"] ?? null);
                                echo "_next\").click(function(){
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    owl";
                                // line 214
                                echo ($context["id"] ?? null);
                                echo ".trigger('owl.next');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    return false;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  });
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#productsCarousel";
                                // line 218
                                echo ($context["id"] ?? null);
                                echo "_prev\").click(function(){
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    owl";
                                // line 219
                                echo ($context["id"] ?? null);
                                echo ".trigger('owl.prev');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    return false;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 225
                                $context["one"] = 1;
                                // line 226
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                $context["two"] = 1;
                                // line 227
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                $context["three"] = 1;
                                // line 228
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                $context["four"] = 1;
                                // line 229
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 230
                                if (((($__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a = $context["submenu"]) && is_array($__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a) || $__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a instanceof ArrayAccess ? ($__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a["column"] ?? null) : null) == 2)) {
                                    // line 231
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["two"] = 1;
                                    // line 232
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["three"] = 2;
                                    // line 233
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["four"] = 2;
                                    // line 234
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                }
                                // line 235
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 236
                                if (((($__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3 = $context["submenu"]) && is_array($__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3) || $__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3 instanceof ArrayAccess ? ($__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3["column"] ?? null) : null) == 3)) {
                                    // line 237
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["two"] = 1;
                                    // line 238
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["three"] = 2;
                                    // line 239
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["four"] = 3;
                                    // line 240
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                }
                                // line 241
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 242
                                if (((($__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4 = $context["submenu"]) && is_array($__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4) || $__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4 instanceof ArrayAccess ? ($__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4["column"] ?? null) : null) == 4)) {
                                    // line 243
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["one"] = 1;
                                    // line 244
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["two"] = 2;
                                    // line 245
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["three"] = 3;
                                    // line 246
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["four"] = 4;
                                    // line 247
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                }
                                // line 248
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 249
                                if (((($__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9 = $context["submenu"]) && is_array($__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9) || $__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9 instanceof ArrayAccess ? ($__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9["column"] ?? null) : null) == 5)) {
                                    // line 250
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["one"] = 2;
                                    // line 251
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["two"] = 3;
                                    // line 252
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["three"] = 4;
                                    // line 253
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["four"] = 5;
                                    // line 254
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                }
                                // line 255
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                // line 256
                                if (((($__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7 = $context["submenu"]) && is_array($__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7) || $__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7 instanceof ArrayAccess ? ($__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7["column"] ?? null) : null) == 6)) {
                                    // line 257
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["one"] = 2;
                                    // line 258
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["two"] = 3;
                                    // line 259
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["three"] = 5;
                                    // line 260
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                    $context["four"] = 6;
                                    // line 261
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                }
                                // line 262
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\towl";
                                // line 264
                                echo ($context["id"] ?? null);
                                echo ".owlCarousel({
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t itemsCustom : [ 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t      [0, ";
                                // line 266
                                echo ($context["one"] ?? null);
                                echo "], 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t      [450, ";
                                // line 267
                                echo ($context["two"] ?? null);
                                echo "], 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t      [768, ";
                                // line 268
                                echo ($context["three"] ?? null);
                                echo "], 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t      [1200, ";
                                // line 269
                                echo ($context["four"] ?? null);
                                echo "] 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ],
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                                // line 271
                                if (((($__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416 = ($context["page_direction"] ?? null)) && is_array($__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416) || $__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416 instanceof ArrayAccess ? ($__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) {
                                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t direction: 'rtl'
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ";
                                }
                                // line 273
                                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t });
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t });</script>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 277
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        echo "\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 278
                        echo "</div>";
                        echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['submenu'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 280
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                    echo "</div>";
                    echo "
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 281
                    echo "</div>";
                    echo "
\t\t\t\t\t\t\t\t\t\t";
                    // line 282
                    echo "</div>";
                    echo "
\t\t\t\t\t\t\t\t\t";
                }
                // line 284
                echo "\t\t\t\t\t\t\t\t";
                echo "</li>";
                echo "
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 286
            echo "\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<script type=\"text/javascript\">
\t  \$(window).load(function(){
\t      var css_tpl = '<style type=\"text/css\">';
\t      css_tpl += '#megamenu_";
            // line 295
            echo ($context["id"] ?? null);
            echo " ul.megamenu > li > .sub-menu > .content {';
\t      css_tpl += '-webkit-transition: all ";
            // line 296
            if ((((($__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e = ($context["ustawienia"] ?? null)) && is_array($__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e) || $__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e instanceof ArrayAccess ? ($__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e["animation_time"] ?? null) : null) > 0) && ((($__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f = ($context["ustawienia"] ?? null)) && is_array($__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f) || $__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f instanceof ArrayAccess ? ($__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f["animation_time"] ?? null) : null) < 5000))) {
                echo " ";
                echo (($__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b = ($context["ustawienia"] ?? null)) && is_array($__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b) || $__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b instanceof ArrayAccess ? ($__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b["animation_time"] ?? null) : null);
                echo " ";
            } else {
                echo " ";
                echo 300;
                echo " ";
            }
            echo "ms ease-out !important;';
\t      css_tpl += '-moz-transition: all ";
            // line 297
            if ((((($__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75 = ($context["ustawienia"] ?? null)) && is_array($__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75) || $__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75 instanceof ArrayAccess ? ($__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75["animation_time"] ?? null) : null) > 0) && ((($__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c = ($context["ustawienia"] ?? null)) && is_array($__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c) || $__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c instanceof ArrayAccess ? ($__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c["animation_time"] ?? null) : null) < 5000))) {
                echo " ";
                echo (($__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1 = ($context["ustawienia"] ?? null)) && is_array($__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1) || $__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1 instanceof ArrayAccess ? ($__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1["animation_time"] ?? null) : null);
                echo " ";
            } else {
                echo " ";
                echo 300;
                echo " ";
            }
            echo "ms ease-out !important;';
\t      css_tpl += '-o-transition: all ";
            // line 298
            if ((((($__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24 = ($context["ustawienia"] ?? null)) && is_array($__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24) || $__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24 instanceof ArrayAccess ? ($__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24["animation_time"] ?? null) : null) > 0) && ((($__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850 = ($context["ustawienia"] ?? null)) && is_array($__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850) || $__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850 instanceof ArrayAccess ? ($__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850["animation_time"] ?? null) : null) < 5000))) {
                echo " ";
                echo (($__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34 = ($context["ustawienia"] ?? null)) && is_array($__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34) || $__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34 instanceof ArrayAccess ? ($__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34["animation_time"] ?? null) : null);
                echo " ";
            } else {
                echo " ";
                echo 300;
                echo " ";
            }
            echo "ms ease-out !important;';
\t      css_tpl += '-ms-transition: all ";
            // line 299
            if ((((($__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df = ($context["ustawienia"] ?? null)) && is_array($__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df) || $__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df instanceof ArrayAccess ? ($__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df["animation_time"] ?? null) : null) > 0) && ((($__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4 = ($context["ustawienia"] ?? null)) && is_array($__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4) || $__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4 instanceof ArrayAccess ? ($__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4["animation_time"] ?? null) : null) < 5000))) {
                echo " ";
                echo (($__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36 = ($context["ustawienia"] ?? null)) && is_array($__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36) || $__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36 instanceof ArrayAccess ? ($__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36["animation_time"] ?? null) : null);
                echo " ";
            } else {
                echo " ";
                echo 300;
                echo " ";
            }
            echo "ms ease-out !important;';
\t      css_tpl += 'transition: all ";
            // line 300
            if ((((($__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b = ($context["ustawienia"] ?? null)) && is_array($__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b) || $__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b instanceof ArrayAccess ? ($__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b["animation_time"] ?? null) : null) > 0) && ((($__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e = ($context["ustawienia"] ?? null)) && is_array($__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e) || $__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e instanceof ArrayAccess ? ($__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e["animation_time"] ?? null) : null) < 5000))) {
                echo " ";
                echo (($__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7 = ($context["ustawienia"] ?? null)) && is_array($__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7) || $__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7 instanceof ArrayAccess ? ($__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7["animation_time"] ?? null) : null);
                echo " ";
            } else {
                echo " ";
                echo 300;
                echo " ";
            }
            echo "ms ease-out !important;';
\t      css_tpl += '}</style>'
\t    \$(\"head\").append(css_tpl);
\t  });
\t</script>
";
        }
    }

    public function getTemplateName()
    {
        return "kofi/template/extension/module/megamenu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  937 => 300,  925 => 299,  913 => 298,  901 => 297,  889 => 296,  885 => 295,  874 => 286,  865 => 284,  860 => 282,  856 => 281,  851 => 280,  843 => 278,  838 => 277,  832 => 273,  826 => 271,  821 => 269,  817 => 268,  813 => 267,  809 => 266,  804 => 264,  800 => 262,  797 => 261,  794 => 260,  791 => 259,  788 => 258,  785 => 257,  783 => 256,  780 => 255,  777 => 254,  774 => 253,  771 => 252,  768 => 251,  765 => 250,  763 => 249,  760 => 248,  757 => 247,  754 => 246,  751 => 245,  748 => 244,  745 => 243,  743 => 242,  740 => 241,  737 => 240,  734 => 239,  731 => 238,  728 => 237,  726 => 236,  723 => 235,  720 => 234,  717 => 233,  714 => 232,  711 => 231,  709 => 230,  706 => 229,  703 => 228,  700 => 227,  697 => 226,  695 => 225,  686 => 219,  682 => 218,  675 => 214,  671 => 213,  664 => 211,  658 => 208,  653 => 207,  645 => 205,  640 => 204,  635 => 202,  632 => 201,  626 => 200,  624 => 199,  620 => 198,  616 => 197,  611 => 196,  608 => 195,  603 => 193,  600 => 192,  595 => 191,  590 => 190,  587 => 189,  585 => 188,  581 => 187,  578 => 186,  575 => 185,  572 => 184,  569 => 183,  566 => 182,  564 => 181,  560 => 180,  556 => 179,  550 => 178,  546 => 177,  542 => 176,  538 => 175,  534 => 174,  530 => 173,  526 => 172,  522 => 171,  517 => 170,  514 => 169,  512 => 168,  507 => 167,  504 => 166,  499 => 164,  494 => 163,  489 => 161,  486 => 160,  480 => 159,  478 => 158,  474 => 157,  470 => 156,  466 => 155,  461 => 154,  458 => 153,  456 => 152,  451 => 151,  449 => 150,  442 => 149,  439 => 148,  437 => 147,  434 => 146,  428 => 145,  425 => 144,  423 => 143,  415 => 141,  413 => 140,  409 => 139,  405 => 138,  400 => 137,  394 => 136,  391 => 135,  388 => 134,  385 => 133,  382 => 132,  379 => 131,  373 => 130,  370 => 129,  368 => 128,  364 => 127,  359 => 126,  354 => 125,  348 => 123,  343 => 121,  338 => 120,  333 => 118,  328 => 117,  323 => 115,  318 => 114,  313 => 112,  308 => 111,  302 => 109,  299 => 108,  295 => 106,  290 => 105,  287 => 104,  282 => 103,  278 => 102,  274 => 101,  269 => 100,  264 => 98,  259 => 97,  256 => 96,  253 => 95,  250 => 94,  248 => 93,  241 => 91,  238 => 90,  232 => 87,  192 => 51,  187 => 49,  180 => 45,  176 => 44,  167 => 42,  159 => 37,  156 => 36,  143 => 35,  139 => 34,  131 => 33,  125 => 29,  116 => 24,  108 => 18,  99 => 13,  91 => 8,  57 => 7,  51 => 5,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/extension/module/megamenu.twig", "");
    }
}
