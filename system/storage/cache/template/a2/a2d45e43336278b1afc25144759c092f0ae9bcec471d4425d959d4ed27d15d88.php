<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/revolution_slider/placement.twig */
class __TwigTemplate_975fe742babb46372827f0d545232409d9b712bd87203d29e8e776f97b3aeefc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo " 
<div id=\"content\"><div class=\"container-fluid\">
\t<div class=\"page-header\">
\t    <h1>Revolution Slider</h1>
\t    <ul class=\"breadcrumb\">
\t\t     ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
\t\t      <li><a href=\"";
            // line 7
            echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["breadcrumb"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["href"] ?? null) : null);
            echo "\">";
            echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["breadcrumb"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["text"] ?? null) : null);
            echo "</a></li>
\t\t      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo " 
\t    </ul>
\t  </div>
        
    <link href='https://fonts.googleapis.com/css?family=Poppins:700,600,500,400,300' rel='stylesheet' type='text/css'>
    
\t";
        // line 14
        if (($context["error_warning"] ?? null)) {
            echo " 
\t\t<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            // line 15
            echo ($context["error_warning"] ?? null);
            echo " 
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t";
        } elseif (        // line 18
($context["success"] ?? null)) {
            echo " 
\t\t<div class=\"alert alert-success\"><i class=\"fa fa-exclamation-circle\"></i> ";
            // line 19
            echo ($context["success"] ?? null);
            echo " 
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t";
        }
        // line 22
        echo " 
\t
\t<!-- Revolution slider -->
\t<div class=\"set-size\" id=\"revolution-slider\">
\t\t
\t\t<form action=\"";
        // line 27
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\">
\t\t\t<!-- Content -->
\t\t\t<div class=\"content\">
\t\t\t\t<div>
\t\t\t\t\t<div class=\"bg-tabs clearfix\">
\t\t\t\t\t\t<div id=\"tabs\">
                            <a href=\"";
        // line 33
        echo ($context["placement"] ?? null);
        echo "\" id=\"placement\" class=\"active\"><span>Module<br> placement</span></a>
                            <a href=\"";
        // line 34
        echo ($context["existing"] ?? null);
        echo "\" id=\"existing\"><span>Existing<br> modules</span></a>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"tab-content2\">
\t\t\t\t\t\t\t<table id=\"module-placement\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"first\">Slider</td>
\t\t\t\t\t\t\t\t\t\t<td>Layout</td>
\t\t\t\t\t\t\t\t\t\t<td>Position</td>
\t\t\t\t\t\t\t\t\t\t<td>Sort Order</td>
\t\t\t\t\t\t\t\t\t\t<td>Slider align top</td>
\t\t\t\t\t\t\t\t\t\t<td>Status</td>
\t\t\t\t\t\t\t\t\t\t<td>Delete</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t";
        // line 50
        $context["module_row"] = 0;
        echo " ";
        if ((($context["modules"] ?? null) != "")) {
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 51
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["modules"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                echo " 
\t\t\t\t\t\t\t\t\t<tbody id=\"module";
                // line 52
                echo ($context["module_row"] ?? null);
                echo "\">
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"first\">
\t\t\t\t\t\t\t\t\t\t\t\t<select name=\"revolution_slider_module[";
                // line 55
                echo ($context["module_row"] ?? null);
                echo "][slider_id]\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 56
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["sliders"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["slider"]) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
                    // line 57
                    echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["slider"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["id"] ?? null) : null);
                    echo "\" ";
                    if (((($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["module"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["slider_id"] ?? null) : null) == (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["slider"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["id"] ?? null) : null))) {
                        echo " ";
                        echo "selected=\"selected\"";
                        echo " ";
                    }
                    echo ">";
                    echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["slider"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["name"] ?? null) : null);
                    echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slider'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 58
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t<select name=\"revolution_slider_module[";
                // line 62
                echo ($context["module_row"] ?? null);
                echo "][layout_id]\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 63
                if ((99999 == (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["module"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["layout_id"] ?? null) : null))) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"99999\" selected=\"selected\">All pages</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 65
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"99999\">All pages</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 67
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 68
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["layouts"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["layout"]) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 69
                    if (((($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["layout"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["layout_id"] ?? null) : null) == (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["module"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["layout_id"] ?? null) : null))) {
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
                        // line 70
                        echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["layout"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["layout_id"] ?? null) : null);
                        echo "\" selected=\"selected\">";
                        echo (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["layout"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["name"] ?? null) : null);
                        echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 71
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
                        // line 72
                        echo (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = $context["layout"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["layout_id"] ?? null) : null);
                        echo "\">";
                        echo (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = $context["layout"]) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["name"] ?? null) : null);
                        echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 73
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['layout'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 74
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t<select name=\"revolution_slider_module[";
                // line 78
                echo ($context["module_row"] ?? null);
                echo "][position]\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 79
                if (((($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["module"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["position"] ?? null) : null) == "menu")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"menu\" selected=\"selected\">Menu</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 81
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"menu\">Menu</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 83
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 84
                if (((($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = $context["module"]) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["position"] ?? null) : null) == "slideshow")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"slideshow\" selected=\"selected\">Slideshow</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 86
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"slideshow\">Slideshow</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 88
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 89
                if (((($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["module"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["position"] ?? null) : null) == "preface_left")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"preface_left\" selected=\"selected\">Preface left</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 91
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"preface_left\">Preface left</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 93
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 94
                if (((($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["module"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["position"] ?? null) : null) == "preface_right")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"preface_right\" selected=\"selected\">Preface right</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 96
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"preface_right\">Preface right</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 98
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 99
                if (((($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = $context["module"]) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["position"] ?? null) : null) == "preface_fullwidth")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"preface_fullwidth\" selected=\"selected\">Preface fullwidth</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 101
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"preface_fullwidth\">Preface fullwidth</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 103
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 104
                if (((($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = $context["module"]) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["position"] ?? null) : null) == "column_left")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"column_left\" selected=\"selected\">Column left</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 106
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"column_left\">Column left</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 108
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 109
                if (((($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = $context["module"]) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["position"] ?? null) : null) == "content_big_column")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"content_big_column\" selected=\"selected\">Content big column</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 111
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"content_big_column\">Content big column</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 113
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 114
                if (((($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = $context["module"]) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["position"] ?? null) : null) == "content_top")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"content_top\" selected=\"selected\">Content top</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 116
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"content_top\">Content top</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 118
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 119
                if (((($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["module"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["position"] ?? null) : null) == "column_right")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"column_right\" selected=\"selected\">Column right</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 121
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"column_right\">Column right</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 123
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 124
                if (((($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = $context["module"]) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["position"] ?? null) : null) == "content_bottom")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"content_bottom\" selected=\"selected\">Content bottom</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 126
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"content_bottom\">Content bottom</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 128
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 129
                if (((($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = $context["module"]) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["position"] ?? null) : null) == "customfooter_top")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"customfooter_top\" selected=\"selected\">CustomFooter Top</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 131
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"customfooter_top\">CustomFooter Top</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 133
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 134
                if (((($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = $context["module"]) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["position"] ?? null) : null) == "customfooter_bottom")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"customfooter_bottom\" selected=\"selected\">CustomFooter Bottom</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 136
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"customfooter_bottom\">CustomFooter Bottom</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 138
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 139
                if (((($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = $context["module"]) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["position"] ?? null) : null) == "footer_top")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"footer_top\" selected=\"selected\">Footer top</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 141
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"footer_top\">Footer top</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 143
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 144
                if (((($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = $context["module"]) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["position"] ?? null) : null) == "footer_left")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"footer_left\" selected=\"selected\">Footer left</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 146
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"footer_left\">Footer left</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 148
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 149
                if (((($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = $context["module"]) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["position"] ?? null) : null) == "footer_right")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"footer_right\" selected=\"selected\">Footer right</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 151
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"footer_right\">Footer right</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 153
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 154
                if (((($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = $context["module"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["position"] ?? null) : null) == "footer_bottom")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"footer_bottom\" selected=\"selected\">Footer bottom</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 156
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"footer_bottom\">Footer bottom</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 158
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 159
                if (((($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = $context["module"]) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["position"] ?? null) : null) == "bottom")) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"bottom\" selected=\"selected\">Bottom</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 161
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"bottom\">Bottom</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 163
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"sort\" name=\"revolution_slider_module[";
                // line 167
                echo ($context["module_row"] ?? null);
                echo "][sort_order]\" value=\"";
                echo (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = $context["module"]) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["sort_order"] ?? null) : null);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 170
                if (twig_get_attribute($this->env, $this->source, $context["module"], "slider_align_top", [], "array", true, true, false, 170)) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 171
                    if ((((($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = $context["module"]) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["slider_align_top"] ?? null) : null) == 0) && ((($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = $context["module"]) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["slider_align_top"] ?? null) : null) != ""))) {
                        echo " ";
                        echo (("<div class=\"status status-off\" title=\"0\" rel=\"module_" . ($context["module_row"] ?? null)) . "_slider_align_top\"></div>");
                        echo " ";
                    } else {
                        echo " ";
                        echo (("<div class=\"status status-on\" title=\"1\" rel=\"module_" . ($context["module_row"] ?? null)) . "_slider_align_top\"></div>");
                        echo " ";
                    }
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 172
                    echo " ";
                    echo (("<div class=\"status status-off\" title=\"0\" rel=\"module_" . ($context["module_row"] ?? null)) . "_slider_align_top\"></div>");
                    echo " ";
                }
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t<input name=\"revolution_slider_module[";
                // line 173
                echo ($context["module_row"] ?? null);
                echo "][slider_align_top]\" value=\"";
                if (twig_get_attribute($this->env, $this->source, $context["module"], "slider_align_top", [], "array", true, true, false, 173)) {
                    echo " ";
                    echo (($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = $context["module"]) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["slider_align_top"] ?? null) : null);
                    echo " ";
                } else {
                    echo " ";
                    echo "0";
                    echo " ";
                }
                echo "\" id=\"module_";
                echo ($context["module_row"] ?? null);
                echo "_slider_align_top\" type=\"hidden\" />
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 176
                if (twig_get_attribute($this->env, $this->source, $context["module"], "status", [], "array", true, true, false, 176)) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 177
                    if ((((($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = $context["module"]) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["status"] ?? null) : null) == 0) && ((($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = $context["module"]) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["status"] ?? null) : null) != ""))) {
                        echo " ";
                        echo (("<div class=\"status status-off\" title=\"0\" rel=\"module_" . ($context["module_row"] ?? null)) . "_status\"></div>");
                        echo " ";
                    } else {
                        echo " ";
                        echo (("<div class=\"status status-on\" title=\"1\" rel=\"module_" . ($context["module_row"] ?? null)) . "_status\"></div>");
                        echo " ";
                    }
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 178
                    echo " ";
                    echo (("<div class=\"status status-off\" title=\"0\" rel=\"module_" . ($context["module_row"] ?? null)) . "_status\"></div>");
                    echo " ";
                }
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t<input name=\"revolution_slider_module[";
                // line 179
                echo ($context["module_row"] ?? null);
                echo "][status]\" value=\"";
                if (twig_get_attribute($this->env, $this->source, $context["module"], "status", [], "array", true, true, false, 179)) {
                    echo " ";
                    echo (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = $context["module"]) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["status"] ?? null) : null);
                    echo " ";
                } else {
                    echo " ";
                    echo "0";
                    echo " ";
                }
                echo "\" id=\"module_";
                echo ($context["module_row"] ?? null);
                echo "_status\" type=\"hidden\" />
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t<a onclick=\"\$('#module";
                // line 182
                echo ($context["module_row"] ?? null);
                echo "').remove();\">Remove</a>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t";
                // line 186
                $context["module_row"] = (($context["module_row"] ?? null) + 1);
                echo " ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
\t\t\t\t\t\t\t\t";
        }
        // line 187
        echo " 
\t\t\t\t\t\t\t\t<tfoot></tfoot>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<a onclick=\"addModule();\" class=\"add-module\">Add item</a>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<script type=\"text/javascript\"><!--
\t\t\t\t\t\t\tvar module_row = ";
        // line 194
        echo ($context["module_row"] ?? null);
        echo ";
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tfunction addModule() {
\t\t\t\t\t\t\t\thtml  = '<tbody id=\"module' + module_row + '\">';
\t\t\t\t\t\t\t\thtml += '  <tr>';
\t\t\t\t\t\t\t\thtml += '    <td class=\"first\">';
\t\t\t\t\t\t\t\thtml += '\t\t<select name=\"revolution_slider_module[' + module_row + '][slider_id]\">';
\t\t\t\t\t\t\t\t";
        // line 201
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sliders"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["slider"]) {
            echo " 
\t\t\t\t\t\t\t\thtml += '\t\t\t<option value=\"";
            // line 202
            echo (($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = $context["slider"]) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["id"] ?? null) : null);
            echo "\">";
            echo (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = $context["slider"]) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["name"] ?? null) : null);
            echo "</option>';
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slider'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 203
        echo " 
\t\t\t\t\t\t\t\thtml += '\t\t</select>';
\t\t\t\t\t\t\t\thtml += '    </td>';
\t\t\t\t\t\t\t\thtml += '    <td>';
\t\t\t\t\t\t\t\thtml += '\t\t<select name=\"revolution_slider_module[' + module_row + '][layout_id]\">';
\t\t\t\t\t\t\t\thtml += '\t\t\t<option value=\"99999\">All pages</option>';
\t\t\t\t\t\t\t\t";
        // line 209
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["layouts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["layout"]) {
            echo " 
\t\t\t\t\t\t\t\thtml += '           <option value=\"";
            // line 210
            echo (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = $context["layout"]) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["layout_id"] ?? null) : null);
            echo "\">";
            echo twig_escape_filter($this->env, (($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = $context["layout"]) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["name"] ?? null) : null), "html");
            echo "</option>';
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['layout'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 211
        echo " 
\t\t\t\t\t\t\t\thtml += '\t\t</select>';
\t\t\t\t\t\t\t\thtml += '    </td>';
\t\t\t\t\t\t\t\thtml += '    <td>';
\t\t\t\t\t\t\t\thtml += '\t\t<select name=\"revolution_slider_module[' + module_row + '][position]\">';
\t\t\t\t\t\t\t\thtml += '       \t\t<option value=\"menu\">Menu</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"slideshow\">Slideshow</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t    <option value=\"breadcrumb_top\">Breadcrumb top</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"breadcrumb_bottom\">Breadcrumb bottom</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"preface_left\">Preface left</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"preface_right\">Preface right</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"preface_fullwidth\">Preface fullwidth</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"column_left\">Column left</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"content_big_column\">Content big column</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"content_top\">Content top</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"column_right\">Column right</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"content_bottom\">Content bottom</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"customfooter_top\">CustomFooter Top</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"customfooter_bottom\">CustomFooter Bottom</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"footer_top\">Footer top</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"footer_left\">Footer left</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"footer_right\">Footer right</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"footer_bottom\">Footer bottom</option>';
\t\t\t\t\t\t\t\thtml += '\t\t\t\t<option value=\"bottom\">Bottom</option>';
\t\t\t\t\t\t\t\thtml += '\t\t</select>';
\t\t\t\t\t\t\t\thtml += '    </td>';
\t\t\t\t\t\t\t\thtml += '    <td>';
\t\t\t\t\t\t\t\thtml += '\t\t<input type=\"text\" class=\"sort\" name=\"revolution_slider_module[' + module_row + '][sort_order]\">';
\t\t\t\t\t\t\t\thtml += '    </td>';
\t\t\t\t\t\t\t\thtml += '    <td>';
\t\t\t\t\t\t\t\thtml += '\t\t<div class=\"status status-off\" title=\"0\" rel=\"module_' + module_row + '_slider_align_top\"></div><input name=\"revolution_slider_module[' + module_row + '][slider_align_top]\" value=\"0\" id=\"module_' + module_row + '_slider_align_top\" type=\"hidden\" />';
\t\t\t\t\t\t\t\thtml += '    </td>';
\t\t\t\t\t\t\t\thtml += '    <td>';
\t\t\t\t\t\t\t\thtml += '\t\t<div class=\"status status-off\" title=\"0\" rel=\"module_' + module_row + '_status\"></div><input name=\"revolution_slider_module[' + module_row + '][status]\" value=\"0\" id=\"module_' + module_row + '_status\" type=\"hidden\" />';
\t\t\t\t\t\t\t\thtml += '    </td>';
\t\t\t\t\t\t\t\thtml += '    <td><a onclick=\"\$(\\'#module' + module_row + '\\').remove();\">Remove</a></td>';
\t\t\t\t\t\t\t\thtml += '  </tr>';
\t\t\t\t\t\t\t\thtml += '</tbody>';
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\$('#module-placement tfoot').before(html);
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tmodule_row++;
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t//--></script> 
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div>
\t\t\t\t\t\t<!-- Buttons -->
\t\t\t\t\t\t<div class=\"buttons\"><input type=\"submit\" name=\"button-save\" class=\"button-save\" value=\"\"></div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>\t\t
\t\t</form>
\t</div>
</div>

<script type=\"text/javascript\">
jQuery(document).ready(function(\$) {
\t
\t\$('#revolution-slider').on('click', '.status', function () {
\t\t
\t\tvar styl = \$(this).attr(\"rel\");
\t\tvar co = \$(this).attr(\"title\");
\t\t
\t\tif(co == 1) {
\t\t
\t\t\t\$(this).removeClass('status-on');
\t\t\t\$(this).addClass('status-off');
\t\t\t\$(this).attr(\"title\", \"0\");

\t\t\t\$(\"#\"+styl+\"\").val(0);
\t\t
\t\t}
\t\t
\t\tif(co == 0) {
\t\t
\t\t\t\$(this).addClass('status-on');
\t\t\t\$(this).removeClass('status-off');
\t\t\t\$(this).attr(\"title\", \"1\");

\t\t\t\$(\"#\"+styl+\"\").val(1);
\t\t
\t\t}
\t\t
\t});

});\t
</script>
";
        // line 300
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/revolution_slider/placement.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  742 => 300,  651 => 211,  641 => 210,  635 => 209,  627 => 203,  617 => 202,  611 => 201,  601 => 194,  592 => 187,  582 => 186,  575 => 182,  557 => 179,  550 => 178,  537 => 177,  533 => 176,  515 => 173,  508 => 172,  495 => 171,  491 => 170,  483 => 167,  477 => 163,  472 => 161,  466 => 159,  463 => 158,  458 => 156,  452 => 154,  449 => 153,  444 => 151,  438 => 149,  435 => 148,  430 => 146,  424 => 144,  421 => 143,  416 => 141,  410 => 139,  407 => 138,  402 => 136,  396 => 134,  393 => 133,  388 => 131,  382 => 129,  379 => 128,  374 => 126,  368 => 124,  365 => 123,  360 => 121,  354 => 119,  351 => 118,  346 => 116,  340 => 114,  337 => 113,  332 => 111,  326 => 109,  323 => 108,  318 => 106,  312 => 104,  309 => 103,  304 => 101,  298 => 99,  295 => 98,  290 => 96,  284 => 94,  281 => 93,  276 => 91,  270 => 89,  267 => 88,  262 => 86,  256 => 84,  253 => 83,  248 => 81,  242 => 79,  238 => 78,  232 => 74,  225 => 73,  218 => 72,  215 => 71,  208 => 70,  204 => 69,  198 => 68,  195 => 67,  190 => 65,  184 => 63,  180 => 62,  174 => 58,  158 => 57,  152 => 56,  148 => 55,  142 => 52,  136 => 51,  130 => 50,  111 => 34,  107 => 33,  98 => 27,  91 => 22,  84 => 19,  80 => 18,  74 => 15,  70 => 14,  62 => 8,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/revolution_slider/placement.twig", "");
    }
}
