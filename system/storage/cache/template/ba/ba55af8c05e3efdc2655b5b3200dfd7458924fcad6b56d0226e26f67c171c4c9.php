<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/product/search.twig */
class __TwigTemplate_e141ada94b5167155b03bdcae6f3ff2eb9327c7f122f8252559784e9909ce353 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
        // line 3
        $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
        // line 4
        $context["page_direction"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_direction"], "method", false, false, false, 4);
        echo " 
";
        // line 5
        $context["language_id"] = twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 5);
        echo " 
";
        // line 6
        $context["background_status"] = twig_constant("false");
        // line 7
        $this->loadTemplate("kofi/template/new_elements/wrapper_top.twig", "kofi/template/product/search.twig", 7)->display($context);
        // line 8
        echo "
<div id=\"mfilter-content-container\">
  <label class=\"control-label\" for=\"input-search\"><b>";
        // line 10
        echo ($context["entry_search"] ?? null);
        echo "</b></label>
  <div class=\"row\" id=\"content-search\" style=\"padding-top: 7px\">
    <div class=\"col-sm-4\">
      <input type=\"text\" name=\"search\" value=\"";
        // line 13
        echo ($context["search"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["text_keyword"] ?? null);
        echo "\" id=\"input-search\" class=\"form-control\" />
    </div>
    <div class=\"col-sm-3\">
      <select name=\"category_id\" class=\"form-control\">
        <option value=\"0\">";
        // line 17
        echo ($context["text_category"] ?? null);
        echo "</option>
        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category_1"]) {
            echo " 
        ";
            // line 19
            if (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["category_1"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["category_id"] ?? null) : null) == ($context["category_id"] ?? null))) {
                echo " 
        <option value=\"";
                // line 20
                echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["category_1"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["category_id"] ?? null) : null);
                echo "\" selected=\"selected\">";
                echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["category_1"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["name"] ?? null) : null);
                echo "</option>
        ";
            } else {
                // line 21
                echo " 
        <option value=\"";
                // line 22
                echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["category_1"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["category_id"] ?? null) : null);
                echo "\">";
                echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["category_1"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["name"] ?? null) : null);
                echo "</option>
        ";
            }
            // line 23
            echo " 
        ";
            // line 24
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["category_1"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["children"] ?? null) : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category_2"]) {
                echo " 
        ";
                // line 25
                if (((($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["category_2"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["category_id"] ?? null) : null) == ($context["category_id"] ?? null))) {
                    echo " 
        <option value=\"";
                    // line 26
                    echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["category_2"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["category_id"] ?? null) : null);
                    echo "\" selected=\"selected\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["category_2"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["name"] ?? null) : null);
                    echo "</option>
        ";
                } else {
                    // line 27
                    echo " 
        <option value=\"";
                    // line 28
                    echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["category_2"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["category_id"] ?? null) : null);
                    echo "\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    echo (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["category_2"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["name"] ?? null) : null);
                    echo "</option>
        ";
                }
                // line 29
                echo " 
        ";
                // line 30
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = $context["category_2"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["children"] ?? null) : null));
                foreach ($context['_seq'] as $context["_key"] => $context["category_3"]) {
                    echo " 
        ";
                    // line 31
                    if (((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = $context["category_3"]) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["category_id"] ?? null) : null) == ($context["category_id"] ?? null))) {
                        echo " 
        <option value=\"";
                        // line 32
                        echo (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["category_3"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["category_id"] ?? null) : null);
                        echo "\" selected=\"selected\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        echo (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = $context["category_3"]) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["name"] ?? null) : null);
                        echo "</option>
        ";
                    } else {
                        // line 33
                        echo " 
        <option value=\"";
                        // line 34
                        echo (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["category_3"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["category_id"] ?? null) : null);
                        echo "\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        echo (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["category_3"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["name"] ?? null) : null);
                        echo "</option>
        ";
                    }
                    // line 35
                    echo " 
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_3'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 36
                echo " 
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_2'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo " 
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_1'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo " 
      </select>
    </div>
    <div class=\"col-sm-3\" style=\"padding-top: 7px\">
      <label class=\"checkbox-inline\">
        ";
        // line 43
        if (($context["sub_category"] ?? null)) {
            echo " 
        <input type=\"checkbox\" name=\"sub_category\" value=\"1\" checked=\"checked\" />
        ";
        } else {
            // line 45
            echo " 
        <input type=\"checkbox\" name=\"sub_category\" value=\"1\" />
        ";
        }
        // line 47
        echo " 
        ";
        // line 48
        echo ($context["text_sub_category"] ?? null);
        echo "</label>
    </div>
  </div>
  <p>
    <label class=\"checkbox-inline\">
      ";
        // line 53
        if (($context["description"] ?? null)) {
            echo " 
      <input type=\"checkbox\" name=\"description\" value=\"1\" id=\"description\" checked=\"checked\" />
      ";
        } else {
            // line 55
            echo " 
      <input type=\"checkbox\" name=\"description\" value=\"1\" id=\"description\" />
      ";
        }
        // line 57
        echo " 
      ";
        // line 58
        echo ($context["entry_description"] ?? null);
        echo "</label>
  </p>
  <input type=\"button\" value=\"";
        // line 60
        echo ($context["button_search"] ?? null);
        echo "\" id=\"button-search\" class=\"btn btn-primary\" style=\"margin-top: 10px\" />
  <h2 style=\"padding-top: 40px\">";
        // line 61
        echo ($context["text_search"] ?? null);
        echo "</h2>
  
  ";
        // line 63
        if (($context["products"] ?? null)) {
            echo " 
  ";
            // line 64
            $context["currently"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getCurrently", [], "method", false, false, false, 64);
            // line 65
            echo "  <!-- Filter -->
  <div class=\"product-filter clearfix\">
    <div class=\"options\">
      <div class=\"product-compare\"><a href=\"";
            // line 68
            echo ($context["compare"] ?? null);
            echo "\" id=\"compare-total\">";
            echo ($context["text_compare"] ?? null);
            echo "</a></div>
      
      <div class=\"button-group display\" data-toggle=\"buttons-radio\">
        <button id=\"grid\" ";
            // line 71
            if ((($context["currently"] ?? null) == "grid")) {
                echo " ";
                echo "class=\"active\"";
                echo " ";
            }
            echo " rel=\"tooltip\" title=\"Grid\" onclick=\"display('grid');\"><i class=\"fa fa-th-large\"></i></button>
        <button id=\"list\" ";
            // line 72
            if ((($context["currently"] ?? null) != "grid")) {
                echo " ";
                echo "class=\"active\"";
                echo " ";
            }
            echo " rel=\"tooltip\" title=\"List\" onclick=\"display('list');\"><i class=\"fa fa-th-list\"></i></button>
      </div>
    </div>
    
    <div class=\"list-options\">
      <div class=\"sort\">
        <select onchange=\"location = this.value;\">
          ";
            // line 79
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                echo " 
          ";
                // line 80
                if (((($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = $context["sorts"]) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["value"] ?? null) : null) == ((($context["sort"] ?? null) . "-") . ($context["order"] ?? null)))) {
                    echo " 
          <option value=\"";
                    // line 81
                    echo (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = $context["sorts"]) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["href"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = $context["sorts"]) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["text"] ?? null) : null);
                    echo "</option>
          ";
                } else {
                    // line 82
                    echo " 
          <option value=\"";
                    // line 83
                    echo (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = $context["sorts"]) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["sorts"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["text"] ?? null) : null);
                    echo "</option>
          ";
                }
                // line 84
                echo " 
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 85
            echo " 
        </select>
      </div>
      
      <div class=\"limit\">
        <select onchange=\"location = this.value;\">
          ";
            // line 91
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                echo " 
          ";
                // line 92
                if (((($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = $context["limits"]) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["value"] ?? null) : null) == ($context["limit"] ?? null))) {
                    echo " 
          <option value=\"";
                    // line 93
                    echo (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = $context["limits"]) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["href"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = $context["limits"]) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["text"] ?? null) : null);
                    echo "</option>
          ";
                } else {
                    // line 94
                    echo " 
          <option value=\"";
                    // line 95
                    echo (($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = $context["limits"]) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = $context["limits"]) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["text"] ?? null) : null);
                    echo "</option>
          ";
                }
                // line 96
                echo " 
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 97
            echo " 
        </select>
      </div>
    </div>
  </div>
  
  ";
            // line 103
            if ((($context["currently"] ?? null) != "grid")) {
                echo " 
  <!-- Products list -->
  <div class=\"product-list active\">
    ";
                // line 106
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    echo " 
    <!-- Product -->
    <div>
      <div class=\"row\">
        <div class=\"image col-sm-3\">
          ";
                    // line 111
                    if (((($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = $context["product"]) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["special"] ?? null) : null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_text_sale"], "method", false, false, false, 111) != "0"))) {
                        echo " 
            ";
                        // line 112
                        $context["text_sale"] = "Sale";
                        // line 113
                        echo "          ";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 113)], "method", false, false, false, 113) != "")) {
                            // line 114
                            echo "            ";
                            $context["text_sale"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 114)], "method", false, false, false, 114);
                            // line 115
                            echo "          ";
                        }
                        echo " 
            ";
                        // line 116
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "type_sale"], "method", false, false, false, 116) == "1")) {
                            echo " 
            ";
                            // line 117
                            $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = $context["product"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["product_id"] ?? null) : null)], "method", false, false, false, 117);
                            // line 118
                            echo "          ";
                            $context["roznica_ceny"] = ((($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = ($context["product_detail"] ?? null)) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["price"] ?? null) : null) - (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = ($context["product_detail"] ?? null)) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["special"] ?? null) : null));
                            // line 119
                            echo "          ";
                            $context["procent"] = ((($context["roznica_ceny"] ?? null) * 100) / (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = ($context["product_detail"] ?? null)) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["price"] ?? null) : null));
                            echo " 
            <div class=\"sale\">-";
                            // line 120
                            echo twig_round(($context["procent"] ?? null));
                            echo "%</div>
            ";
                        } else {
                            // line 121
                            echo " 
            <div class=\"sale\">";
                            // line 122
                            echo ($context["text_sale"] ?? null);
                            echo "</div>
            ";
                        }
                        // line 123
                        echo " 
          ";
                    } elseif (((twig_get_attribute($this->env, $this->source,                     // line 124
($context["theme_options"] ?? null), "get", [0 => "display_text_new"], "method", false, false, false, 124) != "0") && twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "isLatestProduct", [0 => (($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = $context["product"]) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["product_id"] ?? null) : null)], "method", false, false, false, 124))) {
                        echo " 
             <div class=\"new\">";
                        // line 125
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "new_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 125)], "method", false, false, false, 125) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "new_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 125)], "method", false, false, false, 125);
                            echo " ";
                        } else {
                            echo " ";
                            echo "New";
                            echo " ";
                        }
                        echo "</div>
          ";
                    }
                    // line 126
                    echo " 
          
          ";
                    // line 128
                    if ((($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = $context["product"]) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["thumb"] ?? null) : null)) {
                        echo " 
            ";
                        // line 129
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "lazy_loading_images"], "method", false, false, false, 129) != "0")) {
                            echo " 
            <a href=\"";
                            // line 130
                            echo (($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = $context["product"]) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["href"] ?? null) : null);
                            echo "\"><img src=\"image/catalog/blank.gif\" data-echo=\"";
                            echo (($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = $context["product"]) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["thumb"] ?? null) : null);
                            echo "\" alt=\"";
                            echo (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = $context["product"]) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["name"] ?? null) : null);
                            echo "\" /></a>
            ";
                        } else {
                            // line 131
                            echo " 
            <a href=\"";
                            // line 132
                            echo (($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = $context["product"]) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["href"] ?? null) : null);
                            echo "\"><img src=\"";
                            echo (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = $context["product"]) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["thumb"] ?? null) : null);
                            echo "\" alt=\"";
                            echo (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = $context["product"]) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["name"] ?? null) : null);
                            echo "\" /></a>
            ";
                        }
                        // line 133
                        echo " 
          ";
                    } else {
                        // line 134
                        echo " 
          <a href=\"";
                        // line 135
                        echo (($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = $context["product"]) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["href"] ?? null) : null);
                        echo "\"><img src=\"image/no_image.jpg\" alt=\"";
                        echo (($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = $context["product"]) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["name"] ?? null) : null);
                        echo "\" /></a>
          ";
                    }
                    // line 136
                    echo " 
          
          ";
                    // line 138
                    if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_specials_countdown"], "method", false, false, false, 138) == "1") && (($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = $context["product"]) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["special"] ?? null) : null))) {
                        echo " ";
                        $context["countdown"] = (twig_random($this->env, 5000) * twig_random($this->env, 500000));
                        echo " 
         ";
                        // line 139
                        $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = $context["product"]) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54["product_id"] ?? null) : null)], "method", false, false, false, 139);
                        // line 140
                        echo "         ";
                        $context["date_end"] = (($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = ($context["product_detail"] ?? null)) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f["date_end"] ?? null) : null);
                        // line 141
                        echo "         ";
                        if (((($context["date_end"] ?? null) != "0000-00-00") && ($context["date_end"] ?? null))) {
                            echo " 
                  <script>
                  \$(function () {
                    var austDay = new Date();
                    austDay = new Date(";
                            // line 145
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "Y", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 145)], "method", false, false, false, 145);
                            echo ", ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "m", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 145)], "method", false, false, false, 145);
                            echo " - 1, ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "d", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 145)], "method", false, false, false, 145);
                            echo ");
                    \$('#countdown";
                            // line 146
                            echo ($context["countdown"] ?? null);
                            echo "').countdown({until: austDay});
                  });
                  </script>
                  <div id=\"countdown";
                            // line 149
                            echo ($context["countdown"] ?? null);
                            echo "\" class=\"clearfix\"></div>
                 ";
                        }
                        // line 150
                        echo " 
          ";
                    }
                    // line 151
                    echo " 
        </div>
        
        <div class=\"name-actions col-sm-4\">
             <div class=\"name\"><a href=\"";
                    // line 155
                    echo (($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 = $context["product"]) && is_array($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327) || $__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 instanceof ArrayAccess ? ($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 = $context["product"]) && is_array($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412) || $__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 instanceof ArrayAccess ? ($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412["name"] ?? null) : null);
                    echo "</a></div>
             ";
                    // line 156
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_list_type"], "method", false, false, false, 156) == "4")) {
                        echo " 
             ";
                        // line 157
                        $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 = $context["product"]) && is_array($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9) || $__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 instanceof ArrayAccess ? ($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9["product_id"] ?? null) : null)], "method", false, false, false, 157);
                        echo " 
             <div class=\"brand\">";
                        // line 158
                        echo (($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e = ($context["product_detail"] ?? null)) && is_array($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e) || $__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e instanceof ArrayAccess ? ($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e["manufacturer"] ?? null) : null);
                        echo "</div>
             ";
                    }
                    // line 159
                    echo " 
             
             ";
                    // line 161
                    if ((($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 = $context["product"]) && is_array($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5) || $__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 instanceof ArrayAccess ? ($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5["price"] ?? null) : null)) {
                        echo " 
          <div class=\"price\">
            ";
                        // line 163
                        if ( !(($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a = $context["product"]) && is_array($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a) || $__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a instanceof ArrayAccess ? ($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a["special"] ?? null) : null)) {
                            echo " 
            ";
                            // line 164
                            echo (($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 = $context["product"]) && is_array($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4) || $__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 instanceof ArrayAccess ? ($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4["price"] ?? null) : null);
                            echo " 
            ";
                        } else {
                            // line 165
                            echo " 
            <span class=\"price-old\">";
                            // line 166
                            echo (($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d = $context["product"]) && is_array($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d) || $__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d instanceof ArrayAccess ? ($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d["price"] ?? null) : null);
                            echo "</span> <span class=\"price-new\">";
                            echo (($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 = $context["product"]) && is_array($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5) || $__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 instanceof ArrayAccess ? ($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5["special"] ?? null) : null);
                            echo "</span>
            ";
                        }
                        // line 167
                        echo " 
          </div>
          ";
                    }
                    // line 169
                    echo " 
          
          <ul>
               ";
                    // line 172
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_cart"], "method", false, false, false, 172) != "0")) {
                        echo " 
                    ";
                        // line 173
                        $context["enquiry"] = twig_constant("false");
                        echo " ";
                        if ((twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "product_blocks_module"], "method", false, false, false, 173) != "")) {
                            echo " ";
                            $context["enquiry"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "productIsEnquiry", [0 => (($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a = $context["product"]) && is_array($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a) || $__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a instanceof ArrayAccess ? ($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a["product_id"] ?? null) : null)], "method", false, false, false, 173);
                            echo " ";
                        }
                        // line 174
                        echo "         ";
                        if (twig_test_iterable(($context["enquiry"] ?? null))) {
                            echo " 
                    <li><a href=\"javascript:openPopup('";
                            // line 175
                            echo (($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da = ($context["enquiry"] ?? null)) && is_array($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da) || $__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da instanceof ArrayAccess ? ($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da["popup_module"] ?? null) : null);
                            echo "', '";
                            echo (($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 = $context["product"]) && is_array($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38) || $__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 instanceof ArrayAccess ? ($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38["product_id"] ?? null) : null);
                            echo "')\" data-toggle=\"tooltip\" data-original-title=\"";
                            echo (($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec = ($context["enquiry"] ?? null)) && is_array($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec) || $__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec instanceof ArrayAccess ? ($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec["block_name"] ?? null) : null);
                            echo "\"><i class=\"fa fa-question\"></i></a></li>
                    ";
                        } else {
                            // line 176
                            echo " 
                    <li><a onclick=\"cart.add('";
                            // line 177
                            echo (($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574 = $context["product"]) && is_array($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574) || $__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574 instanceof ArrayAccess ? ($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574["product_id"] ?? null) : null);
                            echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                            echo ($context["button_cart"] ?? null);
                            echo "\"><i class=\"fa fa-shopping-cart\"></i></a></li>
                    ";
                        }
                        // line 178
                        echo " 
               ";
                    }
                    // line 179
                    echo " 
               
               ";
                    // line 181
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quick_view"], "method", false, false, false, 181) == 1)) {
                        echo " 
               <li class=\"quickview\"><a href=\"index.php?route=product/quickview&amp;product_id=";
                        // line 182
                        echo (($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c = $context["product"]) && is_array($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c) || $__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c instanceof ArrayAccess ? ($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c["product_id"] ?? null) : null);
                        echo "\" data-toggle=\"tooltip\" data-original-title=\"";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quickview_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 182)], "method", false, false, false, 182) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "html_entity_decode", [0 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quickview_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 182)], "method", false, false, false, 182)], "method", false, false, false, 182);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Quickview";
                            echo " ";
                        }
                        echo "\"><i class=\"fa fa-search\"></i></a></li>
               ";
                    }
                    // line 183
                    echo " 
          
            ";
                    // line 185
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_compare"], "method", false, false, false, 185) != "0")) {
                        echo " 
            <li><a onclick=\"compare.add('";
                        // line 186
                        echo (($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0 = $context["product"]) && is_array($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0) || $__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0 instanceof ArrayAccess ? ($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0["product_id"] ?? null) : null);
                        echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 186)], "method", false, false, false, 186) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 186)], "method", false, false, false, 186);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Add to compare";
                            echo " ";
                        }
                        echo "\"><i class=\"fa fa-exchange\"></i></a></li>
            ";
                    }
                    // line 187
                    echo " 
            
            ";
                    // line 189
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_wishlist"], "method", false, false, false, 189) != "0")) {
                        echo " 
            <li><a onclick=\"wishlist.add('";
                        // line 190
                        echo (($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc = $context["product"]) && is_array($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc) || $__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc instanceof ArrayAccess ? ($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc["product_id"] ?? null) : null);
                        echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_wishlist_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 190)], "method", false, false, false, 190) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_wishlist_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 190)], "method", false, false, false, 190);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Add to wishlist";
                            echo " ";
                        }
                        echo "\"><i class=\"fa fa-heart\"></i></a></li>
            ";
                    }
                    // line 191
                    echo " 
          </ul>
        </div>
        
        <div class=\"desc col-sm-5\">
          <div class=\"description\">";
                    // line 196
                    echo (($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd = $context["product"]) && is_array($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd) || $__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd instanceof ArrayAccess ? ($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd["description"] ?? null) : null);
                    echo "</div>
        </div>
      </div>
    </div>
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 200
                echo " 
  </div>
  ";
            }
            // line 202
            echo " 
  
  ";
            // line 204
            if ((($context["currently"] ?? null) == "grid")) {
                echo " 
  <!-- Products grid -->
  
 ";
                // line 207
                $context["class"] = 3;
                echo " 
 ";
                // line 208
                $context["row"] = 4;
                echo " 
 
 ";
                // line 210
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 210) == 6)) {
                    echo " ";
                    $context["class"] = 2;
                    echo " ";
                }
                // line 211
                echo " ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 211) == 5)) {
                    echo " ";
                    $context["class"] = 25;
                    echo " ";
                }
                // line 212
                echo " ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 212) == 3)) {
                    echo " ";
                    $context["class"] = 4;
                    echo " ";
                }
                // line 213
                echo " 
 ";
                // line 214
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 214) > 1)) {
                    echo " ";
                    $context["row"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 214);
                    echo " ";
                }
                echo " 
 
  <div class=\"product-grid active\">
    <div class=\"row\">
        ";
                // line 218
                $context["row_fluid"] = 0;
                echo " ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    echo " ";
                    $context["row_fluid"] = (($context["row_fluid"] ?? null) + 1);
                    echo " 
          ";
                    // line 219
                    $context["r"] = (($context["row_fluid"] ?? null) - (twig_round((($context["row_fluid"] ?? null) / ($context["row"] ?? null)), 0, "floor") * ($context["row"] ?? null)));
                    echo " ";
                    if (((($context["row_fluid"] ?? null) > ($context["row"] ?? null)) && (($context["r"] ?? null) == 1))) {
                        echo " ";
                        echo "</div><div class=\"row\">";
                        echo " ";
                    }
                    echo " 
          <div class=\"col-sm-";
                    // line 220
                    echo ($context["class"] ?? null);
                    echo " col-xs-6\">
              ";
                    // line 221
                    $this->loadTemplate("kofi/template/new_elements/product.twig", "kofi/template/product/search.twig", 221)->display($context);
                    echo " 
          </div>
        ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 223
                echo " 
    </div>
  </div>
  ";
            }
            // line 226
            echo " 
  
  <div class=\"row pagination-results\">
    <div class=\"col-sm-6 text-left\">";
            // line 229
            echo ($context["pagination"] ?? null);
            echo "</div>
    <div class=\"col-sm-6 text-right\">";
            // line 230
            echo ($context["results"] ?? null);
            echo "</div>
  </div>
  ";
        } else {
            // line 232
            echo " 
  <p style=\"padding-bottom: 10px;padding-top: 10px\">";
            // line 233
            echo ($context["text_empty"] ?? null);
            echo "</p>
  ";
        }
        // line 234
        echo " 
</div>
<script type=\"text/javascript\"><!--
\$('#content-search input[name=\\'search\\']').keydown(function(e) {
  if (e.keyCode == 13) {
    \$('#button-search').trigger('click');
  }
});

\$('select[name=\\'category_id\\']').bind('change', function() {
  if (this.value == '0') {
    \$('input[name=\\'sub_category\\']').attr('disabled', 'disabled');
    \$('input[name=\\'sub_category\\']').removeAttr('checked');
  } else {
    \$('input[name=\\'sub_category\\']').removeAttr('disabled');
  }
});

\$('select[name=\\'category_id\\']').trigger('change');

\$('#button-search').bind('click', function() {
  url = 'index.php?route=product/search';
  
  var search = \$('#content-search input[name=\\'search\\']').attr('value');
  
  if (search) {
    url += '&search=' + encodeURIComponent(search);
  }

  var category_id = \$('#content select[name=\\'category_id\\']').attr('value');
  
  if (category_id > 0) {
    url += '&category_id=' + encodeURIComponent(category_id);
  }
  
  var sub_category = \$('#content input[name=\\'sub_category\\']:checked').attr('value');
  
  if (sub_category) {
    url += '&sub_category=true';
  }
    
  var filter_description = \$('#content input[name=\\'description\\']:checked').attr('value');
  
  if (filter_description) {
    url += '&description=true';
  }

  location = url;
});

function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function display(view) {
  if (view == 'list') {
    setCookie('display','list');
    location.reload();
  } else {
    setCookie('display','grid');
    location.reload();
  }
}
//--></script> 

";
        // line 301
        $this->loadTemplate("kofi/template/new_elements/wrapper_bottom.twig", "kofi/template/product/search.twig", 301)->display($context);
        // line 302
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "kofi/template/product/search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  945 => 302,  943 => 301,  874 => 234,  869 => 233,  866 => 232,  860 => 230,  856 => 229,  851 => 226,  845 => 223,  828 => 221,  824 => 220,  814 => 219,  791 => 218,  780 => 214,  777 => 213,  770 => 212,  763 => 211,  757 => 210,  752 => 208,  748 => 207,  742 => 204,  738 => 202,  733 => 200,  722 => 196,  715 => 191,  700 => 190,  696 => 189,  692 => 187,  677 => 186,  673 => 185,  669 => 183,  654 => 182,  650 => 181,  646 => 179,  642 => 178,  635 => 177,  632 => 176,  623 => 175,  618 => 174,  610 => 173,  606 => 172,  601 => 169,  596 => 167,  589 => 166,  586 => 165,  581 => 164,  577 => 163,  572 => 161,  568 => 159,  563 => 158,  559 => 157,  555 => 156,  549 => 155,  543 => 151,  539 => 150,  534 => 149,  528 => 146,  520 => 145,  512 => 141,  509 => 140,  507 => 139,  501 => 138,  497 => 136,  490 => 135,  487 => 134,  483 => 133,  474 => 132,  471 => 131,  462 => 130,  458 => 129,  454 => 128,  450 => 126,  437 => 125,  433 => 124,  430 => 123,  425 => 122,  422 => 121,  417 => 120,  412 => 119,  409 => 118,  407 => 117,  403 => 116,  398 => 115,  395 => 114,  392 => 113,  390 => 112,  386 => 111,  376 => 106,  370 => 103,  362 => 97,  355 => 96,  348 => 95,  345 => 94,  338 => 93,  334 => 92,  328 => 91,  320 => 85,  313 => 84,  306 => 83,  303 => 82,  296 => 81,  292 => 80,  286 => 79,  272 => 72,  264 => 71,  256 => 68,  251 => 65,  249 => 64,  245 => 63,  240 => 61,  236 => 60,  231 => 58,  228 => 57,  223 => 55,  217 => 53,  209 => 48,  206 => 47,  201 => 45,  195 => 43,  188 => 38,  181 => 37,  174 => 36,  167 => 35,  160 => 34,  157 => 33,  150 => 32,  146 => 31,  140 => 30,  137 => 29,  130 => 28,  127 => 27,  120 => 26,  116 => 25,  110 => 24,  107 => 23,  100 => 22,  97 => 21,  90 => 20,  86 => 19,  80 => 18,  76 => 17,  67 => 13,  61 => 10,  57 => 8,  55 => 7,  53 => 6,  49 => 5,  45 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/product/search.twig", "");
    }
}
