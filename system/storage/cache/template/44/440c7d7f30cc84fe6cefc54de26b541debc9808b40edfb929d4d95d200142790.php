<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/product/category.twig */
class __TwigTemplate_845a130df06dc61bd319746bc4fe83f1b351aecbabbff7fca963cb0ee5f0f806 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
        // line 3
        $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
        echo " 
";
        // line 4
        $context["background_status"] = twig_constant("false");
        // line 5
        $this->loadTemplate("kofi/template/new_elements/wrapper_top.twig", "kofi/template/product/category.twig", 5)->display($context);
        // line 6
        echo "
<div id=\"mfilter-content-container\">
  ";
        // line 8
        if ((($context["thumb"] ?? null) || ($context["description"] ?? null))) {
            echo " 
  <div class=\"category-info clearfix\">
    ";
            // line 10
            if (($context["thumb"] ?? null)) {
                echo " 
      <div class=\"image\"><img src=\"";
                // line 11
                echo ($context["thumb"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" /></div>
    ";
            }
            // line 12
            echo " 
    ";
            // line 13
            if (($context["description"] ?? null)) {
                echo " 
      ";
                // line 14
                echo ($context["description"] ?? null);
                echo " 
    ";
            }
            // line 15
            echo " 
  </div>
  ";
        }
        // line 17
        echo " 
  ";
        // line 18
        if ((($context["categories"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_style"], "method", false, false, false, 18) != "2"))) {
            echo " 
  <div class=\"refine_search_overflow text-center\"><h2 class=\"refine_search\">";
            // line 19
            echo ($context["text_refine"] ?? null);
            echo "</h2></div>
  <div class=\"category-list";
            // line 20
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_style"], "method", false, false, false, 20) == "1")) {
                echo " ";
                echo " category-list-text-only";
                echo " ";
            }
            echo "\">
    <div class=\"row\">
      
   ";
            // line 23
            $context["class"] = 3;
            echo " 
   ";
            // line 24
            $context["row"] = 4;
            echo " 
   
   ";
            // line 26
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_number"], "method", false, false, false, 26) == 2)) {
                echo " ";
                $context["class"] = 62;
                echo " ";
            }
            // line 27
            echo "   ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_number"], "method", false, false, false, 27) == 5)) {
                echo " ";
                $context["class"] = 25;
                echo " ";
            }
            // line 28
            echo "   ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_number"], "method", false, false, false, 28) == 3)) {
                echo " ";
                $context["class"] = 4;
                echo " ";
            }
            // line 29
            echo "   ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_number"], "method", false, false, false, 29) == 6)) {
                echo " ";
                $context["class"] = 2;
                echo " ";
            }
            // line 30
            echo "   
   ";
            // line 31
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_number"], "method", false, false, false, 31) > 1)) {
                echo " ";
                $context["row"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_number"], "method", false, false, false, 31);
                echo " ";
            }
            echo " 
   
    ";
            // line 33
            $context["row_fluid"] = 0;
            echo " ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "refineSearch", [], "method", false, false, false, 33));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                echo " ";
                $context["row_fluid"] = (($context["row_fluid"] ?? null) + 1);
                echo " 
      
    ";
                // line 35
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_style"], "method", false, false, false, 35) != "1")) {
                    // line 36
                    echo "      ";
                    $context["width"] = 250;
                    // line 37
                    echo "      ";
                    $context["height"] = 250;
                    // line 38
                    echo "      ";
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_image_width"], "method", false, false, false, 38) > 20)) {
                        echo " ";
                        $context["width"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_image_width"], "method", false, false, false, 38);
                    }
                    // line 39
                    echo "      ";
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_image_height"], "method", false, false, false, 39) > 20)) {
                        echo " ";
                        $context["height"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_image_height"], "method", false, false, false, 39);
                    }
                    // line 40
                    echo "      ";
                    $context["model_tool_image"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "model_tool_image"], "method", false, false, false, 40);
                    // line 41
                    echo "      ";
                    if (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["category"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["thumb"] ?? null) : null) != "")) {
                        echo " 
        ";
                        // line 42
                        $context["image"] = twig_get_attribute($this->env, $this->source, ($context["model_tool_image"] ?? null), "resize", [0 => (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["category"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["thumb"] ?? null) : null), 1 => ($context["width"] ?? null), 2 => ($context["height"] ?? null)], "method", false, false, false, 42);
                        echo " 
      ";
                    } else {
                        // line 43
                        echo " 
        ";
                        // line 44
                        $context["image"] = twig_get_attribute($this->env, $this->source, ($context["model_tool_image"] ?? null), "resize", [0 => "no_image.jpg", 1 => ($context["width"] ?? null), 2 => ($context["height"] ?? null)], "method", false, false, false, 44);
                        echo " 
      ";
                    }
                    // line 45
                    echo " 
    ";
                }
                // line 47
                echo "    
      ";
                // line 48
                $context["r"] = (($context["row_fluid"] ?? null) - (twig_round((($context["row_fluid"] ?? null) / ($context["row"] ?? null)), 0, "floor") * ($context["row"] ?? null)));
                echo " ";
                if (((($context["row_fluid"] ?? null) > ($context["row"] ?? null)) && (($context["r"] ?? null) == 1))) {
                    echo " ";
                    echo "</div><div class=\"row\">";
                    echo " ";
                }
                echo " 
      <div class=\"col-sm-";
                // line 49
                echo ($context["class"] ?? null);
                echo " col-xs-6\">
        ";
                // line 50
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_style"], "method", false, false, false, 50) != "1")) {
                    echo " 
        <a href=\"";
                    // line 51
                    echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["category"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["href"] ?? null) : null);
                    echo "\"><img src=\"";
                    echo ($context["image"] ?? null);
                    echo "\" alt=\"";
                    echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["category"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["name"] ?? null) : null);
                    echo "\" /></a>
        ";
                }
                // line 52
                echo " 
        <a href=\"";
                // line 53
                echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["category"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["href"] ?? null) : null);
                echo "\">";
                echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["category"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["name"] ?? null) : null);
                echo "</a>
      </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo " 
  </div>
  </div>
  ";
        }
        // line 58
        echo " 
  ";
        // line 59
        if (($context["products"] ?? null)) {
            echo " 
  ";
            // line 60
            $context["currently"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getCurrently", [], "method", false, false, false, 60);
            // line 61
            echo "  <!-- Filter -->
  <div class=\"product-filter clearfix\">
    <div class=\"options\">
      <div class=\"product-compare\"><a href=\"";
            // line 64
            echo ($context["compare"] ?? null);
            echo "\" id=\"compare-total\">";
            echo ($context["text_compare"] ?? null);
            echo "</a></div>
      
      <div class=\"button-group display\" data-toggle=\"buttons-radio\">
        <button id=\"grid\" ";
            // line 67
            if ((($context["currently"] ?? null) == "grid")) {
                echo " ";
                echo "class=\"active\"";
                echo " ";
            }
            echo " rel=\"tooltip\" title=\"Grid\" onclick=\"display('grid');\"><i class=\"fa fa-th-large\"></i></button>
        <button id=\"list\" ";
            // line 68
            if ((($context["currently"] ?? null) != "grid")) {
                echo " ";
                echo "class=\"active\"";
                echo " ";
            }
            echo " rel=\"tooltip\" title=\"List\" onclick=\"display('list');\"><i class=\"fa fa-th-list\"></i></button>
      </div>
    </div>
    
    <div class=\"list-options\">
      <div class=\"sort\">
        <select id=\"sort-options\" onchange=\"location = this.value;\">
          ";
            // line 75
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                echo " 
          ";
                // line 76
                if (((($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["sorts"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["value"] ?? null) : null) == ((($context["sort"] ?? null) . "-") . ($context["order"] ?? null)))) {
                    echo " 
          <option value=\"";
                    // line 77
                    echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["sorts"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["href"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["sorts"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["text"] ?? null) : null);
                    echo "</option>
          ";
                } else {
                    // line 78
                    echo " 
          <option value=\"";
                    // line 79
                    echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["sorts"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["sorts"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["text"] ?? null) : null);
                    echo "</option>
          ";
                }
                // line 80
                echo " 
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo " 
          <option id=\"bestseller\">Bestsellers</option>
        </select>
      </div>
      
      <script type=\"text/javascript\">
        const urlParams = new URLSearchParams(window.location.search);
        const myParam = urlParams.get('path');
        const url = \"http://djaf.prodavalnik.site/index.php?route=product/category&path=\"+myParam+\"&sort=bestseller&order=ASC\";
        
          document.getElementById('bestseller').value = url;
          console.log(url);
      </script>
      
      <script type=\"text/javascript\">
          window.onload = function(){
                const urlParams = new URLSearchParams(window.location.search);
                const sort = urlParams.get('sort');
                
                if(sort == 'bestseller'){
                    let sort_dropdown = document.getElementById('sort-options');
                    sort_dropdown.selectedIndex = 9;
                }
          }
      </script>
      <div class=\"limit\">
        <select onchange=\"location = this.value;\">
          ";
            // line 108
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                echo " 
          ";
                // line 109
                if (((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = $context["limits"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["value"] ?? null) : null) == ($context["limit"] ?? null))) {
                    echo " 
          <option value=\"";
                    // line 110
                    echo (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = $context["limits"]) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["href"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["limits"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["text"] ?? null) : null);
                    echo "</option>
          ";
                } else {
                    // line 111
                    echo " 
          <option value=\"";
                    // line 112
                    echo (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = $context["limits"]) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["limits"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["text"] ?? null) : null);
                    echo "</option>
          ";
                }
                // line 113
                echo " 
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 114
            echo " 
        </select>
      </div>
    </div>
  </div>
  
  ";
            // line 120
            if ((($context["currently"] ?? null) != "grid")) {
                echo " 
  <!-- Products list -->
  <div class=\"product-list active\">
    ";
                // line 123
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    echo " 
    <!-- Product -->
    <div>
      <div class=\"row\">
        <div class=\"image col-sm-3\">
          ";
                    // line 128
                    if (((($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["product"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["special"] ?? null) : null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_text_sale"], "method", false, false, false, 128) != "0"))) {
                        echo " 
            ";
                        // line 129
                        $context["text_sale"] = "Sale";
                        // line 130
                        echo "          ";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 130)], "method", false, false, false, 130) != "")) {
                            // line 131
                            echo "            ";
                            $context["text_sale"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 131)], "method", false, false, false, 131);
                            // line 132
                            echo "          ";
                        }
                        echo " 
            ";
                        // line 133
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "type_sale"], "method", false, false, false, 133) == "1")) {
                            echo " 
            ";
                            // line 134
                            $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = $context["product"]) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["product_id"] ?? null) : null)], "method", false, false, false, 134);
                            // line 135
                            echo "          ";
                            $context["roznica_ceny"] = ((($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = ($context["product_detail"] ?? null)) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["price"] ?? null) : null) - (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = ($context["product_detail"] ?? null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["special"] ?? null) : null));
                            // line 136
                            echo "          ";
                            $context["procent"] = ((($context["roznica_ceny"] ?? null) * 100) / (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = ($context["product_detail"] ?? null)) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["price"] ?? null) : null));
                            echo " 
            <div class=\"sale\">-";
                            // line 137
                            echo twig_round(($context["procent"] ?? null));
                            echo "%</div>
            ";
                        } else {
                            // line 138
                            echo " 
            <div class=\"sale\">";
                            // line 139
                            echo ($context["text_sale"] ?? null);
                            echo "</div>
            ";
                        }
                        // line 140
                        echo " 
          ";
                    } elseif (((twig_get_attribute($this->env, $this->source,                     // line 141
($context["theme_options"] ?? null), "get", [0 => "display_text_new"], "method", false, false, false, 141) != "0") && twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "isLatestProduct", [0 => (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["product"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["product_id"] ?? null) : null)], "method", false, false, false, 141))) {
                        echo " 
             <div class=\"new\">";
                        // line 142
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "new_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 142)], "method", false, false, false, 142) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "new_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 142)], "method", false, false, false, 142);
                            echo " ";
                        } else {
                            echo " ";
                            echo "New";
                            echo " ";
                        }
                        echo "</div>
          ";
                    }
                    // line 143
                    echo " 
          
          ";
                    // line 145
                    if ((($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = $context["product"]) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["thumb"] ?? null) : null)) {
                        echo " 
            ";
                        // line 146
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "lazy_loading_images"], "method", false, false, false, 146) != "0")) {
                            echo " 
            <a href=\"";
                            // line 147
                            echo (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = $context["product"]) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["href"] ?? null) : null);
                            echo "\"><img src=\"image/catalog/blank.gif\" data-echo=\"";
                            echo (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = $context["product"]) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["thumb"] ?? null) : null);
                            echo "\" alt=\"";
                            echo (($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = $context["product"]) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["name"] ?? null) : null);
                            echo "\" /></a>
            ";
                        } else {
                            // line 148
                            echo " 
            <a href=\"";
                            // line 149
                            echo (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = $context["product"]) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["href"] ?? null) : null);
                            echo "\"><img src=\"";
                            echo (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = $context["product"]) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["thumb"] ?? null) : null);
                            echo "\" alt=\"";
                            echo (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = $context["product"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["name"] ?? null) : null);
                            echo "\" /></a>
            ";
                        }
                        // line 150
                        echo " 
          ";
                    } else {
                        // line 151
                        echo " 
          <a href=\"";
                        // line 152
                        echo (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = $context["product"]) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["href"] ?? null) : null);
                        echo "\"><img src=\"image/no_image.jpg\" alt=\"";
                        echo (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = $context["product"]) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["name"] ?? null) : null);
                        echo "\" /></a>
          ";
                    }
                    // line 153
                    echo " 
          
          ";
                    // line 155
                    if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_specials_countdown"], "method", false, false, false, 155) == "1") && (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = $context["product"]) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["special"] ?? null) : null))) {
                        echo " 
            ";
                        // line 156
                        $context["countdown"] = (twig_random($this->env, 5000) * twig_random($this->env, 50000));
                        echo " 
             ";
                        // line 157
                        $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = $context["product"]) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["product_id"] ?? null) : null)], "method", false, false, false, 157);
                        // line 158
                        echo "             ";
                        $context["date_end"] = (($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = ($context["product_detail"] ?? null)) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["date_end"] ?? null) : null);
                        // line 159
                        echo "             ";
                        if (((($context["date_end"] ?? null) != "0000-00-00") && ($context["date_end"] ?? null))) {
                            echo " 
                  <script>
                  \$(function () {
                    var austDay = new Date();
                    austDay = new Date(";
                            // line 163
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "Y", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 163)], "method", false, false, false, 163);
                            echo ", ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "m", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 163)], "method", false, false, false, 163);
                            echo " - 1, ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "d", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 163)], "method", false, false, false, 163);
                            echo ");
                    \$('#countdown";
                            // line 164
                            echo ($context["countdown"] ?? null);
                            echo "').countdown({until: austDay});
                  });
                  </script>
                  <div id=\"countdown";
                            // line 167
                            echo ($context["countdown"] ?? null);
                            echo "\" class=\"clearfix\"></div>
                 ";
                        }
                        // line 168
                        echo " 
          ";
                    }
                    // line 169
                    echo " 
        </div>
        
        <div class=\"name-actions col-sm-4\">
             <div class=\"name\"><a href=\"";
                    // line 173
                    echo (($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = $context["product"]) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = $context["product"]) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["name"] ?? null) : null);
                    echo "</a></div>
             ";
                    // line 174
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_list_type"], "method", false, false, false, 174) == "4")) {
                        echo " 
               ";
                        // line 175
                        $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = $context["product"]) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["product_id"] ?? null) : null)], "method", false, false, false, 175);
                        echo " 
               <div class=\"brand\">";
                        // line 176
                        echo (($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = ($context["product_detail"] ?? null)) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["manufacturer"] ?? null) : null);
                        echo "</div>
             ";
                    }
                    // line 177
                    echo " 
             
             ";
                    // line 179
                    if ((($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = $context["product"]) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["price"] ?? null) : null)) {
                        echo " 
          <div class=\"price\">
            ";
                        // line 181
                        if ( !(($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = $context["product"]) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["special"] ?? null) : null)) {
                            echo " 
              ";
                            // line 182
                            echo (($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = $context["product"]) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["price"] ?? null) : null);
                            echo " 
            ";
                        } else {
                            // line 183
                            echo " 
              <span class=\"price-old\">";
                            // line 184
                            echo (($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = $context["product"]) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["price"] ?? null) : null);
                            echo "</span> <span class=\"price-new\">";
                            echo (($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = $context["product"]) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["special"] ?? null) : null);
                            echo "</span>
            ";
                        }
                        // line 185
                        echo " 
          </div>
          ";
                    }
                    // line 187
                    echo " 
          
          <ul>
               ";
                    // line 190
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_cart"], "method", false, false, false, 190) != "0")) {
                        echo " 
                    ";
                        // line 191
                        $context["enquiry"] = twig_constant("false");
                        echo " ";
                        if ((twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "product_blocks_module"], "method", false, false, false, 191) != "")) {
                            echo " ";
                            $context["enquiry"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "productIsEnquiry", [0 => (($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = $context["product"]) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54["product_id"] ?? null) : null)], "method", false, false, false, 191);
                            echo " ";
                        }
                        // line 192
                        echo "                    ";
                        if (twig_test_iterable(($context["enquiry"] ?? null))) {
                            echo " 
                    <li><a href=\"javascript:openPopup('";
                            // line 193
                            echo (($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = ($context["enquiry"] ?? null)) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f["popup_module"] ?? null) : null);
                            echo "', '";
                            echo (($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 = $context["product"]) && is_array($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327) || $__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 instanceof ArrayAccess ? ($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327["product_id"] ?? null) : null);
                            echo "')\" data-toggle=\"tooltip\" data-original-title=\"";
                            echo (($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 = ($context["enquiry"] ?? null)) && is_array($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412) || $__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 instanceof ArrayAccess ? ($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412["block_name"] ?? null) : null);
                            echo "\"><i class=\"fa fa-question\"></i></a></li>
                    ";
                        } else {
                            // line 194
                            echo " 
                    <li class=\"add-to-cart\"><a onclick=\"cart.add('";
                            // line 195
                            echo (($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 = $context["product"]) && is_array($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9) || $__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 instanceof ArrayAccess ? ($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9["product_id"] ?? null) : null);
                            echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                            echo ($context["button_cart"] ?? null);
                            echo "\"></a></li>
                    ";
                        }
                        // line 196
                        echo " 
               ";
                    }
                    // line 197
                    echo " 
               
               ";
                    // line 199
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quick_view"], "method", false, false, false, 199) == 1)) {
                        echo " 
               <li class=\"quickview\"><a href=\"index.php?route=product/quickview&amp;product_id=";
                        // line 200
                        echo (($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e = $context["product"]) && is_array($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e) || $__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e instanceof ArrayAccess ? ($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e["product_id"] ?? null) : null);
                        echo "\" data-toggle=\"tooltip\" data-original-title=\"";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quickview_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 200)], "method", false, false, false, 200) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "html_entity_decode", [0 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quickview_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 200)], "method", false, false, false, 200)], "method", false, false, false, 200);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Quickview";
                            echo " ";
                        }
                        echo "\"></a></li>
               ";
                    }
                    // line 201
                    echo " 
          
            ";
                    // line 203
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_compare"], "method", false, false, false, 203) != "0")) {
                        echo " 
            <li><a onclick=\"compare.add('";
                        // line 204
                        echo (($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 = $context["product"]) && is_array($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5) || $__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 instanceof ArrayAccess ? ($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5["product_id"] ?? null) : null);
                        echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 204)], "method", false, false, false, 204) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 204)], "method", false, false, false, 204);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Add to compare";
                            echo " ";
                        }
                        echo "\"></a></li>
            ";
                    }
                    // line 205
                    echo " 
            
            ";
                    // line 207
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_wishlist"], "method", false, false, false, 207) != "0")) {
                        echo " 
            <li class=\"wishlist\"><a onclick=\"wishlist.add('";
                        // line 208
                        echo (($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a = $context["product"]) && is_array($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a) || $__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a instanceof ArrayAccess ? ($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a["product_id"] ?? null) : null);
                        echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_wishlist_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 208)], "method", false, false, false, 208) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_wishlist_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 208)], "method", false, false, false, 208);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Add to wishlist";
                            echo " ";
                        }
                        echo "\"></a></li>
            ";
                    }
                    // line 209
                    echo " 
          </ul>
        </div>
        
        <div class=\"desc col-sm-5\">
          <div class=\"description\">";
                    // line 214
                    echo (($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 = $context["product"]) && is_array($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4) || $__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 instanceof ArrayAccess ? ($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4["description"] ?? null) : null);
                    echo "</div>
        </div>
      </div>
    </div>
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 218
                echo " 
  </div>
  ";
            }
            // line 220
            echo " 
  
  ";
            // line 222
            if ((($context["currently"] ?? null) == "grid")) {
                echo " 
  <!-- Products grid -->
  
 ";
                // line 225
                $context["class"] = 3;
                echo " 
 ";
                // line 226
                $context["row"] = 4;
                echo " 
 
 ";
                // line 228
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow2"], "method", false, false, false, 228) == 6)) {
                    echo " ";
                    $context["class"] = 2;
                    echo " ";
                }
                // line 229
                echo " ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow2"], "method", false, false, false, 229) == 5)) {
                    echo " ";
                    $context["class"] = 25;
                    echo " ";
                }
                // line 230
                echo " ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow2"], "method", false, false, false, 230) == 3)) {
                    echo " ";
                    $context["class"] = 4;
                    echo " ";
                }
                // line 231
                echo " 
 ";
                // line 232
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow2"], "method", false, false, false, 232) > 1)) {
                    echo " ";
                    $context["row"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow2"], "method", false, false, false, 232);
                    echo " ";
                }
                echo " 
 
  <div class=\"product-grid active\">
    <div class=\"row\">
      ";
                // line 236
                $context["row_fluid"] = 0;
                echo " ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    echo " ";
                    $context["row_fluid"] = (($context["row_fluid"] ?? null) + 1);
                    echo " 
        ";
                    // line 237
                    $context["r"] = (($context["row_fluid"] ?? null) - (twig_round((($context["row_fluid"] ?? null) / ($context["row"] ?? null)), 0, "floor") * ($context["row"] ?? null)));
                    echo " ";
                    if (((($context["row_fluid"] ?? null) > ($context["row"] ?? null)) && (($context["r"] ?? null) == 1))) {
                        echo " ";
                        echo "</div><div class=\"row\">";
                        echo " ";
                    }
                    echo " 
        <div class=\"col-sm-";
                    // line 238
                    echo ($context["class"] ?? null);
                    echo " col-xs-6\">
            ";
                    // line 239
                    $this->loadTemplate("kofi/template/new_elements/product.twig", "kofi/template/product/category.twig", 239)->display($context);
                    // line 240
                    echo "        </div>
      ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 241
                echo " 
    </div>
  </div>
  ";
            }
            // line 244
            echo " 
  
  <div class=\"row pagination-results\">
    <div class=\"col-sm-6 text-left\">";
            // line 247
            echo ($context["pagination"] ?? null);
            echo "</div>
    <div class=\"col-sm-6 text-right\">";
            // line 248
            echo ($context["results"] ?? null);
            echo "</div>
  </div>
  ";
        }
        // line 250
        echo " 
  ";
        // line 251
        if (( !($context["categories"] ?? null) &&  !($context["products"] ?? null))) {
            echo " 
  <p style=\"padding-top: 30px\">";
            // line 252
            echo ($context["text_empty"] ?? null);
            echo "</p>
  <div class=\"buttons\">
    <div class=\"pull-right\"><a href=\"";
            // line 254
            echo ($context["continue"] ?? null);
            echo "\" class=\"btn btn-primary\">";
            echo ($context["button_continue"] ?? null);
            echo "</a></div>
  </div>
  ";
        }
        // line 256
        echo " 
<script type=\"text/javascript\"><!--
function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function display(view) {
  if (view == 'list') {
    setCookie('display','list');
    location.reload();
  } else {
    setCookie('display','grid');
    location.reload();
  }
}
//--></script> 
</div>

";
        // line 276
        $this->loadTemplate("kofi/template/new_elements/wrapper_bottom.twig", "kofi/template/product/category.twig", 276)->display($context);
        // line 277
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "kofi/template/product/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  954 => 277,  952 => 276,  930 => 256,  922 => 254,  917 => 252,  913 => 251,  910 => 250,  904 => 248,  900 => 247,  895 => 244,  889 => 241,  874 => 240,  872 => 239,  868 => 238,  858 => 237,  835 => 236,  824 => 232,  821 => 231,  814 => 230,  807 => 229,  801 => 228,  796 => 226,  792 => 225,  786 => 222,  782 => 220,  777 => 218,  766 => 214,  759 => 209,  744 => 208,  740 => 207,  736 => 205,  721 => 204,  717 => 203,  713 => 201,  698 => 200,  694 => 199,  690 => 197,  686 => 196,  679 => 195,  676 => 194,  667 => 193,  662 => 192,  654 => 191,  650 => 190,  645 => 187,  640 => 185,  633 => 184,  630 => 183,  625 => 182,  621 => 181,  616 => 179,  612 => 177,  607 => 176,  603 => 175,  599 => 174,  593 => 173,  587 => 169,  583 => 168,  578 => 167,  572 => 164,  564 => 163,  556 => 159,  553 => 158,  551 => 157,  547 => 156,  543 => 155,  539 => 153,  532 => 152,  529 => 151,  525 => 150,  516 => 149,  513 => 148,  504 => 147,  500 => 146,  496 => 145,  492 => 143,  479 => 142,  475 => 141,  472 => 140,  467 => 139,  464 => 138,  459 => 137,  454 => 136,  451 => 135,  449 => 134,  445 => 133,  440 => 132,  437 => 131,  434 => 130,  432 => 129,  428 => 128,  418 => 123,  412 => 120,  404 => 114,  397 => 113,  390 => 112,  387 => 111,  380 => 110,  376 => 109,  370 => 108,  341 => 81,  334 => 80,  327 => 79,  324 => 78,  317 => 77,  313 => 76,  307 => 75,  293 => 68,  285 => 67,  277 => 64,  272 => 61,  270 => 60,  266 => 59,  263 => 58,  257 => 55,  246 => 53,  243 => 52,  234 => 51,  230 => 50,  226 => 49,  216 => 48,  213 => 47,  209 => 45,  204 => 44,  201 => 43,  196 => 42,  191 => 41,  188 => 40,  182 => 39,  176 => 38,  173 => 37,  170 => 36,  168 => 35,  157 => 33,  148 => 31,  145 => 30,  138 => 29,  131 => 28,  124 => 27,  118 => 26,  113 => 24,  109 => 23,  99 => 20,  95 => 19,  91 => 18,  88 => 17,  83 => 15,  78 => 14,  74 => 13,  71 => 12,  64 => 11,  60 => 10,  55 => 8,  51 => 6,  49 => 5,  47 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/product/category.twig", "");
    }
}
