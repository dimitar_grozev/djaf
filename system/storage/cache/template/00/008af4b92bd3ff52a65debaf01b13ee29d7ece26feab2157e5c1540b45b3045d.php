<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/extension/module/account.twig */
class __TwigTemplate_41eaa2b643aa38956947872630e4d036fea37d11fd612534c75862c07a522fa5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"box box-with-links box-no-advanced\">
  <div class=\"box-heading\">";
        // line 2
        echo ($context["heading_title"] ?? null);
        echo "</div>
  <div class=\"strip-line\"></div>
  <div class=\"box-content\">
    <ul class=\"list-box\">
      ";
        // line 6
        if ( !($context["logged"] ?? null)) {
            echo " 
      <li><a href=\"";
            // line 7
            echo ($context["login"] ?? null);
            echo "\">";
            echo ($context["text_login"] ?? null);
            echo "</a> / <a href=\"";
            echo ($context["register"] ?? null);
            echo "\">";
            echo ($context["text_register"] ?? null);
            echo "</a></li>
      <li><a href=\"";
            // line 8
            echo ($context["forgotten"] ?? null);
            echo "\">";
            echo ($context["text_forgotten"] ?? null);
            echo "</a></li>
      ";
        }
        // line 9
        echo " 
      <li><a href=\"";
        // line 10
        echo ($context["account"] ?? null);
        echo "\">";
        echo ($context["text_account"] ?? null);
        echo "</a></li>
      ";
        // line 11
        if (($context["logged"] ?? null)) {
            echo " 
      <li><a href=\"";
            // line 12
            echo ($context["edit"] ?? null);
            echo "\">";
            echo ($context["text_edit"] ?? null);
            echo "</a></li>
      <li><a href=\"";
            // line 13
            echo ($context["password"] ?? null);
            echo "\">";
            echo ($context["text_password"] ?? null);
            echo "</a></li>
      ";
        }
        // line 14
        echo " 
      <li><a href=\"";
        // line 15
        echo ($context["address"] ?? null);
        echo "\">";
        echo ($context["text_address"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 16
        echo ($context["wishlist"] ?? null);
        echo "\">";
        echo ($context["text_wishlist"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 17
        echo ($context["order"] ?? null);
        echo "\">";
        echo ($context["text_order"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 18
        echo ($context["download"] ?? null);
        echo "\">";
        echo ($context["text_download"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 19
        echo ($context["recurring"] ?? null);
        echo "\">";
        echo ($context["text_recurring"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 20
        echo ($context["reward"] ?? null);
        echo "\">";
        echo ($context["text_reward"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 21
        echo ($context["return"] ?? null);
        echo "\">";
        echo ($context["text_return"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 22
        echo ($context["transaction"] ?? null);
        echo "\">";
        echo ($context["text_transaction"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 23
        echo ($context["newsletter"] ?? null);
        echo "\">";
        echo ($context["text_newsletter"] ?? null);
        echo "</a></li>
      ";
        // line 24
        if (($context["logged"] ?? null)) {
            echo " 
      <li><a href=\"";
            // line 25
            echo ($context["logout"] ?? null);
            echo "\">";
            echo ($context["text_logout"] ?? null);
            echo "</a></li>
      ";
        }
        // line 26
        echo " 
    </ul>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "kofi/template/extension/module/account.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 26,  155 => 25,  151 => 24,  145 => 23,  139 => 22,  133 => 21,  127 => 20,  121 => 19,  115 => 18,  109 => 17,  103 => 16,  97 => 15,  94 => 14,  87 => 13,  81 => 12,  77 => 11,  71 => 10,  68 => 9,  61 => 8,  51 => 7,  47 => 6,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/extension/module/account.twig", "");
    }
}
