<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/extension/module/category.twig */
class __TwigTemplate_b16d0775ec4602476269937660c58dbd28bbf152ae466b7c56f2c018f2976e27 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
\t";
            // line 2
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
            echo " 
\t<div class=\"box box-with-categories ";
            // line 3
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "category_box_style"], "method", false, false, false, 3) == "3")) {
                echo " ";
                echo "category-box-type-4 category-box-type-2";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "category_box_style"], "method", false, false, false, 3) == "2")) {
                echo " ";
                echo "category-box-type-3 category-box-type-2";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "category_box_style"], "method", false, false, false, 3) == "1")) {
                echo " ";
                echo "category-box-type-2";
                echo " ";
            }
            echo " box-no-advanced\">
\t  <div class=\"box-heading\">";
            // line 4
            echo ($context["heading_title"] ?? null);
            echo "</div>
\t  <div class=\"strip-line\"></div>
\t  <div class=\"box-content box-category\">
\t    <ul class=\"accordion\" id=\"accordion-category\">
\t      ";
            // line 8
            $context["i"] = 0;
            echo " ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                echo " 
\t      ";
                // line 9
                $context["i"] = (($context["i"] ?? null) + 1);
                echo " 
\t      ";
                // line 10
                $context["first"] = (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["category"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["category_id"] ?? null) : null);
                echo " 
\t      <li class=\"panel\">
\t        ";
                // line 12
                if (((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["category"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["category_id"] ?? null) : null) == ($context["category_id"] ?? null))) {
                    echo " 
\t        <a href=\"";
                    // line 13
                    echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["category"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["href"] ?? null) : null);
                    echo "\" class=\"active\">";
                    echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["category"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["name"] ?? null) : null);
                    echo "</a>
\t        ";
                } else {
                    // line 14
                    echo " 
\t        <a href=\"";
                    // line 15
                    echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["category"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["category"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["name"] ?? null) : null);
                    echo "</a>
\t        ";
                }
                // line 16
                echo " 

\t        ";
                // line 18
                $context["categories_2"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getCategories", [0 => (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["category"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["category_id"] ?? null) : null)], "method", false, false, false, 18);
                echo " 
\t        ";
                // line 19
                if (($context["categories_2"] ?? null)) {
                    echo " 
\t        <span class=\"head\"><a style=\"float:right;padding-right:5px\" class=\"accordion-toggle";
                    // line 20
                    if (((($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["category"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["category_id"] ?? null) : null) != ($context["category_id"] ?? null))) {
                        echo " ";
                        echo " collapsed";
                        echo " ";
                    }
                    echo "\" data-toggle=\"collapse\" data-parent=\"#accordion-category\" href=\"#category";
                    echo ($context["i"] ?? null);
                    echo "\"><span class=\"plus\">+</span><span class=\"minus\">-</span></a></span>
\t        ";
                    // line 21
                    if ( !twig_test_empty(($context["categories_2"] ?? null))) {
                        echo " 
\t        <div id=\"category";
                        // line 22
                        echo ($context["i"] ?? null);
                        echo "\" class=\"panel-collapse collapse ";
                        if (((($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["category"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["category_id"] ?? null) : null) == ($context["category_id"] ?? null))) {
                            echo " ";
                            echo "in";
                            echo " ";
                        }
                        echo "\" style=\"clear:both\">
\t        \t<ul>
\t\t\t       ";
                        // line 24
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(($context["categories_2"] ?? null));
                        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                            echo " ";
                            $context["second"] = (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["child"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["category_id"] ?? null) : null);
                            echo " 
\t\t\t        <li>
\t\t\t         ";
                            // line 26
                            if (((($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["child"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["category_id"] ?? null) : null) == ($context["child_id"] ?? null))) {
                                echo " 
\t\t\t         <a href=\"";
                                // line 27
                                echo (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = $context["child"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["href"] ?? null) : null);
                                echo "\" class=\"active\">";
                                echo (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = $context["child"]) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["name"] ?? null) : null);
                                echo "</a>
\t\t\t         ";
                            } else {
                                // line 28
                                echo " 
\t\t\t         <a href=\"";
                                // line 29
                                echo (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["child"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["href"] ?? null) : null);
                                echo "\">";
                                echo (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = $context["child"]) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["name"] ?? null) : null);
                                echo "</a>
\t\t\t         ";
                            }
                            // line 30
                            echo " 
\t\t\t         ";
                            // line 31
                            $context["categories_3"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getCategories", [0 => (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["child"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["category_id"] ?? null) : null)], "method", false, false, false, 31);
                            echo " 
\t\t\t         ";
                            // line 32
                            if ((twig_length_filter($this->env, ($context["categories_3"] ?? null)) > 0)) {
                                echo " 
\t\t\t         \t<span class=\"head\"><a style=\"float:right;padding-right:5px\" class=\"accordion-toggle ";
                                // line 33
                                if (((($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["child"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["category_id"] ?? null) : null) != ($context["child_id"] ?? null))) {
                                    echo "collapsed";
                                }
                                echo "\" data-toggle=\"collapse\" data-parent=\"#accordion-category2\" href=\"#category";
                                echo (($context["i"] ?? null) . (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = $context["child"]) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["category_id"] ?? null) : null));
                                echo "\"><span class=\"plus\">+</span><span class=\"minus\">-</span></a></span>
\t\t\t         \t<div id=\"category";
                                // line 34
                                echo (($context["i"] ?? null) . (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = $context["child"]) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["category_id"] ?? null) : null));
                                echo "\" class=\"panel-collapse collapse ";
                                if (((($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = $context["child"]) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["category_id"] ?? null) : null) == ($context["child_id"] ?? null))) {
                                    echo " ";
                                    echo "in";
                                    echo " ";
                                }
                                echo "\" style=\"clear:both\">
\t\t\t\t         \t<ul>
\t\t\t\t         \t";
                                // line 36
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(($context["categories_3"] ?? null));
                                foreach ($context['_seq'] as $context["_key"] => $context["subcategory"]) {
                                    echo " 
\t\t\t\t         \t\t";
                                    // line 37
                                    $context["link"] = ((((($context["first"] ?? null) . "_") . ($context["second"] ?? null)) . "_") . (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = $context["subcategory"]) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["category_id"] ?? null) : null));
                                    echo " 
\t\t\t\t         \t\t<li><a href=\"";
                                    // line 38
                                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "link", [0 => ($context["link"] ?? null)], "method", false, false, false, 38);
                                    echo "\" ";
                                    if ((($context["link"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getVariable", [0 => "path"], "method", false, false, false, 38))) {
                                        echo " ";
                                        echo "class=\"active\"";
                                        echo " ";
                                    }
                                    echo ">&nbsp; ";
                                    echo (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["subcategory"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["name"] ?? null) : null);
                                    echo "</a></li>
\t\t\t\t         \t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcategory'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 39
                                echo " 
\t\t\t\t         \t</ul>
\t\t\t\t         </div>
\t\t\t         ";
                            }
                            // line 42
                            echo " 
\t\t\t        </li>
\t\t\t       ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 44
                        echo " 
\t\t        </ul>
\t        </div>
\t        ";
                    }
                    // line 47
                    echo " 
\t        ";
                }
                // line 48
                echo " 
\t      </li>
\t      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo " 
\t    </ul>
\t  </div>
\t</div>
";
        }
        // line 54
        echo " ";
    }

    public function getTemplateName()
    {
        return "kofi/template/extension/module/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  272 => 54,  265 => 50,  257 => 48,  253 => 47,  247 => 44,  239 => 42,  233 => 39,  217 => 38,  213 => 37,  207 => 36,  196 => 34,  188 => 33,  184 => 32,  180 => 31,  177 => 30,  170 => 29,  167 => 28,  160 => 27,  156 => 26,  147 => 24,  136 => 22,  132 => 21,  122 => 20,  118 => 19,  114 => 18,  110 => 16,  103 => 15,  100 => 14,  93 => 13,  89 => 12,  84 => 10,  80 => 9,  72 => 8,  65 => 4,  45 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/extension/module/category.twig", "");
    }
}
