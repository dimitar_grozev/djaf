<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/common/success.twig */
class __TwigTemplate_5684ad1241b7f8b22575e504320a2393fa6a53534acca3e485a7db6757d6cc11 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $this->loadTemplate("kofi/template/new_elements/wrapper_top.twig", "kofi/template/common/success.twig", 2)->display($context);
        // line 3
        echo "
";
        // line 4
        if (array_key_exists("text_message", $context)) {
            echo " ";
            echo ($context["text_message"] ?? null);
        }
        echo " 
<div class=\"buttons\" style=\"padding-top: 10px\">
  <div class=\"pull-right\"><a href=\"";
        // line 6
        echo ($context["continue"] ?? null);
        echo "\" class=\"btn btn-primary\">";
        echo ($context["button_continue"] ?? null);
        echo "</a></div>
</div>
  
";
        // line 9
        $this->loadTemplate("kofi/template/new_elements/wrapper_bottom.twig", "kofi/template/common/success.twig", 9)->display($context);
        // line 10
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "kofi/template/common/success.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 10,  62 => 9,  54 => 6,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/common/success.twig", "");
    }
}
