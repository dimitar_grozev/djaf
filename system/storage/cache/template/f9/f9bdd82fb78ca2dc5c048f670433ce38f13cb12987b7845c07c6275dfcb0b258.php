<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/product/manufacturer_info.twig */
class __TwigTemplate_5ba87eb004c9eaf3ccb03630f18f47d4d6e7241c1ea99cdaf35bdeef7542033a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
        // line 3
        $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
        echo " 
";
        // line 4
        $context["background_status"] = twig_constant("false");
        // line 5
        $this->loadTemplate("kofi/template/new_elements/wrapper_top.twig", "kofi/template/product/manufacturer_info.twig", 5)->display($context);
        // line 6
        echo "
<div id=\"mfilter-content-container\">
  ";
        // line 8
        if (($context["products"] ?? null)) {
            echo " 
  ";
            // line 9
            $context["currently"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getCurrently", [], "method", false, false, false, 9);
            // line 10
            echo "  <!-- Filter -->
  <div class=\"product-filter clearfix\">
    <div class=\"options\">
      <div class=\"product-compare\"><a href=\"";
            // line 13
            echo ($context["compare"] ?? null);
            echo "\" id=\"compare-total\">";
            echo ($context["text_compare"] ?? null);
            echo "</a></div>
      
      <div class=\"button-group display\" data-toggle=\"buttons-radio\">
        <button id=\"grid\" ";
            // line 16
            if ((($context["currently"] ?? null) == "grid")) {
                echo " ";
                echo "class=\"active\"";
                echo " ";
            }
            echo " rel=\"tooltip\" title=\"Grid\" onclick=\"display('grid');\"><i class=\"fa fa-th-large\"></i></button>
        <button id=\"list\" ";
            // line 17
            if ((($context["currently"] ?? null) != "grid")) {
                echo " ";
                echo "class=\"active\"";
                echo " ";
            }
            echo " rel=\"tooltip\" title=\"List\" onclick=\"display('list');\"><i class=\"fa fa-th-list\"></i></button>
      </div>
    </div>
    
    <div class=\"list-options\">
      <div class=\"sort\">
        <select onchange=\"location = this.value;\">
          ";
            // line 24
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                echo " 
          ";
                // line 25
                if (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["sorts"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["value"] ?? null) : null) == ((($context["sort"] ?? null) . "-") . ($context["order"] ?? null)))) {
                    echo " 
          <option value=\"";
                    // line 26
                    echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["sorts"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["href"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["sorts"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["text"] ?? null) : null);
                    echo "</option>
          ";
                } else {
                    // line 27
                    echo " 
          <option value=\"";
                    // line 28
                    echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["sorts"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["sorts"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["text"] ?? null) : null);
                    echo "</option>
          ";
                }
                // line 29
                echo " 
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo " 
        </select>
      </div>
      
      <div class=\"limit\">
        <select onchange=\"location = this.value;\">
          ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                echo " 
          ";
                // line 37
                if (((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["limits"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["value"] ?? null) : null) == ($context["limit"] ?? null))) {
                    echo " 
          <option value=\"";
                    // line 38
                    echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["limits"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["href"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["limits"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["text"] ?? null) : null);
                    echo "</option>
          ";
                } else {
                    // line 39
                    echo " 
          <option value=\"";
                    // line 40
                    echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["limits"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["limits"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["text"] ?? null) : null);
                    echo "</option>
          ";
                }
                // line 41
                echo " 
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo " 
        </select>
      </div>
    </div>
  </div>
  
  ";
            // line 48
            if ((($context["currently"] ?? null) != "grid")) {
                echo " 
  <!-- Products list -->
  <div class=\"product-list active\">
    ";
                // line 51
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    echo " 
    <!-- Product -->
    <div>
      <div class=\"row\">
        <div class=\"image col-sm-3\">
          ";
                    // line 56
                    if (((($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["product"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["special"] ?? null) : null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_text_sale"], "method", false, false, false, 56) != "0"))) {
                        echo " 
            ";
                        // line 57
                        $context["text_sale"] = "Sale";
                        // line 58
                        echo "          ";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 58)], "method", false, false, false, 58) != "")) {
                            // line 59
                            echo "            ";
                            $context["text_sale"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 59)], "method", false, false, false, 59);
                            // line 60
                            echo "          ";
                        }
                        echo " 
            ";
                        // line 61
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "type_sale"], "method", false, false, false, 61) == "1")) {
                            echo " 
            ";
                            // line 62
                            $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = $context["product"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["product_id"] ?? null) : null)], "method", false, false, false, 62);
                            // line 63
                            echo "          ";
                            $context["roznica_ceny"] = ((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = ($context["product_detail"] ?? null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["price"] ?? null) : null) - (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = ($context["product_detail"] ?? null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["special"] ?? null) : null));
                            // line 64
                            echo "          ";
                            $context["procent"] = ((($context["roznica_ceny"] ?? null) * 100) / (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = ($context["product_detail"] ?? null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["price"] ?? null) : null));
                            echo " 
            <div class=\"sale\">-";
                            // line 65
                            echo twig_round(($context["procent"] ?? null));
                            echo "%</div>
            ";
                        } else {
                            // line 66
                            echo " 
            <div class=\"sale\">";
                            // line 67
                            echo ($context["text_sale"] ?? null);
                            echo "</div>
            ";
                        }
                        // line 68
                        echo " 
          ";
                    } elseif (((twig_get_attribute($this->env, $this->source,                     // line 69
($context["theme_options"] ?? null), "get", [0 => "display_text_new"], "method", false, false, false, 69) != "0") && twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "isLatestProduct", [0 => (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["product"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["product_id"] ?? null) : null)], "method", false, false, false, 69))) {
                        echo " 
             <div class=\"new\">";
                        // line 70
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "new_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 70)], "method", false, false, false, 70) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "new_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 70)], "method", false, false, false, 70);
                            echo " ";
                        } else {
                            echo " ";
                            echo "New";
                            echo " ";
                        }
                        echo "</div>
          ";
                    }
                    // line 71
                    echo " 
          
          ";
                    // line 73
                    if ((($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["product"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["thumb"] ?? null) : null)) {
                        echo " 
            ";
                        // line 74
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "lazy_loading_images"], "method", false, false, false, 74) != "0")) {
                            echo " 
            <a href=\"";
                            // line 75
                            echo (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = $context["product"]) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["href"] ?? null) : null);
                            echo "\"><img src=\"image/catalog/blank.gif\" data-echo=\"";
                            echo (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = $context["product"]) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["thumb"] ?? null) : null);
                            echo "\" alt=\"";
                            echo (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = $context["product"]) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["name"] ?? null) : null);
                            echo "\" /></a>
            ";
                        } else {
                            // line 76
                            echo " 
            <a href=\"";
                            // line 77
                            echo (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = $context["product"]) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["href"] ?? null) : null);
                            echo "\"><img src=\"";
                            echo (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["product"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["thumb"] ?? null) : null);
                            echo "\" alt=\"";
                            echo (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = $context["product"]) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["name"] ?? null) : null);
                            echo "\" /></a>
            ";
                        }
                        // line 78
                        echo " 
          ";
                    } else {
                        // line 79
                        echo " 
          <a href=\"";
                        // line 80
                        echo (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = $context["product"]) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["href"] ?? null) : null);
                        echo "\"><img src=\"image/no_image.jpg\" alt=\"";
                        echo (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = $context["product"]) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["name"] ?? null) : null);
                        echo "\" /></a>
          ";
                    }
                    // line 81
                    echo " 
          
          ";
                    // line 83
                    if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_specials_countdown"], "method", false, false, false, 83) == "1") && (($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = $context["product"]) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["special"] ?? null) : null))) {
                        echo " ";
                        $context["countdown"] = (twig_random($this->env, 5000) * twig_random($this->env, 50000));
                        echo " 
         ";
                        // line 84
                        $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = $context["product"]) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["product_id"] ?? null) : null)], "method", false, false, false, 84);
                        // line 85
                        echo "         ";
                        $context["date_end"] = (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = ($context["product_detail"] ?? null)) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["date_end"] ?? null) : null);
                        // line 86
                        echo "         ";
                        if (((($context["date_end"] ?? null) != "0000-00-00") && ($context["date_end"] ?? null))) {
                            echo " 
                  <script>
                  \$(function () {
                    var austDay = new Date();
                    austDay = new Date(";
                            // line 90
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "Y", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 90)], "method", false, false, false, 90);
                            echo ", ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "m", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 90)], "method", false, false, false, 90);
                            echo " - 1, ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "d", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 90)], "method", false, false, false, 90);
                            echo ");
                    \$('#countdown";
                            // line 91
                            echo ($context["countdown"] ?? null);
                            echo "').countdown({until: austDay});
                  });
                  </script>
                  <div id=\"countdown";
                            // line 94
                            echo ($context["countdown"] ?? null);
                            echo "\" class=\"clearfix\"></div>
                 ";
                        }
                        // line 95
                        echo " 
          ";
                    }
                    // line 96
                    echo " 
        </div>
        
        <div class=\"name-actions col-sm-4\">
             <div class=\"name\"><a href=\"";
                    // line 100
                    echo (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = $context["product"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = $context["product"]) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["name"] ?? null) : null);
                    echo "</a></div>
             ";
                    // line 101
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_list_type"], "method", false, false, false, 101) == "4")) {
                        echo " 
               ";
                        // line 102
                        $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = $context["product"]) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["product_id"] ?? null) : null)], "method", false, false, false, 102);
                        echo " 
               <div class=\"brand\">";
                        // line 103
                        echo (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = ($context["product_detail"] ?? null)) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["manufacturer"] ?? null) : null);
                        echo "</div>
             ";
                    }
                    // line 104
                    echo " 
             
             ";
                    // line 106
                    if ((($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = $context["product"]) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["price"] ?? null) : null)) {
                        echo " 
          <div class=\"price\">
            ";
                        // line 108
                        if ( !(($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = $context["product"]) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["special"] ?? null) : null)) {
                            echo " 
            ";
                            // line 109
                            echo (($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = $context["product"]) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["price"] ?? null) : null);
                            echo " 
            ";
                        } else {
                            // line 110
                            echo " 
            <span class=\"price-old\">";
                            // line 111
                            echo (($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = $context["product"]) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["price"] ?? null) : null);
                            echo "</span> <span class=\"price-new\">";
                            echo (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = $context["product"]) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["special"] ?? null) : null);
                            echo "</span>
            ";
                        }
                        // line 112
                        echo " 
          </div>
          ";
                    }
                    // line 114
                    echo " 
          
          <ul>
               ";
                    // line 117
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_cart"], "method", false, false, false, 117) != "0")) {
                        echo " 
                    ";
                        // line 118
                        $context["enquiry"] = twig_constant("false");
                        echo " ";
                        if ((twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "product_blocks_module"], "method", false, false, false, 118) != "")) {
                            echo " ";
                            $context["enquiry"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "productIsEnquiry", [0 => (($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = $context["product"]) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["product_id"] ?? null) : null)], "method", false, false, false, 118);
                            echo " ";
                        }
                        // line 119
                        echo "                  ";
                        if (twig_test_iterable(($context["enquiry"] ?? null))) {
                            echo " 
                    <li><a href=\"javascript:openPopup('";
                            // line 120
                            echo (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = ($context["enquiry"] ?? null)) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["popup_module"] ?? null) : null);
                            echo "', '";
                            echo (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = $context["product"]) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["product_id"] ?? null) : null);
                            echo "')\" data-toggle=\"tooltip\" data-original-title=\"";
                            echo (($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = ($context["enquiry"] ?? null)) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["block_name"] ?? null) : null);
                            echo "\"><i class=\"fa fa-question\"></i></a></li>
                    ";
                        } else {
                            // line 121
                            echo " 
                    <li><a onclick=\"cart.add('";
                            // line 122
                            echo (($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = $context["product"]) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["product_id"] ?? null) : null);
                            echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                            echo ($context["button_cart"] ?? null);
                            echo "\"><i class=\"fa fa-shopping-cart\"></i></a></li>
                    ";
                        }
                        // line 123
                        echo " 
               ";
                    }
                    // line 124
                    echo " 
               
               ";
                    // line 126
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quick_view"], "method", false, false, false, 126) == 1)) {
                        echo " 
               <li class=\"quickview\"><a href=\"index.php?route=product/quickview&amp;product_id=";
                        // line 127
                        echo (($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = $context["product"]) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["product_id"] ?? null) : null);
                        echo "\" data-toggle=\"tooltip\" data-original-title=\"";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quickview_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 127)], "method", false, false, false, 127) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "html_entity_decode", [0 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quickview_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 127)], "method", false, false, false, 127)], "method", false, false, false, 127);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Quickview";
                            echo " ";
                        }
                        echo "\"><i class=\"fa fa-search\"></i></a></li>
               ";
                    }
                    // line 128
                    echo " 
          
            ";
                    // line 130
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_compare"], "method", false, false, false, 130) != "0")) {
                        echo " 
            <li><a onclick=\"compare.add('";
                        // line 131
                        echo (($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = $context["product"]) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54["product_id"] ?? null) : null);
                        echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 131)], "method", false, false, false, 131) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 131)], "method", false, false, false, 131);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Add to compare";
                            echo " ";
                        }
                        echo "\"><i class=\"fa fa-exchange\"></i></a></li>
            ";
                    }
                    // line 132
                    echo " 
            
            ";
                    // line 134
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_wishlist"], "method", false, false, false, 134) != "0")) {
                        echo " 
            <li><a onclick=\"wishlist.add('";
                        // line 135
                        echo (($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = $context["product"]) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f["product_id"] ?? null) : null);
                        echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_wishlist_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 135)], "method", false, false, false, 135) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_wishlist_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 135)], "method", false, false, false, 135);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Add to wishlist";
                            echo " ";
                        }
                        echo "\"><i class=\"fa fa-heart\"></i></a></li>
            ";
                    }
                    // line 136
                    echo " 
          </ul>
        </div>
        
        <div class=\"desc col-sm-5\">
          <div class=\"description\">";
                    // line 141
                    echo (($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 = $context["product"]) && is_array($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327) || $__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 instanceof ArrayAccess ? ($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327["description"] ?? null) : null);
                    echo "</div>
        </div>
      </div>
    </div>
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 145
                echo " 
  </div>
  ";
            }
            // line 147
            echo " 
  
  ";
            // line 149
            if ((($context["currently"] ?? null) == "grid")) {
                echo " 
  <!-- Products grid -->
  
 ";
                // line 152
                $context["class"] = 3;
                echo " 
 ";
                // line 153
                $context["row"] = 4;
                echo " 
 
 ";
                // line 155
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 155) == 6)) {
                    echo " ";
                    $context["class"] = 2;
                    echo " ";
                }
                // line 156
                echo " ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 156) == 5)) {
                    echo " ";
                    $context["class"] = 25;
                    echo " ";
                }
                // line 157
                echo " ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 157) == 3)) {
                    echo " ";
                    $context["class"] = 4;
                    echo " ";
                }
                // line 158
                echo " 
 ";
                // line 159
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 159) > 1)) {
                    echo " ";
                    $context["row"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 159);
                    echo " ";
                }
                echo " 
 
  <div class=\"product-grid active\">
    <div class=\"row\">
        ";
                // line 163
                $context["row_fluid"] = 0;
                echo " ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    echo " ";
                    $context["row_fluid"] = (($context["row_fluid"] ?? null) + 1);
                    echo " 
          ";
                    // line 164
                    $context["r"] = (($context["row_fluid"] ?? null) - (twig_round((($context["row_fluid"] ?? null) / ($context["row"] ?? null)), 0, "floor") * ($context["row"] ?? null)));
                    echo " ";
                    if (((($context["row_fluid"] ?? null) > ($context["row"] ?? null)) && (($context["r"] ?? null) == 1))) {
                        echo " ";
                        echo "</div><div class=\"row\">";
                        echo " ";
                    }
                    echo " 
          <div class=\"col-sm-";
                    // line 165
                    echo ($context["class"] ?? null);
                    echo " col-xs-6\">
              ";
                    // line 166
                    $this->loadTemplate("kofi/template/new_elements/product.twig", "kofi/template/product/manufacturer_info.twig", 166)->display($context);
                    // line 167
                    echo "          </div>
        ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 168
                echo " 
    </div>
  </div>
  ";
            }
            // line 171
            echo " 
  
  <div class=\"row pagination-results\">
    <div class=\"col-sm-6 text-left\">";
            // line 174
            echo ($context["pagination"] ?? null);
            echo "</div>
    <div class=\"col-sm-6 text-right\">";
            // line 175
            echo ($context["results"] ?? null);
            echo "</div>
  </div>
  ";
        } else {
            // line 177
            echo " 
  <p style=\"padding-top: 30px\">";
            // line 178
            echo ($context["text_empty"] ?? null);
            echo "</p>
  <div class=\"buttons\">
    <div class=\"pull-right\"><a href=\"";
            // line 180
            echo ($context["continue"] ?? null);
            echo "\" class=\"btn btn-primary\">";
            echo ($context["button_continue"] ?? null);
            echo "</a></div>
  </div>
  ";
        }
        // line 182
        echo " 
<script type=\"text/javascript\"><!--
function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function display(view) {
  if (view == 'list') {
    setCookie('display','list');
    location.reload();
  } else {
    setCookie('display','grid');
    location.reload();
  }
}
//--></script> 

</div>

";
        // line 203
        $this->loadTemplate("kofi/template/new_elements/wrapper_bottom.twig", "kofi/template/product/manufacturer_info.twig", 203)->display($context);
        // line 204
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "kofi/template/product/manufacturer_info.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  717 => 204,  715 => 203,  692 => 182,  684 => 180,  679 => 178,  676 => 177,  670 => 175,  666 => 174,  661 => 171,  655 => 168,  640 => 167,  638 => 166,  634 => 165,  624 => 164,  601 => 163,  590 => 159,  587 => 158,  580 => 157,  573 => 156,  567 => 155,  562 => 153,  558 => 152,  552 => 149,  548 => 147,  543 => 145,  532 => 141,  525 => 136,  510 => 135,  506 => 134,  502 => 132,  487 => 131,  483 => 130,  479 => 128,  464 => 127,  460 => 126,  456 => 124,  452 => 123,  445 => 122,  442 => 121,  433 => 120,  428 => 119,  420 => 118,  416 => 117,  411 => 114,  406 => 112,  399 => 111,  396 => 110,  391 => 109,  387 => 108,  382 => 106,  378 => 104,  373 => 103,  369 => 102,  365 => 101,  359 => 100,  353 => 96,  349 => 95,  344 => 94,  338 => 91,  330 => 90,  322 => 86,  319 => 85,  317 => 84,  311 => 83,  307 => 81,  300 => 80,  297 => 79,  293 => 78,  284 => 77,  281 => 76,  272 => 75,  268 => 74,  264 => 73,  260 => 71,  247 => 70,  243 => 69,  240 => 68,  235 => 67,  232 => 66,  227 => 65,  222 => 64,  219 => 63,  217 => 62,  213 => 61,  208 => 60,  205 => 59,  202 => 58,  200 => 57,  196 => 56,  186 => 51,  180 => 48,  172 => 42,  165 => 41,  158 => 40,  155 => 39,  148 => 38,  144 => 37,  138 => 36,  130 => 30,  123 => 29,  116 => 28,  113 => 27,  106 => 26,  102 => 25,  96 => 24,  82 => 17,  74 => 16,  66 => 13,  61 => 10,  59 => 9,  55 => 8,  51 => 6,  49 => 5,  47 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/product/manufacturer_info.twig", "");
    }
}
