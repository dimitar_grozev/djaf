<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/megamenu.twig */
class __TwigTemplate_61f3e9649337efdb5e7dcda0214fe7eed094ec3dad323885e22886833cb5b532 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo " 
<div id=\"content\"><div class=\"container-fluid\">
\t<div class=\"page-header\">
\t    <h1>MegaMenu</h1>
\t    <ul class=\"breadcrumb\">
\t\t     ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
\t\t      <li><a href=\"";
            // line 7
            echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["breadcrumb"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["href"] ?? null) : null);
            echo "\">";
            echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["breadcrumb"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["text"] ?? null) : null);
            echo "</a></li>
\t\t      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo " 
\t    </ul>
\t  </div>
    
     <link rel=\"stylesheet\" type=\"text/css\" href=\"view/stylesheet/css/colorpicker.css\" />
     <script type=\"text/javascript\" src=\"view/javascript/jquery/colorpicker.js\"></script>
\t<link href='https://fonts.googleapis.com/css?family=Poppins:700,600,500,400,300' rel='stylesheet' type='text/css'>
\t
\t<script type=\"text/javascript\">
\t\$.fn.tabs = function() {
\t\tvar selector = this;
\t\t
\t\tthis.each(function() {
\t\t\tvar obj = \$(this); 
\t\t\t
\t\t\t\$(obj.attr('href')).hide();
\t\t\t
\t\t\t\$(obj).click(function() {
\t\t\t\t\$(selector).removeClass('selected');
\t\t\t\t
\t\t\t\t\$(selector).each(function(i, element) {
\t\t\t\t\t\$(\$(element).attr('href')).hide();
\t\t\t\t});
\t\t\t\t
\t\t\t\t\$(this).addClass('selected');
\t\t\t\t
\t\t\t\t\$(\$(this).attr('href')).show();
\t\t\t\t
\t\t\t\treturn false;
\t\t\t});
\t\t});
\t
\t\t\$(this).show();
\t\t
\t\t\$(this).first().click();
\t};
\t</script>
\t
\t";
        // line 46
        if (($context["error_warning"] ?? null)) {
            echo " 
\t\t<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            // line 47
            echo ($context["error_warning"] ?? null);
            echo " 
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t";
        } elseif (        // line 50
($context["success"] ?? null)) {
            echo " 
\t\t<div class=\"alert alert-success\"><i class=\"fa fa-exclamation-circle\"></i> ";
            // line 51
            echo ($context["success"] ?? null);
            echo " 
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t";
        }
        // line 54
        echo " 
\t
\t<div class=\"set-size\" id=\"megamenu\">
\t\t<form action=\"";
        // line 57
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\">
\t\t\t<!-- Unlimited MegaMenu Modules -->
\t\t\t<div class=\"content theme-skins\">
\t\t\t\t<div>
\t\t\t\t\t<ul class=\"skins\">
\t\t\t\t\t\t<li><p>Edited Module: <br><span>";
        // line 62
        echo ($context["active_module"] ?? null);
        echo "</span></p></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<select name=\"megamenu_modules\">
\t\t\t\t\t\t\t\t";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["megamenu_modules"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            echo " 
\t\t\t\t\t\t\t\t<option  style=\"font-size: 12px !important\" ";
            // line 66
            if ((($context["active_module_id"] ?? null) == (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["module"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["id"] ?? null) : null))) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo " value=\"";
            echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["module"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["id"] ?? null) : null);
            echo "\">";
            echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["module"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["name"] ?? null) : null);
            echo "</option>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo " 
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li><input type=\"submit\" name=\"active-module\" class=\"button-active\"></li>
\t\t\t\t\t\t<li><a onclick=\"#\" class=\"button-add\"><span>Add</span></a><div class=\"add-skin\"><input type=\"text\" name=\"add-module-name\" class=\"add-skin-name\" value=\"\"><input type=\"submit\" name=\"add-module\" value=\"Add module\" class=\"button-add-skin\"></div></li>
\t\t\t\t\t\t<li><input type=\"submit\" name=\"delete-module\" class=\"button-delete\" onclick=\"return confirm('Are you sure you want to delete?')\"></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<script type=\"text/javascript\">
\t\t\t\t\$(document).ready(function (){
\t\t\t\t\t\$(\".button-add\").click(function() {
\t\t\t\t\t\t\$(\".add-skin\").show();
\t\t\t\t\t\treturn false;
\t\t\t\t\t});
\t\t\t\t});
\t\t\t</script>
\t\t\t
\t\t\t<div class=\"content\">
\t\t\t\t<div>
\t\t\t\t\t<div class=\"background clearfix\">
\t\t\t\t\t\t<div class=\"left\">
\t\t\t\t\t\t\t<a href=\"";
        // line 90
        echo ($context["action"] ?? null);
        echo "&action=create\" class=\"create-new-item\"></a>
\t\t\t\t\t\t\t";
        // line 91
        echo ($context["nestable_list"] ?? null);
        echo " 
\t\t\t\t\t\t\t<div id='sortDBfeedback' style=\"font-size: 10px; font-weight: 600; text-transform: uppercase;padding: 15px 0px 0px 0px\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 95
        if (((($context["action_type"] ?? null) == "create") || (($context["action_type"] ?? null) == "edit"))) {
            echo " 
\t\t\t\t\t\t<div class=\"right\">
\t\t\t\t\t\t\t";
            // line 97
            if ((($context["action_type"] ?? null) == "edit")) {
                echo " 
\t\t\t\t\t\t\t<h4>Edit item (ID: ";
                // line 98
                echo ($context["get_edit"] ?? null);
                echo ")</h4>
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"id\" value=\"";
                // line 99
                echo ($context["get_edit"] ?? null);
                echo "\" />
\t\t\t\t\t\t\t";
            } else {
                // line 100
                echo " 
\t\t\t\t\t\t\t<h4>Create new item</h4>
\t\t\t\t\t\t\t";
            }
            // line 102
            echo " 
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Name</p>
\t\t\t\t\t\t\t\t<div class=\"list-language\">
\t\t\t\t\t\t\t\t";
            // line 107
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t\t\t\t<div class=\"language\">
\t\t\t\t\t\t\t\t\t";
                // line 109
                $context["language_id"] = (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["language"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["language_id"] ?? null) : null);
                echo " 
\t\t\t\t\t\t\t\t\t<img src=\"language/";
                // line 110
                echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["language"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["code"] ?? null) : null);
                echo "/";
                echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["language"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["code"] ?? null) : null);
                echo ".png\" alt=\"";
                echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["language"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["name"] ?? null) : null);
                echo "\" width=\"16px\" height=\"11px\" />
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"name[";
                // line 111
                echo ($context["language_id"] ?? null);
                echo "]\" ";
                if (twig_get_attribute($this->env, $this->source, ($context["name"] ?? null), ($context["language_id"] ?? null), [], "array", true, true, false, 111)) {
                    echo " ";
                    echo (("value=\"" . (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["name"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386[($context["language_id"] ?? null)] ?? null) : null)) . "\"");
                    echo " ";
                }
                echo ">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 113
            echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Description</p>
\t\t\t\t\t\t\t\t<div class=\"list-language\">
\t\t\t\t\t\t\t\t";
            // line 121
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t\t\t\t<div class=\"language\">
\t\t\t\t\t\t\t\t\t";
                // line 123
                $context["language_id"] = (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["language"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["language_id"] ?? null) : null);
                echo " 
\t\t\t\t\t\t\t\t\t<img src=\"language/";
                // line 124
                echo (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = $context["language"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["code"] ?? null) : null);
                echo "/";
                echo (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = $context["language"]) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["code"] ?? null) : null);
                echo ".png\" alt=\"";
                echo (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["language"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["name"] ?? null) : null);
                echo "\" width=\"16px\" height=\"11px\" />
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"description[";
                // line 125
                echo ($context["language_id"] ?? null);
                echo "]\" ";
                if (twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), ($context["language_id"] ?? null), [], "array", true, true, false, 125)) {
                    echo " ";
                    echo (("value=\"" . (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = ($context["description"] ?? null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f[($context["language_id"] ?? null)] ?? null) : null)) . "\"");
                    echo " ";
                }
                echo ">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 127
            echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Label</p>
\t\t\t\t\t\t\t\t<div class=\"list-language\">
\t\t\t\t\t\t\t\t";
            // line 135
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t\t\t\t<div class=\"language\">
\t\t\t\t\t\t\t\t\t";
                // line 137
                $context["language_id"] = (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["language"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["language_id"] ?? null) : null);
                echo " 
\t\t\t\t\t\t\t\t\t<img src=\"language/";
                // line 138
                echo (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["language"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["code"] ?? null) : null);
                echo "/";
                echo (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = $context["language"]) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["code"] ?? null) : null);
                echo ".png\" alt=\"";
                echo (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = $context["language"]) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["name"] ?? null) : null);
                echo "\" width=\"16px\" height=\"11px\" />
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"label[";
                // line 139
                echo ($context["language_id"] ?? null);
                echo "]\" ";
                if (twig_get_attribute($this->env, $this->source, ($context["label"] ?? null), ($context["language_id"] ?? null), [], "array", true, true, false, 139)) {
                    echo (("value=\"" . (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = ($context["label"] ?? null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972[($context["language_id"] ?? null)] ?? null) : null)) . "\"");
                    echo " ";
                }
                echo ">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 141
            echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Label text color</p>
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"label_text_color\" id=\"label_text_color\" value=\"";
            // line 148
            if (array_key_exists("label_text_color", $context)) {
                echo ($context["label_text_color"] ?? null);
            }
            echo "\">
\t\t\t\t\t\t\t\t<div class=\"color_selected\" ";
            // line 149
            if (array_key_exists("label_text_color", $context)) {
                echo " ";
                if ((($context["label_text_color"] ?? null) != "")) {
                    echo (("style=\"background: " . ($context["label_text_color"] ?? null)) . "\"");
                    echo " ";
                }
                echo " ";
            }
            echo "></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\$(document).ready(function() {
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\$('#label_text_color').ColorPicker({
\t\t\t\t\t\t\t\t\tonChange: function (hsb, hex, rgb, el) {
\t\t\t\t\t\t\t\t\t\t\$(el).val(\"#\" +hex);
\t\t\t\t\t\t\t\t\t\t\$(el).next('.color_selected').css(\"background\", \"#\" + hex);
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tonShow: function (colpkr) {
\t\t\t\t\t\t\t\t\t\t\$(colpkr).show();
\t\t\t\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tonHide: function (colpkr) {
\t\t\t\t\t\t\t\t\t\t\$(colpkr).hide();
\t\t\t\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Label background color</p>
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"label_background_color\" id=\"label_background_color\" value=\"";
            // line 176
            if (array_key_exists("label_background_color", $context)) {
                echo ($context["label_background_color"] ?? null);
            }
            echo "\">
\t\t\t\t\t\t\t\t<div class=\"color_selected\" ";
            // line 177
            if (array_key_exists("label_background_color", $context)) {
                echo " ";
                if ((($context["label_background_color"] ?? null) != "")) {
                    echo (("style=\"background: " . ($context["label_background_color"] ?? null)) . "\"");
                    echo " ";
                }
                echo " ";
            }
            echo "></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\$(document).ready(function() {
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\$('#label_background_color').ColorPicker({
\t\t\t\t\t\t\t\t\tonChange: function (hsb, hex, rgb, el) {
\t\t\t\t\t\t\t\t\t\t\$(el).val(\"#\" +hex);
\t\t\t\t\t\t\t\t\t\t\$(el).next('.color_selected').css(\"background\", \"#\" + hex);
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tonShow: function (colpkr) {
\t\t\t\t\t\t\t\t\t\t\$(colpkr).show();
\t\t\t\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tonHide: function (colpkr) {
\t\t\t\t\t\t\t\t\t\t\$(colpkr).hide();
\t\t\t\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Custom class</p>
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"custom_class\" id=\"custom_class\" value=\"";
            // line 204
            if (array_key_exists("custom_class", $context)) {
                echo ($context["custom_class"] ?? null);
            }
            echo "\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Icon</p>
\t\t\t\t\t\t\t\t<div class=\"own_image clearfix\">
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"icon\" value=\"";
            // line 211
            echo ($context["icon"] ?? null);
            echo "\" id=\"input-icon_img_preview\" />
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
            // line 213
            if ((($context["icon"] ?? null) == "")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<a href=\"\" id=\"thumb-icon_img_preview\" class=\"img-thumbnail img-edit\" data-toggle=\"image\"><img src=\"";
                // line 214
                echo ($context["placeholder"] ?? null);
                echo "\" alt=\"\" title=\"\" data-placeholder=\"";
                echo ($context["placeholder"] ?? null);
                echo "\" /></a>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 215
                echo " 
\t\t\t\t\t\t\t\t\t\t<a href=\"\" id=\"thumb-icon_img_preview\" class=\"img-thumbnail img-edit\" data-toggle=\"image\"><img src=\"../image/";
                // line 216
                echo ($context["icon"] ?? null);
                echo "\" data-placeholder=\"";
                echo ($context["placeholder"] ?? null);
                echo "\" alt=\"\" /></a>
\t\t\t\t\t\t\t\t\t";
            }
            // line 217
            echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Links</p>
\t\t\t\t\t\t\t\t<div class=\"list-language\">
\t\t\t\t\t\t\t\t";
            // line 225
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t\t\t\t<div class=\"language\">
\t\t\t\t\t\t\t\t\t";
                // line 227
                $context["language_id"] = (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = $context["language"]) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["language_id"] ?? null) : null);
                echo " 
\t\t\t\t\t\t\t\t\t<img src=\"language/";
                // line 228
                echo (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["language"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["code"] ?? null) : null);
                echo "/";
                echo (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = $context["language"]) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["code"] ?? null) : null);
                echo ".png\" alt=\"";
                echo (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = $context["language"]) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["name"] ?? null) : null);
                echo "\" width=\"16px\" height=\"11px\" />
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"link[";
                // line 229
                echo ($context["language_id"] ?? null);
                echo "]\" ";
                if ((twig_get_attribute($this->env, $this->source, ($context["link"] ?? null), ($context["language_id"] ?? null), [], "array", true, true, false, 229) && twig_test_iterable(($context["link"] ?? null)))) {
                    echo " ";
                    echo (("value=\"" . (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = ($context["link"] ?? null)) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc[($context["language_id"] ?? null)] ?? null) : null)) . "\"");
                    echo " ";
                } elseif ((array_key_exists("link", $context) &&  !twig_test_iterable(($context["link"] ?? null)))) {
                    echo " ";
                    echo (("value=\"" . ($context["link"] ?? null)) . "\"");
                    echo " ";
                }
                echo ">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 231
            echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Link in new window</p>
\t\t\t\t\t\t\t\t<select name=\"new_window\">
\t\t\t\t\t\t\t\t\t";
            // line 239
            if ((($context["new_window"] ?? null) == 1)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\">disabled</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">enabled</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 242
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">disabled</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">enabled</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 245
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Status</p>
\t\t\t\t\t\t\t\t<select name=\"status\">
\t\t\t\t\t\t\t\t\t";
            // line 253
            if ((($context["status"] ?? null) == 1)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\">enabled</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">disabled</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 256
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">enabled</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">disabled</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 259
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Display on mobile</p>
\t\t\t\t\t\t\t\t<select name=\"display_on_mobile\">
\t\t\t\t\t\t\t\t\t";
            // line 267
            if ((($context["display_on_mobile"] ?? null) == 1)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\">yes</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">no</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 270
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">yes</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">no</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 273
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Position</p>
\t\t\t\t\t\t\t\t<select name=\"position\">
\t\t\t\t\t\t\t\t\t";
            // line 281
            if ((($context["position"] ?? null) == 1)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\">Left</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">Right</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 284
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">Left</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">Right</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 287
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Submenu width</p>
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"submenu_width\" value=\"";
            // line 294
            echo ($context["submenu_width"] ?? null);
            echo "\"> <div>Example: 100%, 260px</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Display submenu on</p>
\t\t\t\t\t\t\t\t<select name=\"display_submenu\">
\t\t\t\t\t\t\t\t\t";
            // line 301
            if ((($context["display_submenu"] ?? null) == "0")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">Hover</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 303
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\">Hover</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 305
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 306
            if ((($context["display_submenu"] ?? null) == "2")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"2\" selected=\"selected\">Hover intent</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 308
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"2\">Hover intent</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 310
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 311
            if ((($context["display_submenu"] ?? null) == "1")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">Click</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 313
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"1\">Click</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 315
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Submenu background</p>
\t\t\t\t\t\t\t\t<div class=\"own_image clearfix\">
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"submenu_background\" value=\"";
            // line 323
            echo ($context["submenu_background"] ?? null);
            echo "\" id=\"input-submenu_background\" />
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
            // line 325
            if ((($context["submenu_background"] ?? null) == "")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<a href=\"\" id=\"thumb-submenu_background\" class=\"img-thumbnail img-edit\" data-toggle=\"image\"><img src=\"";
                // line 326
                echo ($context["placeholder"] ?? null);
                echo "\" alt=\"\" title=\"\" data-placeholder=\"";
                echo ($context["placeholder"] ?? null);
                echo "\" /></a>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 327
                echo " 
\t\t\t\t\t\t\t\t\t\t<a href=\"\" id=\"thumb-submenu_background\" class=\"img-thumbnail img-edit\" data-toggle=\"image\"><img src=\"../image/";
                // line 328
                echo ($context["submenu_background"] ?? null);
                echo "\" data-placeholder=\"";
                echo ($context["placeholder"] ?? null);
                echo "\" alt=\"\" /></a>
\t\t\t\t\t\t\t\t\t";
            }
            // line 329
            echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Submenu background position</p>
\t\t\t\t\t\t\t\t<select name=\"submenu_background_position\">
\t\t\t\t\t\t\t\t\t<option value=\"top left\"";
            // line 338
            if ((($context["submenu_background_position"] ?? null) == "top left")) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo ">Top left</option>
\t\t\t\t\t\t\t\t\t<option value=\"top center\"";
            // line 339
            if ((($context["submenu_background_position"] ?? null) == "top center")) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo ">Top center</option>
\t\t\t\t\t\t\t\t\t<option value=\"top right\"";
            // line 340
            if ((($context["submenu_background_position"] ?? null) == "top right")) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo ">Top right</option>
\t\t\t\t\t\t\t\t\t<option value=\"bottom left\"";
            // line 341
            if ((($context["submenu_background_position"] ?? null) == "bottom left")) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo ">Bottom left</option>
\t\t\t\t\t\t\t\t\t<option value=\"bottom center\"";
            // line 342
            if ((($context["submenu_background_position"] ?? null) == "bottom center")) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo ">Bottom center</option>
\t\t\t\t\t\t\t\t\t<option value=\"bottom right\"";
            // line 343
            if ((($context["submenu_background_position"] ?? null) == "bottom right")) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo ">Bottom right</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- End Input -->
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->\t
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Submenu background repeat</p>
\t\t\t\t\t\t\t\t<select name=\"submenu_background_repeat\">
\t\t\t\t\t\t\t\t\t<option value=\"no-repeat\"";
            // line 352
            if ((($context["submenu_background_repeat"] ?? null) == "no-repeat")) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo ">no-repeat</option>
\t\t\t\t\t\t\t\t\t<option value=\"repeat-x\"";
            // line 353
            if ((($context["submenu_background_repeat"] ?? null) == "repeat-x")) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo ">repeat-x</option>
\t\t\t\t\t\t\t\t\t<option value=\"repeat-y\"";
            // line 354
            if ((($context["submenu_background_repeat"] ?? null) == "repeat-y")) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo ">repeat-y</option>
\t\t\t\t\t\t\t\t\t<option value=\"repeat\"";
            // line 355
            if ((($context["submenu_background_repeat"] ?? null) == "repeat")) {
                echo " ";
                echo " selected=\"selected\"";
                echo " ";
            }
            echo ">repeat</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- End Input -->
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<h5 style=\"margin-top:20px\">Content item - content of these fields will only be displayed if the menu be set as submenu.</h5>
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Content width</p>
\t\t\t\t\t\t\t\t<select name=\"content_width\">
\t\t\t\t\t\t\t\t\t";
            // line 365
            $context["i"] = 1;
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range($context["i"], 13));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"";
                // line 366
                echo $context["i"];
                echo "\" ";
                if (($context["i"] == ($context["content_width"] ?? null))) {
                    echo " ";
                    echo "selected=\"selected\"";
                    echo " ";
                }
                echo ">";
                echo $context["i"];
                echo "/12</option>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 367
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Content type</p>
\t\t\t\t\t\t\t\t<select name=\"content_type\">
\t\t\t\t\t\t\t\t\t";
            // line 375
            if ((($context["content_type"] ?? null) != "0")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\">HTML</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 377
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">HTML</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 379
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 380
            if ((($context["content_type"] ?? null) != "1")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"1\">Product</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 382
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">Product</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 384
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 385
            if ((($context["content_type"] ?? null) != "2")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"2\">Links</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 387
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"2\" selected=\"selected\">Links</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 389
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 390
            if ((($context["content_type"] ?? null) != "3")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"3\">Products</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 392
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"3\" selected=\"selected\">Products</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 394
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div id=\"content_type0\"";
            // line 398
            if ((($context["content_type"] ?? null) != "0")) {
                echo " style=\"display:none\"";
            }
            echo " class=\"content_type\">
\t\t\t\t\t\t\t\t<h5 style=\"margin-top:20px\">HTML</h5>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"htmltabs htabs\">
\t\t\t\t\t\t\t\t";
            // line 402
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t\t\t\t<a href=\"#content_html_";
                // line 403
                echo (($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = $context["language"]) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["language_id"] ?? null) : null);
                echo "\"><img src=\"language/";
                echo (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = $context["language"]) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["code"] ?? null) : null);
                echo "/";
                echo (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = $context["language"]) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["code"] ?? null) : null);
                echo ".png\" title=\"";
                echo (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = $context["language"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["name"] ?? null) : null);
                echo "\" /> ";
                echo (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = $context["language"]) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["name"] ?? null) : null);
                echo "</a>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 404
            echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
            // line 407
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " ";
                $context["lang_id"] = (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = $context["language"]) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["language_id"] ?? null) : null);
                echo " 
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div id=\"content_html_";
                // line 409
                echo (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = $context["language"]) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["language_id"] ?? null) : null);
                echo "\" class=\"content_html\">
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\" style=\"padding-top: 7px\">
\t\t\t\t\t\t\t\t\t\t<textarea name=\"content[html][text][";
                // line 411
                echo (($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = $context["language"]) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["language_id"] ?? null) : null);
                echo "]\" id=\"content_html_text_";
                echo (($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = $context["language"]) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["language_id"] ?? null) : null);
                echo "\" style=\"float: none;width: 100%;max-width: 100%;height: 300px\">";
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "html", [], "array", false, true, false, 411), "text", [], "array", false, true, false, 411), ($context["lang_id"] ?? null), [], "array", true, true, false, 411)) {
                    echo (($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = (($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = ($context["content"] ?? null)) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["html"] ?? null) : null)) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["text"] ?? null) : null)) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f[($context["lang_id"] ?? null)] ?? null) : null);
                }
                echo "</textarea>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 414
            echo " 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div id=\"content_type1\"";
            // line 417
            if ((($context["content_type"] ?? null) != "1")) {
                echo " style=\"display:none\"";
            }
            echo " class=\"content_type\">
\t\t\t\t\t\t\t\t<h5 style=\"margin-top:20px\">Product</h5>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Products:<br><span style=\"font-size:11px;color:#808080\">(Autocomplete)</span></p>
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"content[product][id]\" value=\"";
            // line 423
            echo (($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = ($context["content"] ?? null)) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["product"] ?? null) : null)) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["id"] ?? null) : null);
            echo "\" />
\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"product_autocomplete\" name=\"content[product][name]\" value=\"";
            // line 424
            echo (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = (($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = ($context["content"] ?? null)) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["product"] ?? null) : null)) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["name"] ?? null) : null);
            echo "\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Dimension product image:<br><span style=\"font-size:11px;color:#808080\">(W x H)</span></p>
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"content[product][width]\" value=\"";
            // line 430
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "product", [], "array", false, true, false, 430), "width", [], "array", true, true, false, 430)) {
                echo (($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = (($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = ($context["content"] ?? null)) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["product"] ?? null) : null)) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["width"] ?? null) : null);
            }
            echo "\" style=\"width: 60px; margin-right: 0px\">
\t\t\t\t\t\t\t\t\t<span style=\"display: block;float: left; padding:7px 15px 0px 7px\">px</span>
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"content[product][height]\" value=\"";
            // line 432
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "product", [], "array", false, true, false, 432), "height", [], "array", true, true, false, 432)) {
                echo (($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = (($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = ($context["content"] ?? null)) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f["product"] ?? null) : null)) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54["height"] ?? null) : null);
            }
            echo "\" style=\"width: 60px; margin-right: 0px\">
\t\t\t\t\t\t\t\t\t<span style=\"display: block;float: left; padding:7px 15px 0px 7px\">px</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div id=\"content_type2\"";
            // line 437
            if ((($context["content_type"] ?? null) != "2")) {
                echo " style=\"display:none\"";
            }
            echo " class=\"content_type\">
\t\t\t\t\t\t\t\t<h5 style=\"margin-top:20px\">Links</h5>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Add categories<br><span style=\"font-size:11px;color:#808080\">(Autocomplete)</span></p>
\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"categories_autocomplete\" value=\"\">\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Add links<br><span style=\"font-size:11px;color:#808080\">(Autocomplete)</span></p>
\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"links_autocomplete\" value=\"\">\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Sort categories, links</p>
\t\t\t\t\t\t\t\t\t<div class=\"cf nestable-lists\">
\t\t\t\t\t\t\t\t\t\t<div class=\"dd\" id=\"sort_categories\">
\t\t\t\t\t\t\t\t\t\t    <ol class=\"dd-list\">
\t\t\t\t\t\t\t\t\t\t    \t";
            // line 458
            echo ($context["list_categories"] ?? null);
            echo " 
\t\t\t\t\t\t\t\t\t\t    </ol>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"sort_categories_data\" name=\"content[categories][categories]\" value=\"";
            // line 461
            echo (($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 = (($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 = ($context["content"] ?? null)) && is_array($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412) || $__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 instanceof ArrayAccess ? ($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412["categories"] ?? null) : null)) && is_array($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327) || $__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 instanceof ArrayAccess ? ($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327["categories"] ?? null) : null);
            echo "\" />
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Columns</p>
\t\t\t\t\t\t\t\t\t<select name=\"content[categories][columns]\">
\t\t\t\t\t\t\t\t\t\t";
            // line 469
            if (((($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 = (($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e = ($context["content"] ?? null)) && is_array($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e) || $__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e instanceof ArrayAccess ? ($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e["categories"] ?? null) : null)) && is_array($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9) || $__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 instanceof ArrayAccess ? ($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9["columns"] ?? null) : null) != "1")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"1\">1</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 471
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">1</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 473
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 474
            if (((($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 = (($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a = ($context["content"] ?? null)) && is_array($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a) || $__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a instanceof ArrayAccess ? ($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a["categories"] ?? null) : null)) && is_array($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5) || $__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 instanceof ArrayAccess ? ($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5["columns"] ?? null) : null) != "2")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"2\">2</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 476
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" selected=\"selected\">2</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 478
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 479
            if (((($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 = (($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d = ($context["content"] ?? null)) && is_array($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d) || $__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d instanceof ArrayAccess ? ($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d["categories"] ?? null) : null)) && is_array($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4) || $__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 instanceof ArrayAccess ? ($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4["columns"] ?? null) : null) != "3")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"3\">3</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 481
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" selected=\"selected\">3</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 483
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 484
            if (((($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 = (($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a = ($context["content"] ?? null)) && is_array($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a) || $__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a instanceof ArrayAccess ? ($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a["categories"] ?? null) : null)) && is_array($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5) || $__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 instanceof ArrayAccess ? ($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5["columns"] ?? null) : null) != "4")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"4\">4</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 486
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"4\" selected=\"selected\">4</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 488
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 489
            if (((($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da = (($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 = ($context["content"] ?? null)) && is_array($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38) || $__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 instanceof ArrayAccess ? ($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38["categories"] ?? null) : null)) && is_array($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da) || $__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da instanceof ArrayAccess ? ($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da["columns"] ?? null) : null) != "5")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"5\">5</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 491
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"5\" selected=\"selected\">5</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 493
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 494
            if (((($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec = (($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574 = ($context["content"] ?? null)) && is_array($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574) || $__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574 instanceof ArrayAccess ? ($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574["categories"] ?? null) : null)) && is_array($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec) || $__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec instanceof ArrayAccess ? ($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec["columns"] ?? null) : null) != "6")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"6\">6</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 496
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"6\" selected=\"selected\">6</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 498
            echo " 
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\" id=\"submenu-type\">
\t\t\t\t\t\t\t\t\t<p>Submenu type</p>
\t\t\t\t\t\t\t\t\t<select name=\"content[categories][submenu]\">
\t\t\t\t\t\t\t\t\t\t";
            // line 506
            if (((($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c = (($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0 = ($context["content"] ?? null)) && is_array($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0) || $__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0 instanceof ArrayAccess ? ($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0["categories"] ?? null) : null)) && is_array($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c) || $__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c instanceof ArrayAccess ? ($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c["submenu"] ?? null) : null) != "1")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"1\">Hover</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 508
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">Hover</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 510
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 511
            if (((($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc = (($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd = ($context["content"] ?? null)) && is_array($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd) || $__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd instanceof ArrayAccess ? ($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd["categories"] ?? null) : null)) && is_array($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc) || $__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc instanceof ArrayAccess ? ($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc["submenu"] ?? null) : null) != "2")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"2\">Visible</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 513
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" selected=\"selected\">Visible</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 515
            echo " 
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix submenu-columns\" ";
            // line 520
            if (((($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81 = (($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007 = ($context["content"] ?? null)) && is_array($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007) || $__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007 instanceof ArrayAccess ? ($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007["categories"] ?? null) : null)) && is_array($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81) || $__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81 instanceof ArrayAccess ? ($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81["submenu"] ?? null) : null) != "2")) {
                echo " ";
                echo "style=\"display:none\"";
                echo " ";
            }
            echo ">
\t\t\t\t\t\t\t\t\t<p>Image position</p>
\t\t\t\t\t\t\t\t\t<select name=\"content[categories][image_position]\">
\t\t\t\t\t\t\t\t\t     ";
            // line 523
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "categories", [], "array", false, true, false, 523), "image_position", [], "array", true, true, false, 523)) {
                echo " 
     \t\t\t\t\t\t\t\t\t\t";
                // line 524
                if (((($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d = (($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba = ($context["content"] ?? null)) && is_array($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba) || $__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba instanceof ArrayAccess ? ($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba["categories"] ?? null) : null)) && is_array($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d) || $__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d instanceof ArrayAccess ? ($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d["image_position"] ?? null) : null) != "1")) {
                    echo " 
     \t\t\t\t\t\t\t\t\t\t<option value=\"1\">Disabled</option>
     \t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 526
                    echo " 
     \t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">Disabled</option>
     \t\t\t\t\t\t\t\t\t\t";
                }
                // line 528
                echo " 
     \t\t\t\t\t\t\t\t\t\t";
                // line 529
                if (((($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49 = (($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639 = ($context["content"] ?? null)) && is_array($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639) || $__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639 instanceof ArrayAccess ? ($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639["categories"] ?? null) : null)) && is_array($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49) || $__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49 instanceof ArrayAccess ? ($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49["image_position"] ?? null) : null) != "2")) {
                    echo " 
     \t\t\t\t\t\t\t\t\t\t<option value=\"2\">Top</option>
     \t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 531
                    echo " 
     \t\t\t\t\t\t\t\t\t\t<option value=\"2\" selected=\"selected\">Top</option>
     \t\t\t\t\t\t\t\t\t\t";
                }
                // line 533
                echo " 
     \t\t\t\t\t\t\t\t\t\t";
                // line 534
                if (((($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf = (($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921 = ($context["content"] ?? null)) && is_array($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921) || $__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921 instanceof ArrayAccess ? ($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921["categories"] ?? null) : null)) && is_array($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf) || $__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf instanceof ArrayAccess ? ($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf["image_position"] ?? null) : null) != "3")) {
                    echo " 
     \t\t\t\t\t\t\t\t\t\t<option value=\"3\">Right</option>
     \t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 536
                    echo " 
     \t\t\t\t\t\t\t\t\t\t<option value=\"3\" selected=\"selected\">Right</option>
     \t\t\t\t\t\t\t\t\t\t";
                }
                // line 538
                echo " 
     \t\t\t\t\t\t\t\t\t\t";
                // line 539
                if (((($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a = (($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4 = ($context["content"] ?? null)) && is_array($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4) || $__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4 instanceof ArrayAccess ? ($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4["categories"] ?? null) : null)) && is_array($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a) || $__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a instanceof ArrayAccess ? ($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a["image_position"] ?? null) : null) != "4")) {
                    echo " 
     \t\t\t\t\t\t\t\t\t\t<option value=\"4\">Left</option>
     \t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 541
                    echo " 
     \t\t\t\t\t\t\t\t\t\t<option value=\"4\" selected=\"selected\">Left</option>
     \t\t\t\t\t\t\t\t\t\t";
                }
                // line 543
                echo " 
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 544
                echo " 
\t\t\t\t\t\t\t\t\t\t     <option value=\"1\">Disabled</option>
\t\t\t\t\t\t\t\t\t\t     <option value=\"2\">Top</option>
\t\t\t\t\t\t\t\t\t\t     <option value=\"3\">Right</option>
\t\t\t\t\t\t\t\t\t\t     <option value=\"4\">Left</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 549
            echo " 
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix submenu-columns\" ";
            // line 554
            if (((($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985 = (($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51 = ($context["content"] ?? null)) && is_array($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51) || $__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51 instanceof ArrayAccess ? ($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51["categories"] ?? null) : null)) && is_array($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985) || $__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985 instanceof ArrayAccess ? ($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985["submenu"] ?? null) : null) != "2")) {
                echo " ";
                echo "style=\"display:none\"";
                echo " ";
            }
            echo ">
\t\t\t\t\t\t\t\t\t<p>Image dimensions (px)</p>
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"content[categories][image_width]\" style=\"width: 70px\" value=\"";
            // line 556
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "categories", [], "array", false, true, false, 556), "image_width", [], "array", true, true, false, 556)) {
                echo (($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a = (($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762 = ($context["content"] ?? null)) && is_array($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762) || $__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762 instanceof ArrayAccess ? ($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762["categories"] ?? null) : null)) && is_array($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a) || $__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a instanceof ArrayAccess ? ($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a["image_width"] ?? null) : null);
            }
            echo "\">
\t\t\t\t\t\t\t\t\t<p style=\"width: 27px\">x</p>
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"content[categories][image_height]\" style=\"width: 70px\" value=\"";
            // line 558
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "categories", [], "array", false, true, false, 558), "image_height", [], "array", true, true, false, 558)) {
                echo (($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053 = (($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c = ($context["content"] ?? null)) && is_array($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c) || $__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c instanceof ArrayAccess ? ($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c["categories"] ?? null) : null)) && is_array($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053) || $__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053 instanceof ArrayAccess ? ($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053["image_height"] ?? null) : null);
            }
            echo "\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix submenu-columns\" ";
            // line 562
            if (((($__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c = (($__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030 = ($context["content"] ?? null)) && is_array($__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030) || $__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030 instanceof ArrayAccess ? ($__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030["categories"] ?? null) : null)) && is_array($__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c) || $__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c instanceof ArrayAccess ? ($__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c["submenu"] ?? null) : null) != "2")) {
                echo " ";
                echo "style=\"display:none\"";
                echo " ";
            }
            echo ">
\t\t\t\t\t\t\t\t\t<p>Submenu columns</p>
\t\t\t\t\t\t\t\t\t<select name=\"content[categories][submenu_columns]\">
\t\t\t\t\t\t\t\t\t\t";
            // line 565
            if (((($__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8 = (($__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86 = ($context["content"] ?? null)) && is_array($__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86) || $__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86 instanceof ArrayAccess ? ($__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86["categories"] ?? null) : null)) && is_array($__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8) || $__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8 instanceof ArrayAccess ? ($__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8["submenu_columns"] ?? null) : null) != "1")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"1\">1</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 567
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">1</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 569
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 570
            if (((($__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9 = (($__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac = ($context["content"] ?? null)) && is_array($__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac) || $__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac instanceof ArrayAccess ? ($__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac["categories"] ?? null) : null)) && is_array($__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9) || $__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9 instanceof ArrayAccess ? ($__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9["submenu_columns"] ?? null) : null) != "2")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"2\">2</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 572
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" selected=\"selected\">2</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 574
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 575
            if (((($__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768 = (($__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57 = ($context["content"] ?? null)) && is_array($__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57) || $__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57 instanceof ArrayAccess ? ($__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57["categories"] ?? null) : null)) && is_array($__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768) || $__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768 instanceof ArrayAccess ? ($__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768["submenu_columns"] ?? null) : null) != "3")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"3\">3</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 577
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" selected=\"selected\">3</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 579
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 580
            if (((($__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898 = (($__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283 = ($context["content"] ?? null)) && is_array($__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283) || $__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283 instanceof ArrayAccess ? ($__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283["categories"] ?? null) : null)) && is_array($__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898) || $__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898 instanceof ArrayAccess ? ($__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898["submenu_columns"] ?? null) : null) != "4")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"4\">4</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 582
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"4\" selected=\"selected\">4</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 584
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 585
            if (((($__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a = (($__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3 = ($context["content"] ?? null)) && is_array($__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3) || $__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3 instanceof ArrayAccess ? ($__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3["categories"] ?? null) : null)) && is_array($__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a) || $__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a instanceof ArrayAccess ? ($__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a["submenu_columns"] ?? null) : null) != "5")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"5\">5</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 587
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"5\" selected=\"selected\">5</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 589
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 590
            if (((($__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4 = (($__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9 = ($context["content"] ?? null)) && is_array($__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9) || $__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9 instanceof ArrayAccess ? ($__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9["categories"] ?? null) : null)) && is_array($__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4) || $__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4 instanceof ArrayAccess ? ($__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4["submenu_columns"] ?? null) : null) != "6")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"6\">6</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 592
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"6\" selected=\"selected\">6</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 594
            echo " 
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div id=\"content_type3\"";
            // line 599
            if ((($context["content_type"] ?? null) != "3")) {
                echo " style=\"display:none\"";
            }
            echo " class=\"content_type\">
\t\t\t\t\t\t\t\t<h5 style=\"margin-top:20px\">Products</h5>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Heading title</p>
\t\t\t\t\t\t\t\t\t<div class=\"list-language\">
\t\t\t\t\t\t\t\t\t";
            // line 606
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t\t\t\t\t<div class=\"language\">
\t\t\t\t\t\t\t\t\t\t";
                // line 608
                $context["language_id"] = (($__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7 = $context["language"]) && is_array($__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7) || $__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7 instanceof ArrayAccess ? ($__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7["language_id"] ?? null) : null);
                echo " 
\t\t\t\t\t\t\t\t\t\t<img src=\"language/";
                // line 609
                echo (($__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416 = $context["language"]) && is_array($__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416) || $__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416 instanceof ArrayAccess ? ($__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416["code"] ?? null) : null);
                echo "/";
                echo (($__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e = $context["language"]) && is_array($__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e) || $__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e instanceof ArrayAccess ? ($__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e["code"] ?? null) : null);
                echo ".png\" alt=\"";
                echo (($__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f = $context["language"]) && is_array($__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f) || $__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f instanceof ArrayAccess ? ($__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f["name"] ?? null) : null);
                echo "\" width=\"16px\" height=\"11px\" />
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"content[products][heading][";
                // line 610
                echo ($context["language_id"] ?? null);
                echo "]\" ";
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "products", [], "array", false, true, false, 610), "heading", [], "array", false, true, false, 610), ($context["language_id"] ?? null), [], "array", true, true, false, 610)) {
                    echo " ";
                    echo (("value=\"" . (($__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b = (($__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75 = (($__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c = ($context["content"] ?? null)) && is_array($__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c) || $__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c instanceof ArrayAccess ? ($__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c["products"] ?? null) : null)) && is_array($__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75) || $__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75 instanceof ArrayAccess ? ($__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75["heading"] ?? null) : null)) && is_array($__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b) || $__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b instanceof ArrayAccess ? ($__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b[($context["language_id"] ?? null)] ?? null) : null)) . "\"");
                    echo " ";
                }
                echo ">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 612
            echo " 
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Add products<br><span style=\"font-size:11px;color:#808080\">(Autocomplete)</span></p>
\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"products_autocomplete\" value=\"\">\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Sort products</p>
\t\t\t\t\t\t\t\t\t<div class=\"cf nestable-lists\">
\t\t\t\t\t\t\t\t\t\t<div class=\"dd\" id=\"sort_products\">
\t\t\t\t\t\t\t\t\t\t    <ol class=\"dd-list\">
\t\t\t\t\t\t\t\t\t\t    \t";
            // line 628
            echo ($context["list_products"] ?? null);
            echo " 
\t\t\t\t\t\t\t\t\t\t    </ol>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"sort_products_data\" name=\"content[products][products]\" value=\"";
            // line 631
            echo (($__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1 = (($__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24 = ($context["content"] ?? null)) && is_array($__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24) || $__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24 instanceof ArrayAccess ? ($__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24["products"] ?? null) : null)) && is_array($__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1) || $__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1 instanceof ArrayAccess ? ($__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1["products"] ?? null) : null);
            echo "\" />
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Number of products in row</p>
\t\t\t\t\t\t\t\t\t<select name=\"content[products][columns]\">
\t\t\t\t\t\t\t\t\t\t";
            // line 639
            if (((($__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850 = (($__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34 = ($context["content"] ?? null)) && is_array($__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34) || $__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34 instanceof ArrayAccess ? ($__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34["products"] ?? null) : null)) && is_array($__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850) || $__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850 instanceof ArrayAccess ? ($__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850["columns"] ?? null) : null) != "1")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"1\">1</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 641
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">1</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 643
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 644
            if (((($__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df = (($__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4 = ($context["content"] ?? null)) && is_array($__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4) || $__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4 instanceof ArrayAccess ? ($__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4["products"] ?? null) : null)) && is_array($__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df) || $__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df instanceof ArrayAccess ? ($__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df["columns"] ?? null) : null) != "2")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"2\">2</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 646
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" selected=\"selected\">2</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 648
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 649
            if (((($__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36 = (($__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b = ($context["content"] ?? null)) && is_array($__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b) || $__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b instanceof ArrayAccess ? ($__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b["products"] ?? null) : null)) && is_array($__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36) || $__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36 instanceof ArrayAccess ? ($__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36["columns"] ?? null) : null) != "3")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"3\">3</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 651
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" selected=\"selected\">3</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 653
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 654
            if (((($__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e = (($__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7 = ($context["content"] ?? null)) && is_array($__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7) || $__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7 instanceof ArrayAccess ? ($__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7["products"] ?? null) : null)) && is_array($__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e) || $__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e instanceof ArrayAccess ? ($__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e["columns"] ?? null) : null) != "4")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"4\">4</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 656
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"4\" selected=\"selected\">4</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 658
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 659
            if (((($__internal_eac0fb02beae87e52d8817de26caac024b72dbca3fe084a7fb60ce6297e74606 = (($__internal_f449bd2e1c43123f4aea5ebb1dcb3149049e6b08332d88c5cbea9cbf72d7d7fd = ($context["content"] ?? null)) && is_array($__internal_f449bd2e1c43123f4aea5ebb1dcb3149049e6b08332d88c5cbea9cbf72d7d7fd) || $__internal_f449bd2e1c43123f4aea5ebb1dcb3149049e6b08332d88c5cbea9cbf72d7d7fd instanceof ArrayAccess ? ($__internal_f449bd2e1c43123f4aea5ebb1dcb3149049e6b08332d88c5cbea9cbf72d7d7fd["products"] ?? null) : null)) && is_array($__internal_eac0fb02beae87e52d8817de26caac024b72dbca3fe084a7fb60ce6297e74606) || $__internal_eac0fb02beae87e52d8817de26caac024b72dbca3fe084a7fb60ce6297e74606 instanceof ArrayAccess ? ($__internal_eac0fb02beae87e52d8817de26caac024b72dbca3fe084a7fb60ce6297e74606["columns"] ?? null) : null) != "5")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"5\">5</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 661
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"5\" selected=\"selected\">5</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 663
            echo " 
\t\t\t\t\t\t\t\t\t\t";
            // line 664
            if (((($__internal_ac6028158aec8e9114a7b50d00df46f3a0352559c4384cdefd768fa8d1f5095e = (($__internal_7c32a0b33fb8ca8d971d62abc97ef27c0b0f4cefceb603cb91f0956165f4a2e1 = ($context["content"] ?? null)) && is_array($__internal_7c32a0b33fb8ca8d971d62abc97ef27c0b0f4cefceb603cb91f0956165f4a2e1) || $__internal_7c32a0b33fb8ca8d971d62abc97ef27c0b0f4cefceb603cb91f0956165f4a2e1 instanceof ArrayAccess ? ($__internal_7c32a0b33fb8ca8d971d62abc97ef27c0b0f4cefceb603cb91f0956165f4a2e1["products"] ?? null) : null)) && is_array($__internal_ac6028158aec8e9114a7b50d00df46f3a0352559c4384cdefd768fa8d1f5095e) || $__internal_ac6028158aec8e9114a7b50d00df46f3a0352559c4384cdefd768fa8d1f5095e instanceof ArrayAccess ? ($__internal_ac6028158aec8e9114a7b50d00df46f3a0352559c4384cdefd768fa8d1f5095e["columns"] ?? null) : null) != "6")) {
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"6\">6</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 666
                echo " 
\t\t\t\t\t\t\t\t\t\t<option value=\"6\" selected=\"selected\">6</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 668
            echo " 
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t\t<p>Product image dimensions (px)</p>
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"content[products][image_width]\" style=\"width: 70px\" value=\"";
            // line 675
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "products", [], "array", false, true, false, 675), "image_width", [], "array", true, true, false, 675)) {
                echo (($__internal_68d3b371ec0c4bb1581025ed4c1d76a431a042b7b439120f66468cb409de0cdb = (($__internal_12df7a6a0a260f0401b6892f7ce4fef2ea0fea7f4abf3aaab9ef6f1113a738cf = ($context["content"] ?? null)) && is_array($__internal_12df7a6a0a260f0401b6892f7ce4fef2ea0fea7f4abf3aaab9ef6f1113a738cf) || $__internal_12df7a6a0a260f0401b6892f7ce4fef2ea0fea7f4abf3aaab9ef6f1113a738cf instanceof ArrayAccess ? ($__internal_12df7a6a0a260f0401b6892f7ce4fef2ea0fea7f4abf3aaab9ef6f1113a738cf["products"] ?? null) : null)) && is_array($__internal_68d3b371ec0c4bb1581025ed4c1d76a431a042b7b439120f66468cb409de0cdb) || $__internal_68d3b371ec0c4bb1581025ed4c1d76a431a042b7b439120f66468cb409de0cdb instanceof ArrayAccess ? ($__internal_68d3b371ec0c4bb1581025ed4c1d76a431a042b7b439120f66468cb409de0cdb["image_width"] ?? null) : null);
            }
            echo "\">
\t\t\t\t\t\t\t\t\t<p style=\"width: 27px\">x</p>
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"content[products][image_height]\" style=\"width: 70px\" value=\"";
            // line 677
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "products", [], "array", false, true, false, 677), "image_height", [], "array", true, true, false, 677)) {
                echo (($__internal_1fa86e54c040f0d1b500ff8a8536fb704ead4a955f38e9ee0c72d436e09d2d6b = (($__internal_7c817ef80fec483e83fdd5a0d75d7936b34e91df63a1e5f99c810f6ddfb73980 = ($context["content"] ?? null)) && is_array($__internal_7c817ef80fec483e83fdd5a0d75d7936b34e91df63a1e5f99c810f6ddfb73980) || $__internal_7c817ef80fec483e83fdd5a0d75d7936b34e91df63a1e5f99c810f6ddfb73980 instanceof ArrayAccess ? ($__internal_7c817ef80fec483e83fdd5a0d75d7936b34e91df63a1e5f99c810f6ddfb73980["products"] ?? null) : null)) && is_array($__internal_1fa86e54c040f0d1b500ff8a8536fb704ead4a955f38e9ee0c72d436e09d2d6b) || $__internal_1fa86e54c040f0d1b500ff8a8536fb704ead4a955f38e9ee0c72d436e09d2d6b instanceof ArrayAccess ? ($__internal_1fa86e54c040f0d1b500ff8a8536fb704ead4a955f38e9ee0c72d436e09d2d6b["image_height"] ?? null) : null);
            }
            echo "\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        } else {
            // line 681
            echo " 
\t\t\t\t\t\t<div class=\"right\">
\t\t\t\t\t\t\t<h4>Basic configuration</h4>
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Status</p>
\t\t\t\t\t\t\t\t<select name=\"status\">
\t\t\t\t\t\t\t\t\t";
            // line 688
            if (($context["status"] ?? null)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">Enabled</option>
\t\t\t\t\t\t\t\t\t<option value=\"0\">Disabled</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 691
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"1\">Enabled</option>
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">Disabled</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 694
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Display on mobile</p>
\t\t\t\t\t\t\t\t<select name=\"display_on_mobile\">
\t\t\t\t\t\t\t\t\t";
            // line 702
            if (($context["display_on_mobile"] ?? null)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\">yes</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">no</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 705
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">yes</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">no</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 708
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Position</p>
\t\t\t\t\t\t\t\t<select name=\"position\">
\t\t\t\t\t\t\t\t\t";
            // line 716
            if ((($context["position"] ?? null) == "menu")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"menu\" selected=\"selected\">Menu</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 718
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"menu\">Menu</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 720
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 721
            if ((($context["position"] ?? null) == "menu2")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"menu2\" selected=\"selected\">Menu2</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 723
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"menu2\">Menu2</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 725
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 726
            if ((($context["position"] ?? null) == "slideshow")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"slideshow\" selected=\"selected\">Slideshow</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 728
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"slideshow\">Slideshow</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 730
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 731
            if ((($context["position"] ?? null) == "preface_left")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"preface_left\" selected=\"selected\">Preface left</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 733
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"preface_left\">Preface left</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 735
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 736
            if ((($context["position"] ?? null) == "preface_right")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"preface_right\" selected=\"selected\">Preface right</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 738
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"preface_right\">Preface right</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 740
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 741
            if ((($context["position"] ?? null) == "preface_fullwidth")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"preface_fullwidth\" selected=\"selected\">Preface fullwidth</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 743
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"preface_fullwidth\">Preface fullwidth</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 745
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 746
            if ((($context["position"] ?? null) == "column_left")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"column_left\" selected=\"selected\">Column left</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 748
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"column_left\">Column left</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 750
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 751
            if ((($context["position"] ?? null) == "content_big_column")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"content_big_column\" selected=\"selected\">Content big column</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 753
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"content_big_column\">Content big column</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 755
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 756
            if ((($context["position"] ?? null) == "content_top")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"content_top\" selected=\"selected\">Content top</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 758
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"content_top\">Content top</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 760
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 761
            if ((($context["position"] ?? null) == "column_right")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"column_right\" selected=\"selected\">Column right</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 763
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"column_right\">Column right</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 765
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 766
            if ((($context["position"] ?? null) == "content_bottom")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"content_bottom\" selected=\"selected\">Content bottom</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 768
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"content_bottom\">Content bottom</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 770
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 771
            if ((($context["position"] ?? null) == "customfooter_top")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"customfooter_top\" selected=\"selected\">CustomFooter Top</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 773
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"customfooter_top\">CustomFooter Top</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 775
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 776
            if ((($context["position"] ?? null) == "customfooter_bottom")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"customfooter_bottom\" selected=\"selected\">CustomFooter Bottom</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 778
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"customfooter_bottom\">CustomFooter Bottom</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 780
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 781
            if ((($context["position"] ?? null) == "footer_top")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"footer_top\" selected=\"selected\">Footer top</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 783
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"footer_top\">Footer top</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 785
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 786
            if ((($context["position"] ?? null) == "footer_left")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"footer_left\" selected=\"selected\">Footer left</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 788
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"footer_left\">Footer left</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 790
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 791
            if ((($context["position"] ?? null) == "footer_right")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"footer_right\" selected=\"selected\">Footer right</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 793
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"footer_right\">Footer right</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 795
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 796
            if ((($context["position"] ?? null) == "footer_bottom")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"footer_bottom\" selected=\"selected\">Footer bottom</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 798
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"footer_bottom\">Footer bottom</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 800
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 801
            if ((($context["position"] ?? null) == "bottom")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"bottom\" selected=\"selected\">Bottom</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 803
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"bottom\">Bottom</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 805
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Layout</p>
\t\t\t\t\t\t\t\t<select name=\"layout_id\">
\t\t\t\t\t\t\t\t\t";
            // line 813
            if ((99999 == ($context["layout_id"] ?? null))) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"99999\" selected=\"selected\">All pages</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 815
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"99999\">All pages</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 817
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 818
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["stores"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
                echo " 
\t\t\t\t\t\t\t\t\t\t";
                // line 819
                if ((("99999" . (($__internal_58f05cb7b103fdb27c83e116d9b750a441975afa718f181d426ba20756cae345 = $context["store"]) && is_array($__internal_58f05cb7b103fdb27c83e116d9b750a441975afa718f181d426ba20756cae345) || $__internal_58f05cb7b103fdb27c83e116d9b750a441975afa718f181d426ba20756cae345 instanceof ArrayAccess ? ($__internal_58f05cb7b103fdb27c83e116d9b750a441975afa718f181d426ba20756cae345["store_id"] ?? null) : null)) == ($context["layout_id"] ?? null))) {
                    // line 820
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                    echo (((("<option value=\"99999" . (($__internal_2c848f3022a3402e3a4e27a30257fa7d076f394b2c17fd1315626995668cc7a3 = $context["store"]) && is_array($__internal_2c848f3022a3402e3a4e27a30257fa7d076f394b2c17fd1315626995668cc7a3) || $__internal_2c848f3022a3402e3a4e27a30257fa7d076f394b2c17fd1315626995668cc7a3 instanceof ArrayAccess ? ($__internal_2c848f3022a3402e3a4e27a30257fa7d076f394b2c17fd1315626995668cc7a3["store_id"] ?? null) : null)) . "\" selected=\"selected\">All pages - Store ") . (($__internal_b8c7cfa2093058440418fed5e0a741d0931d374a0b972ab2bdfe5d1a043c45d0 = $context["store"]) && is_array($__internal_b8c7cfa2093058440418fed5e0a741d0931d374a0b972ab2bdfe5d1a043c45d0) || $__internal_b8c7cfa2093058440418fed5e0a741d0931d374a0b972ab2bdfe5d1a043c45d0 instanceof ArrayAccess ? ($__internal_b8c7cfa2093058440418fed5e0a741d0931d374a0b972ab2bdfe5d1a043c45d0["name"] ?? null) : null)) . "</option>");
                    echo "
\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 821
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 822
                    echo (((("<option value=\"99999" . (($__internal_c1cd480b2bae110b528bbc3f808e69c4b6a9aeedf00a361275f8ddb342dfe938 = $context["store"]) && is_array($__internal_c1cd480b2bae110b528bbc3f808e69c4b6a9aeedf00a361275f8ddb342dfe938) || $__internal_c1cd480b2bae110b528bbc3f808e69c4b6a9aeedf00a361275f8ddb342dfe938 instanceof ArrayAccess ? ($__internal_c1cd480b2bae110b528bbc3f808e69c4b6a9aeedf00a361275f8ddb342dfe938["store_id"] ?? null) : null)) . "\">All pages - Store ") . (($__internal_079975fe41d37645946c3a823d9bb78a9ae0e38816557a03403725361f35feb3 = $context["store"]) && is_array($__internal_079975fe41d37645946c3a823d9bb78a9ae0e38816557a03403725361f35feb3) || $__internal_079975fe41d37645946c3a823d9bb78a9ae0e38816557a03403725361f35feb3 instanceof ArrayAccess ? ($__internal_079975fe41d37645946c3a823d9bb78a9ae0e38816557a03403725361f35feb3["name"] ?? null) : null)) . "</option>");
                    echo "
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 824
                echo "\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 825
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["layouts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["layout"]) {
                echo " 
\t\t\t\t\t\t\t\t\t";
                // line 826
                if (((($__internal_740db85f46dbd95cea320267399fd88e8007c386d126eec44ce5a5594fea0daa = $context["layout"]) && is_array($__internal_740db85f46dbd95cea320267399fd88e8007c386d126eec44ce5a5594fea0daa) || $__internal_740db85f46dbd95cea320267399fd88e8007c386d126eec44ce5a5594fea0daa instanceof ArrayAccess ? ($__internal_740db85f46dbd95cea320267399fd88e8007c386d126eec44ce5a5594fea0daa["layout_id"] ?? null) : null) == ($context["layout_id"] ?? null))) {
                    echo " 
\t\t\t\t\t\t\t\t\t<option value=\"";
                    // line 827
                    echo (($__internal_04e2723480818cf7e4ae08c1e7380310abe34ee48600ebabbfbaca3a62b4f1fb = $context["layout"]) && is_array($__internal_04e2723480818cf7e4ae08c1e7380310abe34ee48600ebabbfbaca3a62b4f1fb) || $__internal_04e2723480818cf7e4ae08c1e7380310abe34ee48600ebabbfbaca3a62b4f1fb instanceof ArrayAccess ? ($__internal_04e2723480818cf7e4ae08c1e7380310abe34ee48600ebabbfbaca3a62b4f1fb["layout_id"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_9dfe9126eb6cb3d8182bbdebdcbf291354ce41935a4d52134757b624790fe26c = $context["layout"]) && is_array($__internal_9dfe9126eb6cb3d8182bbdebdcbf291354ce41935a4d52134757b624790fe26c) || $__internal_9dfe9126eb6cb3d8182bbdebdcbf291354ce41935a4d52134757b624790fe26c instanceof ArrayAccess ? ($__internal_9dfe9126eb6cb3d8182bbdebdcbf291354ce41935a4d52134757b624790fe26c["name"] ?? null) : null);
                    echo "</option>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 828
                    echo " 
\t\t\t\t\t\t\t\t\t<option value=\"";
                    // line 829
                    echo (($__internal_7886d104df990d4d01343e15743b569d1995f6a6a8de3ead740a6091880b629a = $context["layout"]) && is_array($__internal_7886d104df990d4d01343e15743b569d1995f6a6a8de3ead740a6091880b629a) || $__internal_7886d104df990d4d01343e15743b569d1995f6a6a8de3ead740a6091880b629a instanceof ArrayAccess ? ($__internal_7886d104df990d4d01343e15743b569d1995f6a6a8de3ead740a6091880b629a["layout_id"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_bf8548f45bc193921ffb4426690048a7605b21cb5873c3e67934670fc157bcb6 = $context["layout"]) && is_array($__internal_bf8548f45bc193921ffb4426690048a7605b21cb5873c3e67934670fc157bcb6) || $__internal_bf8548f45bc193921ffb4426690048a7605b21cb5873c3e67934670fc157bcb6 instanceof ArrayAccess ? ($__internal_bf8548f45bc193921ffb4426690048a7605b21cb5873c3e67934670fc157bcb6["name"] ?? null) : null);
                    echo "</option>
\t\t\t\t\t\t\t\t\t";
                }
                // line 830
                echo " 
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['layout'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 831
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Sort order</p>
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"sort_order\" value=\"";
            // line 838
            echo ($context["sort_order"] ?? null);
            echo "\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<h4 style=\"margin-top:20px\">Design configuration</h4>
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Orientation</p>
\t\t\t\t\t\t\t\t<select name=\"orientation\">
\t\t\t\t\t\t\t\t\t";
            // line 846
            if ((($context["orientation"] ?? null) == 1)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\">Horizontal</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">Vertical</option>
\t\t\t\t\t\t\t\t\t<option value=\"2\">Horizontal type 2 (Flowers version)</option>
\t\t\t\t\t\t\t\t\t<option value=\"3\">Horizontal type 3 (Audio version)</option>
\t\t\t\t\t\t\t\t\t";
            } elseif ((            // line 851
($context["orientation"] ?? null) == 2)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\">Horizontal</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">Vertical</option>
\t\t\t\t\t\t\t\t\t<option value=\"2\" selected=\"selected\">Horizontal type 2 (Flowers version)</option>
\t\t\t\t\t\t\t\t\t<option value=\"3\">Horizontal type 3 (Audio version)</option>
\t\t\t\t\t\t\t\t\t";
            } elseif ((            // line 856
($context["orientation"] ?? null) == 3)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\">Horizontal</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">Vertical</option>
\t\t\t\t\t\t\t\t\t<option value=\"2\">Horizontal type 2 (Flowers version)</option>
\t\t\t\t\t\t\t\t\t<option value=\"3\" selected=\"selected\">Horizontal type 3 (Audio version)</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 861
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">Horizontal</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">Vertical</option>
\t\t\t\t\t\t\t\t\t<option value=\"2\">Horizontal type 2 (Flowers version)</option>
\t\t\t\t\t\t\t\t\t<option value=\"3\">Horizontal type 3 (Audio version)</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 866
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Search -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Search bar</p>
\t\t\t\t\t\t\t\t<div><input type=\"checkbox\" name=\"search_bar\" ";
            // line 873
            if ((($context["search_bar"] ?? null) == 1)) {
                echo " ";
                echo "checked=\"checked\"";
                echo " ";
            }
            echo " value=\"1\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Navigation text</p>
\t\t\t\t\t\t\t\t<div class=\"list-language\">
\t\t\t\t\t\t\t\t";
            // line 880
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t\t\t\t<div class=\"language\">
\t\t\t\t\t\t\t\t\t";
                // line 882
                $context["language_id"] = (($__internal_d9d33b22591102d2e461af9c204c3c40751fa247b0275a5e9ac02a242b6b099b = $context["language"]) && is_array($__internal_d9d33b22591102d2e461af9c204c3c40751fa247b0275a5e9ac02a242b6b099b) || $__internal_d9d33b22591102d2e461af9c204c3c40751fa247b0275a5e9ac02a242b6b099b instanceof ArrayAccess ? ($__internal_d9d33b22591102d2e461af9c204c3c40751fa247b0275a5e9ac02a242b6b099b["language_id"] ?? null) : null);
                echo " 
\t\t\t\t\t\t\t\t\t<img src=\"language/";
                // line 883
                echo (($__internal_eed548cde44c216c917d86f1b41aeead16364f508b904d138a9861b48cf18526 = $context["language"]) && is_array($__internal_eed548cde44c216c917d86f1b41aeead16364f508b904d138a9861b48cf18526) || $__internal_eed548cde44c216c917d86f1b41aeead16364f508b904d138a9861b48cf18526 instanceof ArrayAccess ? ($__internal_eed548cde44c216c917d86f1b41aeead16364f508b904d138a9861b48cf18526["code"] ?? null) : null);
                echo "/";
                echo (($__internal_69673c0dda0724bda92ca0f89665181eb299815d5bf0d9166a7fa457f623049f = $context["language"]) && is_array($__internal_69673c0dda0724bda92ca0f89665181eb299815d5bf0d9166a7fa457f623049f) || $__internal_69673c0dda0724bda92ca0f89665181eb299815d5bf0d9166a7fa457f623049f instanceof ArrayAccess ? ($__internal_69673c0dda0724bda92ca0f89665181eb299815d5bf0d9166a7fa457f623049f["code"] ?? null) : null);
                echo ".png\" alt=\"";
                echo (($__internal_98e7c66b1d8077f0adf5874b6a626a0256df01315d35d9e34fe7dbdf2b1f397c = $context["language"]) && is_array($__internal_98e7c66b1d8077f0adf5874b6a626a0256df01315d35d9e34fe7dbdf2b1f397c) || $__internal_98e7c66b1d8077f0adf5874b6a626a0256df01315d35d9e34fe7dbdf2b1f397c instanceof ArrayAccess ? ($__internal_98e7c66b1d8077f0adf5874b6a626a0256df01315d35d9e34fe7dbdf2b1f397c["name"] ?? null) : null);
                echo "\" width=\"16px\" height=\"11px\" />
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"navigation_text[";
                // line 884
                echo ($context["language_id"] ?? null);
                echo "]\" ";
                if (twig_get_attribute($this->env, $this->source, ($context["navigation_text"] ?? null), ($context["language_id"] ?? null), [], "array", true, true, false, 884)) {
                    echo " ";
                    echo (("value=\"" . (($__internal_b34f576c9690a300f94652a12516183f72eacabacea74206fd0ebac5164ede74 = ($context["navigation_text"] ?? null)) && is_array($__internal_b34f576c9690a300f94652a12516183f72eacabacea74206fd0ebac5164ede74) || $__internal_b34f576c9690a300f94652a12516183f72eacabacea74206fd0ebac5164ede74 instanceof ArrayAccess ? ($__internal_b34f576c9690a300f94652a12516183f72eacabacea74206fd0ebac5164ede74[($context["language_id"] ?? null)] ?? null) : null)) . "\"");
                    echo " ";
                }
                echo ">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 886
            echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Expand Menu Bar Full Width?</p>
\t\t\t\t\t\t\t\t<select name=\"full_width\">
\t\t\t\t\t\t\t\t\t";
            // line 894
            if (($context["full_width"] ?? null)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">Yes</option>
\t\t\t\t\t\t\t\t\t<option value=\"0\">No</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 897
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"1\">Yes</option>
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">No</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 900
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Home item</p>
\t\t\t\t\t\t\t\t<select name=\"home_item\">
\t\t\t\t\t\t\t\t\t";
            // line 908
            if ((($context["home_item"] ?? null) == "icon")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"icon\" selected=\"selected\">Icon</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 910
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"icon\">Icon</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 912
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 913
            if ((($context["home_item"] ?? null) == "text")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"text\" selected=\"selected\">Text</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 915
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"text\">Text</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 917
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 918
            if ((($context["home_item"] ?? null) == "disabled")) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"disabled\" selected=\"selected\">Disabled</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 920
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"disabled\">Disabled</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 922
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Home text</p>
\t\t\t\t\t\t\t\t<div class=\"list-language\">
\t\t\t\t\t\t\t\t";
            // line 930
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t\t\t\t<div class=\"language\">
\t\t\t\t\t\t\t\t\t";
                // line 932
                $context["language_id"] = (($__internal_4a561b149f190c3cf54242a61f4c7f0df2a717b925f2c7a775371ef55c39caff = $context["language"]) && is_array($__internal_4a561b149f190c3cf54242a61f4c7f0df2a717b925f2c7a775371ef55c39caff) || $__internal_4a561b149f190c3cf54242a61f4c7f0df2a717b925f2c7a775371ef55c39caff instanceof ArrayAccess ? ($__internal_4a561b149f190c3cf54242a61f4c7f0df2a717b925f2c7a775371ef55c39caff["language_id"] ?? null) : null);
                echo " 
\t\t\t\t\t\t\t\t\t<img src=\"language/";
                // line 933
                echo (($__internal_7f64f85f9301de90a5b045895fc6e5587d70b65ebc68918344f8c25d458d3918 = $context["language"]) && is_array($__internal_7f64f85f9301de90a5b045895fc6e5587d70b65ebc68918344f8c25d458d3918) || $__internal_7f64f85f9301de90a5b045895fc6e5587d70b65ebc68918344f8c25d458d3918 instanceof ArrayAccess ? ($__internal_7f64f85f9301de90a5b045895fc6e5587d70b65ebc68918344f8c25d458d3918["code"] ?? null) : null);
                echo "/";
                echo (($__internal_b57c690297f5d1db403c4e65f613e450889065335a92d7f73f82e713f90b25e5 = $context["language"]) && is_array($__internal_b57c690297f5d1db403c4e65f613e450889065335a92d7f73f82e713f90b25e5) || $__internal_b57c690297f5d1db403c4e65f613e450889065335a92d7f73f82e713f90b25e5 instanceof ArrayAccess ? ($__internal_b57c690297f5d1db403c4e65f613e450889065335a92d7f73f82e713f90b25e5["code"] ?? null) : null);
                echo ".png\" alt=\"";
                echo (($__internal_75d475ff9edc93fda230c7c714c00f4c5dbb39fa0dbbcb262e9ab1617f92f219 = $context["language"]) && is_array($__internal_75d475ff9edc93fda230c7c714c00f4c5dbb39fa0dbbcb262e9ab1617f92f219) || $__internal_75d475ff9edc93fda230c7c714c00f4c5dbb39fa0dbbcb262e9ab1617f92f219 instanceof ArrayAccess ? ($__internal_75d475ff9edc93fda230c7c714c00f4c5dbb39fa0dbbcb262e9ab1617f92f219["name"] ?? null) : null);
                echo "\" width=\"16px\" height=\"11px\" />
\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"home_text[";
                // line 934
                echo ($context["language_id"] ?? null);
                echo "]\" ";
                if (twig_get_attribute($this->env, $this->source, ($context["home_text"] ?? null), ($context["language_id"] ?? null), [], "array", true, true, false, 934)) {
                    echo " ";
                    echo (("value=\"" . (($__internal_5aa517627f62fe0421e8b859b10fd7903a81d7224c214373093f337db21ecc20 = ($context["home_text"] ?? null)) && is_array($__internal_5aa517627f62fe0421e8b859b10fd7903a81d7224c214373093f337db21ecc20) || $__internal_5aa517627f62fe0421e8b859b10fd7903a81d7224c214373093f337db21ecc20 instanceof ArrayAccess ? ($__internal_5aa517627f62fe0421e8b859b10fd7903a81d7224c214373093f337db21ecc20[($context["language_id"] ?? null)] ?? null) : null)) . "\"");
                    echo " ";
                }
                echo ">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 936
            echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<h4 style=\"margin-top:20px\">jQuery Animations</h4>
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Animation</p>
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"slide\" name=\"animation\" ";
            // line 945
            if ((($context["animation"] ?? null) == "slide")) {
                echo " ";
                echo "checked";
                echo " ";
            }
            echo "> Slide<br>
\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"fade\" name=\"animation\" ";
            // line 946
            if ((($context["animation"] ?? null) == "fade")) {
                echo " ";
                echo "checked";
                echo " ";
            }
            echo "> Fade<br>
\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"shift-up\" name=\"animation\" ";
            // line 947
            if ((($context["animation"] ?? null) == "shift-up")) {
                echo " ";
                echo "checked";
                echo " ";
            }
            echo "> Shift Up<br>
\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"shift-down\" name=\"animation\" ";
            // line 948
            if ((($context["animation"] ?? null) == "shift-down")) {
                echo " ";
                echo "checked";
                echo " ";
            }
            echo "> Shift Down<br>
\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"shift-left\" name=\"animation\" ";
            // line 949
            if ((($context["animation"] ?? null) == "shift-left")) {
                echo " ";
                echo "checked";
                echo " ";
            }
            echo "> Shift Left<br>
\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"shift-right\" name=\"animation\" ";
            // line 950
            if ((($context["animation"] ?? null) == "shift-right")) {
                echo " ";
                echo "checked";
                echo " ";
            }
            echo "> Shift Right<br>
\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"flipping\" name=\"animation\" ";
            // line 951
            if ((($context["animation"] ?? null) == "flipping")) {
                echo " ";
                echo "checked";
                echo " ";
            }
            echo "> 3D Flipping<br>
\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"none\" name=\"animation\" ";
            // line 952
            if ((($context["animation"] ?? null) == "none")) {
                echo " ";
                echo "checked";
                echo " ";
            }
            echo "> None
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Animation Time</p>
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"animation_time\" style=\"width:60px;margin-right:10px\" value=\"";
            // line 959
            echo ($context["animation_time"] ?? null);
            echo "\">
\t\t\t\t\t\t\t\t<div>ms</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<h4 style=\"margin-top:20px\">Cache Configuration</h4>
\t\t\t\t\t\t\t<p style=\"background: #D9EDFA;padding:12px 15px;margin-top:20px\">Allows for faster page loading. Changes made when this option is enabled will be visible after a cache time.</p>
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Status</p>
\t\t\t\t\t\t\t\t<select name=\"status_cache\">
\t\t\t\t\t\t\t\t\t";
            // line 969
            if (($context["status_cache"] ?? null)) {
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">Enabled</option>
\t\t\t\t\t\t\t\t\t<option value=\"0\">Disabled</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 972
                echo " 
\t\t\t\t\t\t\t\t\t<option value=\"1\">Enabled</option>
\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">Disabled</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 975
            echo " 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Input -->
\t\t\t\t\t\t\t<div class=\"input clearfix\">
\t\t\t\t\t\t\t\t<p>Cache Time (in hours)</p>
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"cache_time\" style=\"width:60px;margin-right:10px\" value=\"";
            // line 982
            echo ($context["cache_time"] ?? null);
            echo "\">
\t\t\t\t\t\t\t\t<div>h</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        }
        // line 987
        echo " 
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"buttons\">
\t\t\t\t\t\t";
        // line 991
        if ((($context["action_type"] ?? null) == "create")) {
            echo " 
\t\t\t\t\t\t<input type=\"submit\" name=\"button-create\" class=\"button-save\" value=\"\">
\t\t\t\t\t\t";
        } elseif ((        // line 993
($context["action_type"] ?? null) == "edit")) {
            echo " 
\t\t\t\t\t\t<input type=\"submit\" name=\"button-edit\" class=\"button-save\" value=\"\">
\t\t\t\t\t\t";
        } else {
            // line 995
            echo " 
\t\t\t\t\t\t<input type=\"submit\" name=\"button-save\" class=\"button-save\" value=\"\">
\t\t\t\t\t\t";
        }
        // line 997
        echo " 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!-- End Content -->
\t\t</form>
\t</div>
 </div>
 
 ";
        // line 1006
        if (((($context["action_type"] ?? null) == "create") || (($context["action_type"] ?? null) == "edit"))) {
            echo " 
 <script type=\"text/javascript\">
 \t \$('.htmltabs a').tabs();
 </script>
 ";
        }
        // line 1010
        echo " 
 
<script type=\"text/javascript\">
\$(document).ready(function() {

\t\$(\"select[name=content_type]\").change(function () {
\t\t\$(\"select[name=content_type] option:selected\").each(function() {
\t\t\t\$(\".content_type\").hide();
\t\t\t\$(\"#content_type\" + \$(this).val()).show();
\t\t});
\t});
\t
\t\$(\"#submenu-type\").change(function () {
\t\t\$(\"#submenu-type option:selected\").each(function() {
\t\t\tif(\$(this).val() == 2) {
\t\t\t\t\$(\".submenu-columns\").show();
\t\t\t} else {
\t\t\t\t\$(\".submenu-columns\").hide();
\t\t\t}
\t\t});
\t});


\t\$('#product_autocomplete').autocomplete({
\t\tdelay: 500,
\t\tmax: 30,
\t\tsource: function(request, response) {\t\t
\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=catalog/product/autocomplete&user_token=";
        // line 1038
        echo ($context["user_token"] ?? null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(json) {
\t\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t\t}
\t\t\t\t\t}));
\t\t\t\t}
\t\t\t});
\t\t},
\t\tselect: function(item) {
\t\t\t\$('#product_autocomplete').val(item['label']);
\t\t\t\$('input[name=\\'content[product][id]\\']').val(item['value']);
\t\t\t
\t\t\treturn false;
\t\t},
\t\tfocus: function(event, ui) {
\t      \treturn false;
\t   \t}
\t});
\t
\t\$('#categories_autocomplete').autocomplete({
\t\tdelay: 500,
\t\tmax: 30,
\t\tsource: function(request, response) {\t\t
\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=catalog/category/autocomplete&user_token=";
        // line 1066
        echo ($context["user_token"] ?? null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(json) {
\t\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\t\tvalue: item['category_id']
\t\t\t\t\t\t}
\t\t\t\t\t}));
\t\t\t\t}
\t\t\t});
\t\t},
\t\tselect: function(item) {
\t\t\t\$(\"#sort_categories > .dd-list\").append('<li class=\"dd-item\" data-id=\"' + item['value'] + '\" data-name=\"' + item['label'] + '\"><a class=\"icon-delete\"></a><div class=\"dd-handle\">' + item['label'] + '</div></li>');
\t\t\tupdateOutput2(\$('#sort_categories').data('output', \$('#sort_categories_data')));
\t\t\treturn false;
\t\t},
\t\tfocus: function(event, ui) {
\t      \treturn false;
\t   \t}
\t});
\t
\t
\t\$('#products_autocomplete').autocomplete({
\t\tdelay: 500,
\t\tmax: 30,
\t\tsource: function(request, response) {\t\t
\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=catalog/product/autocomplete&user_token=";
        // line 1094
        echo ($context["user_token"] ?? null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(json) {
\t\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t\t}
\t\t\t\t\t}));
\t\t\t\t}
\t\t\t});
\t\t},
\t\tselect: function(item) {
\t\t\t\$(\"#sort_products > .dd-list\").append('<li class=\"dd-item\" data-id=\"' + item['value'] + '\" data-name=\"' + item['label'] + '\"><a class=\"icon-delete\"></a><div class=\"dd-handle\">' + item['label'] + '</div></li>');
\t\t\tupdateOutput3(\$('#sort_products').data('output', \$('#sort_products_data')));
\t\t\treturn false;
\t\t},
\t\tfocus: function(event, ui) {
\t      \treturn false;
\t   \t}
\t});
\t
\t\$('#links_autocomplete').autocomplete({
\t\tdelay: 500,
\t\tmax: 30,
\t\tsource: function(request, response) {\t\t
\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=extension/module/megamenu/autocomplete&user_token=";
        // line 1121
        echo ($context["user_token"] ?? null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(json) {
\t\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\t\tvalue: item['id']
\t\t\t\t\t\t}
\t\t\t\t\t}));
\t\t\t\t}
\t\t\t});
\t\t},
\t\tselect: function(item) {
\t\t\t\$(\"#sort_categories > .dd-list\").append('<li class=\"dd-item\" data-id=\"' + item['value'] + '\" data-type=\"link\" data-name=\"' + item['label'] + '\"><a class=\"icon-delete\"></a><div class=\"dd-handle\">' + item['label'] + '</div></li>');
\t\t\tupdateOutput2(\$('#sort_categories').data('output', \$('#sort_categories_data')));
\t\t\treturn false;
\t\t},
\t\tfocus: function(event, ui) {
\t      \treturn false;
\t   \t}
\t});
\t
\tfunction lagXHRobjekt() {
\t\tvar XHRobjekt = null;
\t\t
\t\ttry {
\t\t\tajaxRequest = new XMLHttpRequest(); // Firefox, Opera, ...
\t\t} catch(err1) {
\t\t\ttry {
\t\t\t\tajaxRequest = new ActiveXObject(\"Microsoft.XMLHTTP\"); // Noen IE v.
\t\t\t} catch(err2) {
\t\t\t\ttry {
\t\t\t\t\t\tajaxRequest = new ActiveXObject(\"Msxml2.XMLHTTP\"); // Noen IE v.
\t\t\t\t} catch(err3) {
\t\t\t\t\tajaxRequest = false;
\t\t\t\t}
\t\t\t}
\t\t}
\t\treturn ajaxRequest;
\t}
\t
\t
\tfunction menu_updatesort(jsonstring) {
\t     \$.post(\"index.php?route=extension/module/megamenu&user_token=";
        // line 1164
        echo ($context["user_token"] ?? null);
        echo "&module_id=";
        echo ($context["active_module_id"] ?? null);
        echo "\", {
\t         jsonstring: jsonstring
\t     }, function(data, status){
\t          var ajaxDisplay = document.getElementById('sortDBfeedback');
\t          ajaxDisplay.innerHTML = data;
\t     });
\t}
\t
\tvar updateOutput = function(e)
\t{
\t    var list   = e.length ? e : \$(e.target),
\t        output = list.data('output');
\t    if (window.JSON) {
\t        menu_updatesort(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
\t    } else {
\t        alert('JSON browser support required for this demo.');
\t    }
\t};
\t
\t\$('#nestable').nestable({
\t\tgroup: 1,
\t\tmaxDepth: 2
\t}).on('change', updateOutput);
\t
\tupdateOutput(\$('#nestable').data('output', \$('#nestable-output')));
\t
\t
\t";
        // line 1191
        if (((($context["action_type"] ?? null) == "create") || (($context["action_type"] ?? null) == "edit"))) {
            echo " 
\t\tvar updateOutput2 = function(e)
\t\t{
\t\t    var list   = e.length ? e : \$(e.target),
\t\t        output = list.data('output');
\t\t    if (window.JSON) {
\t\t        output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
\t\t    } else {
\t\t        output.val('JSON browser support required for this demo.');
\t\t    }
\t};
\t\t
\t\t\$('#sort_categories').nestable({
\t\t\tgroup: 1,
\t\t\tmaxDepth: 5
\t\t}).on('change', updateOutput2);
\t\t
\t\tupdateOutput2(\$('#sort_categories').data('output', \$('#sort_categories_data')));
\t\t
\t\t\$('#sort_categories').on('click', '.icon-delete', function() {
\t\t\t\$(this).parent().remove();
\t\t\tupdateOutput2(\$('#sort_categories').data('output', \$('#sort_categories_data')));
\t\t});
\t\t
\t\tvar updateOutput3 = function(e)
\t\t{
\t\t    var list   = e.length ? e : \$(e.target),
\t\t        output = list.data('output');
\t\t    if (window.JSON) {
\t\t        output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
\t\t    } else {
\t\t        output.val('JSON browser support required for this demo.');
\t\t    }
\t};
\t\t
\t\t\$('#sort_products').nestable({
\t\t\tgroup: 1,
\t\t\tmaxDepth: 1
\t\t}).on('change', updateOutput3);
\t\t
\t\tupdateOutput3(\$('#sort_products').data('output', \$('#sort_products_data')));
\t\t
\t\t\$('#sort_products').on('click', '.icon-delete', function() {
\t\t\t\$(this).parent().remove();
\t\t\tupdateOutput3(\$('#sort_products').data('output', \$('#sort_products_data')));
\t\t});
\t";
        }
        // line 1237
        echo " 
\t
});
</script>
";
        // line 1241
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/megamenu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2633 => 1241,  2627 => 1237,  2577 => 1191,  2545 => 1164,  2499 => 1121,  2469 => 1094,  2438 => 1066,  2407 => 1038,  2377 => 1010,  2369 => 1006,  2358 => 997,  2353 => 995,  2347 => 993,  2342 => 991,  2336 => 987,  2327 => 982,  2318 => 975,  2312 => 972,  2305 => 969,  2292 => 959,  2278 => 952,  2270 => 951,  2262 => 950,  2254 => 949,  2246 => 948,  2238 => 947,  2230 => 946,  2222 => 945,  2211 => 936,  2196 => 934,  2188 => 933,  2184 => 932,  2177 => 930,  2167 => 922,  2162 => 920,  2156 => 918,  2153 => 917,  2148 => 915,  2142 => 913,  2139 => 912,  2134 => 910,  2128 => 908,  2118 => 900,  2112 => 897,  2105 => 894,  2095 => 886,  2080 => 884,  2072 => 883,  2068 => 882,  2061 => 880,  2047 => 873,  2038 => 866,  2030 => 861,  2021 => 856,  2013 => 851,  2005 => 846,  1994 => 838,  1985 => 831,  1978 => 830,  1971 => 829,  1968 => 828,  1961 => 827,  1957 => 826,  1951 => 825,  1943 => 824,  1938 => 822,  1935 => 821,  1929 => 820,  1927 => 819,  1921 => 818,  1918 => 817,  1913 => 815,  1907 => 813,  1897 => 805,  1892 => 803,  1886 => 801,  1883 => 800,  1878 => 798,  1872 => 796,  1869 => 795,  1864 => 793,  1858 => 791,  1855 => 790,  1850 => 788,  1844 => 786,  1841 => 785,  1836 => 783,  1830 => 781,  1827 => 780,  1822 => 778,  1816 => 776,  1813 => 775,  1808 => 773,  1802 => 771,  1799 => 770,  1794 => 768,  1788 => 766,  1785 => 765,  1780 => 763,  1774 => 761,  1771 => 760,  1766 => 758,  1760 => 756,  1757 => 755,  1752 => 753,  1746 => 751,  1743 => 750,  1738 => 748,  1732 => 746,  1729 => 745,  1724 => 743,  1718 => 741,  1715 => 740,  1710 => 738,  1704 => 736,  1701 => 735,  1696 => 733,  1690 => 731,  1687 => 730,  1682 => 728,  1676 => 726,  1673 => 725,  1668 => 723,  1662 => 721,  1659 => 720,  1654 => 718,  1648 => 716,  1638 => 708,  1632 => 705,  1625 => 702,  1615 => 694,  1609 => 691,  1602 => 688,  1593 => 681,  1583 => 677,  1576 => 675,  1567 => 668,  1562 => 666,  1556 => 664,  1553 => 663,  1548 => 661,  1542 => 659,  1539 => 658,  1534 => 656,  1528 => 654,  1525 => 653,  1520 => 651,  1514 => 649,  1511 => 648,  1506 => 646,  1500 => 644,  1497 => 643,  1492 => 641,  1486 => 639,  1475 => 631,  1469 => 628,  1451 => 612,  1436 => 610,  1428 => 609,  1424 => 608,  1417 => 606,  1405 => 599,  1398 => 594,  1393 => 592,  1387 => 590,  1384 => 589,  1379 => 587,  1373 => 585,  1370 => 584,  1365 => 582,  1359 => 580,  1356 => 579,  1351 => 577,  1345 => 575,  1342 => 574,  1337 => 572,  1331 => 570,  1328 => 569,  1323 => 567,  1317 => 565,  1307 => 562,  1298 => 558,  1291 => 556,  1282 => 554,  1275 => 549,  1267 => 544,  1263 => 543,  1258 => 541,  1252 => 539,  1249 => 538,  1244 => 536,  1238 => 534,  1235 => 533,  1230 => 531,  1224 => 529,  1221 => 528,  1216 => 526,  1210 => 524,  1206 => 523,  1196 => 520,  1189 => 515,  1184 => 513,  1178 => 511,  1175 => 510,  1170 => 508,  1164 => 506,  1154 => 498,  1149 => 496,  1143 => 494,  1140 => 493,  1135 => 491,  1129 => 489,  1126 => 488,  1121 => 486,  1115 => 484,  1112 => 483,  1107 => 481,  1101 => 479,  1098 => 478,  1093 => 476,  1087 => 474,  1084 => 473,  1079 => 471,  1073 => 469,  1062 => 461,  1056 => 458,  1030 => 437,  1020 => 432,  1013 => 430,  1004 => 424,  1000 => 423,  989 => 417,  984 => 414,  968 => 411,  963 => 409,  954 => 407,  949 => 404,  933 => 403,  927 => 402,  918 => 398,  912 => 394,  907 => 392,  901 => 390,  898 => 389,  893 => 387,  887 => 385,  884 => 384,  879 => 382,  873 => 380,  870 => 379,  865 => 377,  859 => 375,  849 => 367,  833 => 366,  826 => 365,  809 => 355,  801 => 354,  793 => 353,  785 => 352,  769 => 343,  761 => 342,  753 => 341,  745 => 340,  737 => 339,  729 => 338,  718 => 329,  711 => 328,  708 => 327,  701 => 326,  697 => 325,  692 => 323,  682 => 315,  677 => 313,  671 => 311,  668 => 310,  663 => 308,  657 => 306,  654 => 305,  649 => 303,  643 => 301,  633 => 294,  624 => 287,  618 => 284,  611 => 281,  601 => 273,  595 => 270,  588 => 267,  578 => 259,  572 => 256,  565 => 253,  555 => 245,  549 => 242,  542 => 239,  532 => 231,  513 => 229,  505 => 228,  501 => 227,  494 => 225,  484 => 217,  477 => 216,  474 => 215,  467 => 214,  463 => 213,  458 => 211,  446 => 204,  409 => 177,  403 => 176,  366 => 149,  360 => 148,  351 => 141,  337 => 139,  329 => 138,  325 => 137,  318 => 135,  308 => 127,  293 => 125,  285 => 124,  281 => 123,  274 => 121,  264 => 113,  249 => 111,  241 => 110,  237 => 109,  230 => 107,  223 => 102,  218 => 100,  213 => 99,  209 => 98,  205 => 97,  200 => 95,  193 => 91,  189 => 90,  164 => 67,  148 => 66,  142 => 65,  136 => 62,  128 => 57,  123 => 54,  116 => 51,  112 => 50,  106 => 47,  102 => 46,  62 => 8,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/megamenu.twig", "");
    }
}
