<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/common/header.twig */
class __TwigTemplate_95cdb769dae839ecb67f3dc00e45a3b16d2e5d436b2fc5163244de9363afbfa1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script type=\"text/javascript\">
    window.onload = function(){
        document.getElementsByClassName(\"fixed-header-1 sticky-header header-type-3 header-type-4\")[0].style.display = \"none\";
    }
    
    window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;

  // 20 is an arbitrary number here, just to make you think if you need the prevScrollpos variable:
  if (currentScrollPos > 270) {
    // I am using 'display' instead of 'top':
    document.getElementsByClassName(\"fixed-header-1 sticky-header header-type-3 header-type-4\")[0].style.display = \"initial\";
  } else {
    document.getElementsByClassName(\"fixed-header-1 sticky-header header-type-3 header-type-4\")[0].style.display = \"none\";
  }
}
</script>

";
        // line 19
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 19) == twig_constant("false"))) {
            echo " 
  <style>
    body {
      display: none !important;
    }
  </style>
  <script>
    window.location = 'themeinstall/index.php';
  </script>
";
        } else {
            // line 29
            echo "\t";
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 29);
            // line 30
            echo "\t";
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 30);
            // line 31
            echo "\t";
            $context["request"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "request"], "method", false, false, false, 31);
            // line 32
            echo "\t";
            $context["page_direction"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_direction"], "method", false, false, false, 32);
            echo " 
\t";
            // line 33
            $context["language_id"] = twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 33);
            echo " 

\t";
            // line 35
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, true, false, 35), "route", [], "array", true, true, false, 35)) {
                // line 36
                echo "\t\t";
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, true, false, 36), "product_id", [], "array", true, true, false, 36)) {
                    // line 37
                    echo "\t\t\t";
                    $context["class"] = ("-" . (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, false, false, 37)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["product_id"] ?? null) : null));
                    // line 38
                    echo "\t\t";
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, true, false, 38), "path", [], "array", true, true, false, 38)) {
                    // line 39
                    echo "\t\t\t";
                    $context["class"] = ("-" . (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, false, false, 39)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["path"] ?? null) : null));
                    // line 40
                    echo "\t\t";
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, true, false, 40), "manufacturer_id", [], "array", true, true, false, 40)) {
                    // line 41
                    echo "\t\t\t";
                    $context["class"] = ("-" . (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, false, false, 41)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["manufacturer_id"] ?? null) : null));
                    // line 42
                    echo "\t\t";
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, true, false, 42), "information_id", [], "array", true, true, false, 42)) {
                    // line 43
                    echo "\t\t\t";
                    $context["class"] = ("-" . (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, false, false, 43)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["information_id"] ?? null) : null));
                    // line 44
                    echo "\t\t";
                } else {
                    echo " 
\t\t\t";
                    // line 45
                    $context["class"] = "";
                    // line 46
                    echo "\t\t";
                }
                // line 47
                echo "
\t\t";
                // line 48
                $context["klasa"] = (twig_replace_filter((($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, false, false, 48)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["route"] ?? null) : null), ["/" => "-"]) . ($context["class"] ?? null));
                // line 49
                echo "\t";
            } else {
                echo " 
\t\t";
                // line 50
                $context["klasa"] = "common-home";
                // line 51
                echo "\t";
            }
            echo " 
\t<!DOCTYPE html>
\t<!--[if IE 7]> <html lang=\"";
            // line 53
            echo ($context["lang"] ?? null);
            echo "\" class=\"ie7 ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "responsive_design"], "method", false, false, false, 53) == "0")) {
                echo " ";
                echo "no-";
                echo " ";
            }
            echo "responsive";
            echo ((((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["page_direction"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) ? (" rtl") : (""));
            echo "\" ";
            echo ((((($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["page_direction"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) ? ("dir=\"rtl\"") : (""));
            echo "> <![endif]-->  
\t<!--[if IE 8]> <html lang=\"";
            // line 54
            echo ($context["lang"] ?? null);
            echo "\" class=\"ie8 ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "responsive_design"], "method", false, false, false, 54) == "0")) {
                echo " ";
                echo "no-";
                echo " ";
            }
            echo "responsive";
            echo ((((($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["page_direction"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) ? (" rtl") : (""));
            echo "\" ";
            echo ((((($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["page_direction"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) ? ("dir=\"rtl\"") : (""));
            echo "> <![endif]-->  
\t<!--[if IE 9]> <html lang=\"";
            // line 55
            echo ($context["lang"] ?? null);
            echo "\" class=\"ie9 ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "responsive_design"], "method", false, false, false, 55) == "0")) {
                echo " ";
                echo "no-";
                echo " ";
            }
            echo "responsive";
            echo ((((($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["page_direction"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) ? (" rtl") : (""));
            echo "\" ";
            echo ((((($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["page_direction"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) ? ("dir=\"rtl\"") : (""));
            echo "> <![endif]-->  
\t<!--[if !IE]><!--> <html lang=\"";
            // line 56
            echo ($context["lang"] ?? null);
            echo "\" class=\"";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "responsive_design"], "method", false, false, false, 56) == "0")) {
                echo " ";
                echo "no-";
                echo " ";
            }
            echo "responsive";
            echo ((((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["page_direction"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) ? (" rtl") : (""));
            echo "\" ";
            echo ((((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = ($context["page_direction"] ?? null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) ? ("dir=\"rtl\"") : (""));
            echo "> <!--<![endif]-->  
\t<head>
\t\t<title>";
            // line 58
            echo ($context["title"] ?? null);
            echo "</title>
\t\t<base href=\"";
            // line 59
            echo ($context["base"] ?? null);
            echo "\" />

\t\t<!-- Meta -->
\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
\t\t";
            // line 64
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "responsive_design"], "method", false, false, false, 64) != "0")) {
                echo " 
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t\t";
            }
            // line 66
            echo " 
\t\t";
            // line 67
            if (($context["description"] ?? null)) {
                echo " 
\t\t<meta name=\"description\" content=\"";
                // line 68
                echo strip_tags(($context["description"] ?? null));
                echo "\" />
\t\t";
            }
            // line 69
            echo " 
\t\t";
            // line 70
            if (($context["keywords"] ?? null)) {
                echo " 
\t\t<meta name=\"keywords\" content=\"";
                // line 71
                echo strip_tags(($context["keywords"] ?? null));
                echo "\" />
\t\t";
            }
            // line 72
            echo " 
\t\t
\t\t";
            // line 74
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["links"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
                echo " 
\t\t<link href=\"";
                // line 75
                echo (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["link"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["href"] ?? null) : null);
                echo "\" rel=\"";
                echo (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = $context["link"]) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["rel"] ?? null) : null);
                echo "\" />
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 76
            echo " 
\t\t
\t\t<!-- Google Fonts -->
\t\t<link href=\"//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700\" rel=\"stylesheet\" type=\"text/css\">
\t\t
\t \t";
            // line 81
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "font_status"], "method", false, false, false, 81) == "1")) {
                // line 82
                echo "\t\t\t";
                $context["lista_fontow"] = [];
                // line 83
                echo "\t \t\t";
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "body_font"], "method", false, false, false, 83) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "body_font"], "method", false, false, false, 83) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "body_font"], "method", false, false, false, 83) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "body_font"], "method", false, false, false, 83) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "body_font"], "method", false, false, false, 83) != "Times New Roman"))) {
                    // line 84
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "body_font"], "method", false, false, false, 84);
                    // line 85
                    echo "\t\t\t\t";
                    $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                    // line 86
                    echo "\t\t\t";
                }
                // line 87
                echo "\t\t\t
\t\t\t";
                // line 88
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_bar"], "method", false, false, false, 88) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_bar"], "method", false, false, false, 88) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_bar"], "method", false, false, false, 88) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_bar"], "method", false, false, false, 88) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_bar"], "method", false, false, false, 88) != "Times New Roman"))) {
                    // line 89
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_bar"], "method", false, false, false, 89);
                    // line 90
                    echo "\t\t\t\t";
                    if (!twig_in_filter(($context["font"] ?? null), ($context["lista_fontow"] ?? null))) {
                        // line 91
                        echo "\t\t\t\t\t";
                        $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                        // line 92
                        echo "\t\t\t\t";
                    }
                    // line 93
                    echo "\t\t\t";
                }
                // line 94
                echo "\t\t\t
\t\t\t";
                // line 95
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_submenu_heading"], "method", false, false, false, 95) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_submenu_heading"], "method", false, false, false, 95) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_submenu_heading"], "method", false, false, false, 95) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_submenu_heading"], "method", false, false, false, 95) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_submenu_heading"], "method", false, false, false, 95) != "Times New Roman"))) {
                    // line 96
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_submenu_heading"], "method", false, false, false, 96);
                    // line 97
                    echo "\t\t\t\t";
                    if (!twig_in_filter(($context["font"] ?? null), ($context["lista_fontow"] ?? null))) {
                        // line 98
                        echo "\t\t\t\t\t";
                        $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                        // line 99
                        echo "\t\t\t\t";
                    }
                    // line 100
                    echo "\t\t\t";
                }
                // line 101
                echo "\t\t\t
\t\t\t";
                // line 102
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_heading"], "method", false, false, false, 102) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_heading"], "method", false, false, false, 102) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_heading"], "method", false, false, false, 102) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_heading"], "method", false, false, false, 102) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_heading"], "method", false, false, false, 102) != "Times New Roman"))) {
                    // line 103
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_heading"], "method", false, false, false, 103);
                    // line 104
                    echo "\t\t\t\t";
                    if (!twig_in_filter(($context["font"] ?? null), ($context["lista_fontow"] ?? null))) {
                        // line 105
                        echo "\t\t\t\t\t";
                        $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                        // line 106
                        echo "\t\t\t\t";
                    }
                    // line 107
                    echo "\t\t\t";
                }
                // line 108
                echo "\t\t\t
\t\t\t";
                // line 109
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_links"], "method", false, false, false, 109) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_links"], "method", false, false, false, 109) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_links"], "method", false, false, false, 109) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_links"], "method", false, false, false, 109) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_links"], "method", false, false, false, 109) != "Times New Roman"))) {
                    // line 110
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "categories_box_links"], "method", false, false, false, 110);
                    // line 111
                    echo "\t\t\t\t";
                    if (!twig_in_filter(($context["font"] ?? null), ($context["lista_fontow"] ?? null))) {
                        // line 112
                        echo "\t\t\t\t\t";
                        $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                        // line 113
                        echo "\t\t\t\t";
                    }
                    // line 114
                    echo "\t\t\t";
                }
                // line 115
                echo "\t\t\t
\t\t\t";
                // line 116
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "headlines"], "method", false, false, false, 116) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "headlines"], "method", false, false, false, 116) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "headlines"], "method", false, false, false, 116) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "headlines"], "method", false, false, false, 116) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "headlines"], "method", false, false, false, 116) != "Times New Roman"))) {
                    // line 117
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "headlines"], "method", false, false, false, 117);
                    // line 118
                    echo "\t\t\t\t";
                    if (!twig_in_filter(($context["font"] ?? null), ($context["lista_fontow"] ?? null))) {
                        // line 119
                        echo "\t\t\t\t\t";
                        $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                        // line 120
                        echo "\t\t\t\t";
                    }
                    // line 121
                    echo "\t\t\t";
                }
                // line 122
                echo "\t\t\t
\t\t\t";
                // line 123
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "footer_headlines"], "method", false, false, false, 123) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "footer_headlines"], "method", false, false, false, 123) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "footer_headlines"], "method", false, false, false, 123) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "footer_headlines"], "method", false, false, false, 123) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "footer_headlines"], "method", false, false, false, 123) != "Times New Roman"))) {
                    // line 124
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "footer_headlines"], "method", false, false, false, 124);
                    // line 125
                    echo "\t\t\t\t";
                    if (!twig_in_filter(($context["font"] ?? null), ($context["lista_fontow"] ?? null))) {
                        // line 126
                        echo "\t\t\t\t\t";
                        $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                        // line 127
                        echo "\t\t\t\t";
                    }
                    // line 128
                    echo "\t\t\t";
                }
                // line 129
                echo "\t\t\t
\t\t\t";
                // line 130
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_name"], "method", false, false, false, 130) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_name"], "method", false, false, false, 130) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_name"], "method", false, false, false, 130) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_name"], "method", false, false, false, 130) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_name"], "method", false, false, false, 130) != "Times New Roman"))) {
                    // line 131
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_name"], "method", false, false, false, 131);
                    // line 132
                    echo "\t\t\t\t";
                    if (!twig_in_filter(($context["font"] ?? null), ($context["lista_fontow"] ?? null))) {
                        // line 133
                        echo "\t\t\t\t\t";
                        $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                        // line 134
                        echo "\t\t\t\t";
                    }
                    // line 135
                    echo "\t\t\t";
                }
                // line 136
                echo "\t\t\t
\t\t\t";
                // line 137
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "button_font"], "method", false, false, false, 137) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "button_font"], "method", false, false, false, 137) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "button_font"], "method", false, false, false, 137) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "button_font"], "method", false, false, false, 137) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "button_font"], "method", false, false, false, 137) != "Times New Roman"))) {
                    // line 138
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "button_font"], "method", false, false, false, 138);
                    // line 139
                    echo "\t\t\t\t";
                    if (!twig_in_filter(($context["font"] ?? null), ($context["lista_fontow"] ?? null))) {
                        // line 140
                        echo "\t\t\t\t\t";
                        $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                        // line 141
                        echo "\t\t\t\t";
                    }
                    // line 142
                    echo "\t\t\t";
                }
                // line 143
                echo "\t\t\t
\t\t\t";
                // line 144
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "custom_price"], "method", false, false, false, 144) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "custom_price"], "method", false, false, false, 144) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "custom_price"], "method", false, false, false, 144) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "custom_price"], "method", false, false, false, 144) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "custom_price"], "method", false, false, false, 144) != "Times New Roman"))) {
                    // line 145
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "custom_price"], "method", false, false, false, 145);
                    // line 146
                    echo "\t\t\t\t";
                    if (!twig_in_filter(($context["font"] ?? null), ($context["lista_fontow"] ?? null))) {
                        // line 147
                        echo "\t\t\t\t\t";
                        $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                        // line 148
                        echo "\t\t\t\t";
                    }
                    // line 149
                    echo "\t\t\t";
                }
                // line 150
                echo "\t\t\t
\t\t\t";
                // line 151
                if ((((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_new_font"], "method", false, false, false, 151) != "") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_new_font"], "method", false, false, false, 151) != "standard")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_new_font"], "method", false, false, false, 151) != "Arial")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_new_font"], "method", false, false, false, 151) != "Georgia")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_new_font"], "method", false, false, false, 151) != "Times New Roman"))) {
                    // line 152
                    echo "\t\t\t\t";
                    $context["font"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_new_font"], "method", false, false, false, 152);
                    // line 153
                    echo "\t\t\t\t";
                    if (!twig_in_filter(($context["font"] ?? null), ($context["lista_fontow"] ?? null))) {
                        // line 154
                        echo "\t\t\t\t\t";
                        $context["lista_fontow"] = twig_array_merge(($context["lista_fontow"] ?? null), [0 => ($context["font"] ?? null)]);
                        // line 155
                        echo "\t\t\t\t";
                    }
                    // line 156
                    echo "\t\t\t";
                }
                // line 157
                echo "\t\t\t
\t\t\t";
                // line 158
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["lista_fontow"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["font"]) {
                    echo " 
\t\t\t\t";
                    // line 159
                    echo (("<link href=\"//fonts.googleapis.com/css?family=" . twig_urlencode_filter($context["font"])) . ":800,700,600,500,400,300,200,100\" rel=\"stylesheet\" type=\"text/css\">");
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['font'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 161
                echo "\t\t";
            }
            // line 162
            echo "\t\t
\t\t
\t\t";
            // line 164
            $context["lista_plikow"] = [0 => "catalog/view/theme/kofi/css/bootstrap.css", 1 => "catalog/view/theme/kofi/css/animate.css", 2 => "catalog/view/theme/kofi/css/stylesheet.css", 3 => "catalog/view/theme/kofi/css/responsive.css", 4 => "catalog/view/theme/kofi/css/menu.css", 5 => "catalog/view/theme/kofi/css/owl.carousel.css", 6 => "catalog/view/theme/kofi/css/font-awesome.min.css"];
            // line 172
            echo " 
\t\t
\t\t";
            // line 175
            echo "\t\t
\t\t";
            // line 176
            if (((($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = ($context["page_direction"] ?? null)) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) {
                // line 177
                echo "\t\t ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/css/rtl.css"]);
                // line 178
                echo "\t\t ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/css/bootstrap_rtl.css"]);
                // line 179
                echo "\t\t";
            }
            // line 180
            echo "\t\t
\t\t";
            // line 182
            echo "\t\t";
            if ((twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "full_screen_background_slider_module"], "method", false, false, false, 182) != "")) {
                echo " ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/css/jquery.vegas.css"]);
            }
            // line 183
            echo "\t\t
\t\t";
            // line 185
            echo "\t\t";
            if ((twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "category_wall_module"], "method", false, false, false, 185) != "")) {
                echo " ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/css/category_wall.css"]);
            }
            // line 186
            echo "\t\t
\t\t";
            // line 188
            echo "\t\t";
            if ((twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "filter_product_module"], "method", false, false, false, 188) != "")) {
                echo " ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/css/filter_product.css"]);
            }
            // line 189
            echo "\t\t
\t\t";
            // line 191
            echo "\t\t";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_width"], "method", false, false, false, 191) == 1)) {
                echo " ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/css/wide-grid.css"]);
            }
            echo " 
\t\t
\t\t";
            // line 194
            echo "\t\t";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_width"], "method", false, false, false, 194) == 3)) {
                echo " ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/css/standard-grid.css"]);
            }
            // line 195
            echo "\t\t
\t\t";
            // line 197
            echo "\t\t";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "spacing_between_columns"], "method", false, false, false, 197) == 2)) {
                echo " ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/css/spacing_20.css"]);
            }
            // line 198
            echo "
\t\t";
            // line 199
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "compressorCodeCss", [0 => "kofi", 1 => ($context["lista_plikow"] ?? null), 2 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "compressor_code_status"], "method", false, false, false, 199), 3 => twig_constant("HTTP_SERVER")], "method", false, false, false, 199);
            echo "
\t\t
\t\t";
            // line 202
            echo "\t\t";
            $this->loadTemplate("kofi/css/custom_colors.twig", "kofi/template/common/header.twig", 202)->display($context);
            // line 203
            echo "\t\t
\t\t";
            // line 204
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "custom_code_css_status"], "method", false, false, false, 204) == 1)) {
                echo " 
\t\t<link rel=\"stylesheet\" href=\"catalog/view/theme/kofi/skins/store_";
                // line 205
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "store"], "method", false, false, false, 205);
                echo "/";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "skin"], "method", false, false, false, 205);
                echo "/css/custom_code.css\">
\t\t";
            }
            // line 206
            echo " 
\t\t
\t\t";
            // line 208
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["styles"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
                echo " 
\t\t\t";
                // line 209
                if (twig_in_filter("mf/jquery-ui.min.css", (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["style"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["href"] ?? null) : null))) {
                    echo " 
\t\t\t\t<link rel=\"";
                    // line 210
                    echo (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = $context["style"]) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["rel"] ?? null) : null);
                    echo "\" type=\"text/css\" href=\"catalog/view/theme/kofi/css/jquery-ui.min.css\" media=\"";
                    echo (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = $context["style"]) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["media"] ?? null) : null);
                    echo "\" />
\t\t\t";
                } elseif (twig_in_filter("mf/style.css", (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 =                 // line 211
$context["style"]) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["href"] ?? null) : null))) {
                    echo " 
\t\t\t\t<link rel=\"";
                    // line 212
                    echo (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = $context["style"]) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["rel"] ?? null) : null);
                    echo "\" type=\"text/css\" href=\"catalog/view/theme/kofi/css/mega_filter.css\" media=\"";
                    echo (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["style"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["media"] ?? null) : null);
                    echo "\" />
\t\t\t";
                } elseif (twig_in_filter("blog-news", (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c =                 // line 213
$context["style"]) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["href"] ?? null) : null))) {
                    echo " 
\t\t\t\t<link rel=\"";
                    // line 214
                    echo (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = $context["style"]) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["rel"] ?? null) : null);
                    echo "\" type=\"text/css\" href=\"catalog/view/theme/kofi/css/blog.css\" media=\"";
                    echo (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = $context["style"]) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["media"] ?? null) : null);
                    echo "\" />
\t\t\t";
                } elseif (((($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 =                 // line 215
$context["style"]) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["href"] ?? null) : null) != "catalog/view/javascript/jquery/owl-carousel/owl.carousel.css")) {
                    echo " 
\t\t\t\t<link rel=\"";
                    // line 216
                    echo (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = $context["style"]) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["rel"] ?? null) : null);
                    echo "\" type=\"text/css\" href=\"";
                    echo (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = $context["style"]) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["href"] ?? null) : null);
                    echo "\" media=\"";
                    echo (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = $context["style"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["media"] ?? null) : null);
                    echo "\" />
\t\t\t";
                }
                // line 217
                echo " 
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 218
            echo " 

\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/javascript/jquery/magnific/magnific-popup.css\" media=\"screen\" />
\t\t
\t\t";
            // line 222
            if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_width"], "method", false, false, false, 222) == 2) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "max_width"], "method", false, false, false, 222) > 900))) {
                echo " 
\t\t<style type=\"text/css\">
\t\t\t.standard-body .full-width .container {
\t\t\t\tmax-width: ";
                // line 225
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "max_width"], "method", false, false, false, 225);
                echo "px;
\t\t\t\t";
                // line 226
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "responsive_design"], "method", false, false, false, 226) == "0")) {
                    echo " 
\t\t\t\twidth: ";
                    // line 227
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "max_width"], "method", false, false, false, 227);
                    echo "px;
\t\t\t\t";
                }
                // line 228
                echo " 
\t\t\t}
\t\t\t
\t\t\t.main-fixed,
\t\t\t.fixed-body-2-2,
\t\t\t.standard-body .fixed2 .background {
\t\t\t\tmax-width: ";
                // line 234
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "max_width"], "method", false, false, false, 234);
                echo "px;
\t\t\t\t";
                // line 235
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "responsive_design"], "method", false, false, false, 235) == "0")) {
                    echo " 
\t\t\t\twidth: ";
                    // line 236
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "max_width"], "method", false, false, false, 236);
                    echo "px;
\t\t\t\t";
                }
                // line 237
                echo " 
\t\t\t}
\t\t\t
\t\t\t.standard-body .fixed .background {
\t\t\t     max-width: ";
                // line 241
                echo (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "max_width"], "method", false, false, false, 241) - 90);
                echo "px;
\t\t\t     ";
                // line 242
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "responsive_design"], "method", false, false, false, 242) == "0")) {
                    echo " 
\t\t\t     width: ";
                    // line 243
                    echo (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "max_width"], "method", false, false, false, 243) - 90);
                    echo "px;
\t\t\t     ";
                }
                // line 244
                echo " 
\t\t\t}
\t\t</style>
\t\t";
            }
            // line 248
            echo "\t\t  
\t    ";
            // line 249
            $context["lista_plikow"] = [];
            echo " 
\t 
\t ";
            // line 251
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/jquery-2.1.1.min.js"]);
            // line 252
            echo "\t ";
            if (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "file_exists", [0 => "catalog/view/javascript/mf/jquery-ui.min.js"], "method", false, false, false, 252)) {
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/javascript/mf/jquery-ui.min.js"]);
            }
            // line 253
            echo "\t ";
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/jquery-migrate-1.2.1.min.js"]);
            // line 254
            echo "\t ";
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/jquery.easing.1.3.js"]);
            // line 255
            echo "\t ";
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/bootstrap.min.js"]);
            // line 256
            echo "\t ";
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/twitter-bootstrap-hover-dropdown.js"]);
            // line 257
            echo "\t ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "lazy_loading_images"], "method", false, false, false, 257) != "0")) {
                echo " ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/echo.min.js"]);
            }
            // line 258
            echo "\t ";
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/common.js"]);
            // line 259
            echo "\t ";
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/tweetfeed.min.js"]);
            // line 260
            echo "\t ";
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/bootstrap-notify.min.js"]);
            // line 261
            echo "\t 
\t ";
            // line 263
            echo "\t ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_specials_countdown"], "method", false, false, false, 263) == "1")) {
                // line 264
                echo "\t ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/jquery.plugin.min.js"]);
                // line 265
                echo "\t ";
                $context["countdown"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "jquery_countdown_translate"], "method", false, false, false, 265);
                // line 266
                echo "\t ";
                $context["language_id"] = twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 266);
                // line 267
                echo "\t ";
                if (twig_get_attribute($this->env, $this->source, ($context["countdown"] ?? null), ($context["language_id"] ?? null), [], "array", true, true, false, 267)) {
                    // line 268
                    echo "\t \t";
                    $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => ("catalog/view/theme/kofi/js/countdown/" . (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = ($context["countdown"] ?? null)) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828[($context["language_id"] ?? null)] ?? null) : null))]);
                    // line 269
                    echo "\t ";
                } else {
                    echo " 
\t \t";
                    // line 270
                    $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/countdown/jquery.countdown.min.js"]);
                    // line 271
                    echo "\t ";
                }
                // line 272
                echo "\t ";
            }
            // line 273
            echo "\t 
\t ";
            // line 275
            echo "\t ";
            if ((twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "banner_module"], "method", false, false, false, 275) != "")) {
                echo " ";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/kofi/js/jquery.cycle2.min.js"]);
            }
            // line 276
            echo "\t 
\t ";
            // line 277
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "compressorCodeJs", [0 => "kofi", 1 => ($context["lista_plikow"] ?? null), 2 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "compressor_code_status"], "method", false, false, false, 277), 3 => twig_constant("HTTPS_SERVER")], "method", false, false, false, 277);
            echo "
\t    
\t    ";
            // line 280
            echo "\t\t";
            if ((twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "full_screen_background_slider_module"], "method", false, false, false, 280) != "")) {
                echo " 
\t        <script type=\"text/javascript\" src=\"catalog/view/theme/kofi/js/jquery.vegas.min.js\"></script>
\t    ";
            }
            // line 282
            echo " 
\t    
\t    <script type=\"text/javascript\" src=\"catalog/view/theme/kofi/js/owl.carousel.min.js\"></script>
\t    
\t    ";
            // line 286
            if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quick_search_autosuggest"], "method", false, false, false, 286) != "0") && (($context["klasa"] ?? null) != "account-tracking"))) {
                echo " 
\t    \t<script type=\"text/javascript\" src=\"catalog/view/theme/kofi/js/jquery-ui-1.10.4.custom.min.js\"></script>
\t    ";
            }
            // line 288
            echo " 
\t    
\t    <script type=\"text/javascript\" src=\"catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js\"></script>
\t\t
\t\t<script type=\"text/javascript\">
\t\t\tvar responsive_design = '";
            // line 293
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "responsive_design"], "method", false, false, false, 293) == "0")) {
                echo "no";
            } else {
                echo "yes";
            }
            echo "';
\t\t</script>
\t\t
\t\t";
            // line 296
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
                echo " 
\t\t\t";
                // line 297
                if (($context["script"] != "catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js")) {
                    echo " 
\t\t\t\t<script type=\"text/javascript\" src=\"";
                    // line 298
                    echo $context["script"];
                    echo "\"></script>
\t\t\t";
                }
                // line 299
                echo " 
\t\t\t";
                // line 300
                if (twig_in_filter("mega_filter.js", $context["script"])) {
                    echo " 
\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\tfunction display_MFP(view) {
\t\t\t\t\t     ";
                    // line 303
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quick_view"], "method", false, false, false, 303) == 1)) {
                        echo " 
\t\t\t\t\t     \$('.quickview a').magnificPopup({
\t\t\t\t\t          preloader: true,
\t\t\t\t\t          tLoading: '',
\t\t\t\t\t          type: 'iframe',
\t\t\t\t\t          mainClass: 'quickview',
\t\t\t\t\t          removalDelay: 200,
\t\t\t\t\t          gallery: {
\t\t\t\t\t           enabled: true
\t\t\t\t\t          }
\t\t\t\t\t     });
\t\t\t\t\t     ";
                    }
                    // line 314
                    echo " 
\t\t\t\t\t}
\t\t\t\t</script>
\t\t\t";
                }
                // line 317
                echo " 
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 318
            echo " 
\t\t
\t\t";
            // line 320
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "custom_code_javascript_status"], "method", false, false, false, 320) == 1)) {
                echo " 
\t\t\t<script type=\"text/javascript\" src=\"catalog/view/theme/kofi/skins/store_";
                // line 321
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "store"], "method", false, false, false, 321);
                echo "/";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "skin"], "method", false, false, false, 321);
                echo "/js/custom_code.js\"></script>
\t\t";
            }
            // line 322
            echo " 
\t\t
\t\t";
            // line 324
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["analytics"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
                echo " 
\t\t";
                // line 325
                echo $context["analytic"];
                echo " 
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 326
            echo "  
\t\t<!--[if lt IE 9]>
\t\t\t<script src=\"https://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
\t\t\t<script src=\"catalog/view/theme/kofi/js/respond.min.js\"></script>
\t\t<![endif]-->

        <script type=\"text/javascript\">
          function facebook_loadScript(url, callback) {
            var script = document.createElement(\"script\");
            script.type = \"text/javascript\";
            if(script.readyState) {  // only required for IE <9
              script.onreadystatechange = function() {
                if (script.readyState === \"loaded\" || script.readyState === \"complete\") {
                  script.onreadystatechange = null;
                  if (callback) {
                    callback();
                  }
                }
              };
            } else {  //Others
              if (callback) {
                script.onload = callback;
              }
            }

            script.src = url;
            document.getElementsByTagName(\"head\")[0].appendChild(script);
          }
        </script>

        <script type=\"text/javascript\">
          (function() {
            var enableCookieBar = '";
            // line 358
            echo ($context["facebook_enable_cookie_bar"] ?? null);
            echo "';
            if (enableCookieBar === 'true') {
              facebook_loadScript(\"catalog/view/javascript/facebook/cookieconsent.min.js\");

              // loading the css file
              var css = document.createElement(\"link\");
              css.setAttribute(\"rel\", \"stylesheet\");
              css.setAttribute(\"type\", \"text/css\");
              css.setAttribute(
                \"href\",
                \"catalog/view/theme/css/facebook/cookieconsent.min.css\");
              document.getElementsByTagName(\"head\")[0].appendChild(css);

              window.addEventListener(\"load\", function(){
                function setConsent() {
                  fbq(
                    'consent',
                    this.hasConsented() ? 'grant' : 'revoke'
                  );
                }
                window.cookieconsent.initialise({
                  palette: {
                    popup: {
                      background: '#237afc'
                    },
                    button: {
                      background: '#fff',
                      text: '#237afc'
                    }
                  },
                  cookie: {
                    name: fbq.consentCookieName
                  },
                  type: 'opt-out',
                  showLink: false,
                  content: {
                    dismiss: 'Agree',
                    deny: 'Opt Out',
                    header: 'Our Site Uses Cookies',
                    message: 'By clicking Agree, you agree to our <a class=\"cc-link\" href=\"https://www.facebook.com/legal/terms/update\" target=\"_blank\">terms of service</a>, <a class=\"cc-link\" href=\"https://www.facebook.com/policies/\" target=\"_blank\">privacy policy</a> and <a class=\"cc-link\" href=\"https://www.facebook.com/policies/cookies/\" target=\"_blank\">cookies policy</a>.'
                  },
                  layout: 'basic-header',
                  location: true,
                  revokable: true,
                  onInitialise: setConsent,
                  onStatusChange: setConsent,
                  onRevokeChoice: setConsent
                }, function (popup) {
                  // If this isn't open, we know that we can use cookies.
                  if (!popup.getStatus() && !popup.options.enabled) {
                    popup.setStatus(cookieconsent.status.dismiss);
                  }
                });
              });
            }
          })();
        </script>

        <script type=\"text/javascript\">
          (function() {
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');

            var enableCookieBar = '";
            // line 424
            echo ($context["facebook_enable_cookie_bar"] ?? null);
            echo "';
            if (enableCookieBar === 'true') {
              fbq.consentCookieName = 'fb_cookieconsent_status';

              (function() {
                function getCookie(t){var i=(\"; \"+document.cookie).split(\"; \"+t+\"=\");if(2==i.length)return i.pop().split(\";\").shift()}
                var consentValue = getCookie(fbq.consentCookieName);
                fbq('consent', consentValue === 'dismiss' ? 'grant' : 'revoke');
              })();
            }

            ";
            // line 435
            if (($context["facebook_pixel_id_FAE"] ?? null)) {
                // line 436
                echo "              facebook_loadScript(
                \"catalog/view/javascript/facebook/facebook_pixel.js\",
                function() {
                  var params = ";
                // line 439
                echo ($context["facebook_pixel_params_FAE"] ?? null);
                echo ";
                  _facebookAdsExtension.facebookPixel.init(
                    '";
                // line 441
                echo ($context["facebook_pixel_id_FAE"] ?? null);
                echo "',
                    ";
                // line 442
                echo ($context["facebook_pixel_pii_FAE"] ?? null);
                echo ",
                    params);
                  ";
                // line 444
                if (($context["facebook_pixel_event_params_FAE"] ?? null)) {
                    // line 445
                    echo "                    _facebookAdsExtension.facebookPixel.firePixel(
                      JSON.parse('";
                    // line 446
                    echo ($context["facebook_pixel_event_params_FAE"] ?? null);
                    echo "'));
                  ";
                }
                // line 448
                echo "                });
            ";
            }
            // line 450
            echo "          })();
        </script>
      
\t</head>\t
\t<body class=\"";
            // line 454
            echo ($context["klasa"] ?? null);
            echo " ";
            if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_width"], "method", false, false, false, 454) == 2) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "max_width"], "method", false, false, false, 454) > 1400))) {
                echo " ";
                echo "body-full-width";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_list_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("product-list-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_list_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("product-grid-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "dropdown_menu_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("dropdown-menu-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "dropdown_menu_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "products_buttons_action"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("products-buttons-action-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "products_buttons_action"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "buttons_prev_next_in_slider"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("buttons-prev-next-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "buttons_prev_next_in_slider"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "inputs_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("inputs-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "inputs_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "cart_block_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("cart-block-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "cart_block_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "my_account_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("my-account-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "my_account_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "top_bar_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("top-bar-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "top_bar_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "show_vertical_menu_category_page"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo "show-vertical-megamenu-category-page";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "show_vertical_menu_product_page"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo "show-vertical-megamenu-product-page";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "show_vertical_menu"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo "show-vertical-megamenu";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_page_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("product-page-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_page_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "megamenu_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("megamenu-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "megamenu_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "search_type_in_header"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("search-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "search_type_in_header"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "megamenu_label_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("megamenu-label-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "megamenu_label_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "box_type"], "method", false, false, false, 454) == 7)) {
                echo " ";
                echo "box-type-4";
                echo " ";
            } else {
                echo " ";
                echo "no-box-type-7";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "box_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("box-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "box_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_margin_top"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("header-margin-top-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_margin_top"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_new_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("sale-new-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_new_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "button_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("button-body-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "button_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "countdown_special"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("countdown-special-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "countdown_special"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "footer_type"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("footer-type-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "footer_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "breadcrumb_style"], "method", false, false, false, 454) > 0)) {
                echo " ";
                echo ("breadcrumb-style-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "breadcrumb_style"], "method", false, false, false, 454));
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "border_width"], "method", false, false, false, 454) == "1")) {
                echo " ";
                echo "border-width-1";
                echo " ";
            } else {
                echo " ";
                echo "border-width-0";
                echo " ";
            }
            echo " ";
            if (((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "body_background_color"], "method", false, false, false, 454) == "#ffffff") || ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_content_background_color"], "method", false, false, false, 454) == twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "body_background_color"], "method", false, false, false, 454)) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "body_background_color"], "method", false, false, false, 454) != ""))) || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_content_background_color"], "method", false, false, false, 454) == "none")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "colors_status"], "method", false, false, false, 454) == "1"))) {
                echo " ";
                echo "body-white";
                echo " ";
            } else {
                echo " ";
                echo "body-other";
                echo " ";
            }
            echo " ";
            if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_content_background_color"], "method", false, false, false, 454) == "none") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "colors_status"], "method", false, false, false, 454) == "1"))) {
                echo " ";
                echo "body-white-type-2";
                echo " ";
            }
            echo " ";
            if ((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_content_background_color"], "method", false, false, false, 454) == "none") && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "box_with_products_background_color"], "method", false, false, false, 454) == "#ffffff")) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "colors_status"], "method", false, false, false, 454) == "1"))) {
                echo " ";
                echo "body-white-type-3";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "hover_effect"], "method", false, false, false, 454) == "1")) {
                echo " ";
                echo ("banners-effect-" . twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "hover_effect_type"], "method", false, false, false, 454));
                echo " ";
            }
            echo " body-header-type-";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 454);
            echo "\">
\t";
            // line 455
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_facebook_status"], "method", false, false, false, 455) == 1)) {
                echo " 
\t<div class=\"facebook_";
                // line 456
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_facebook_position"], "method", false, false, false, 456) == 1)) {
                    echo "left";
                } else {
                    echo "right";
                }
                echo " hidden-xs hidden-sm\">
\t\t<div class=\"facebook-icon\"></div>
\t\t<div class=\"facebook-content\">
\t\t\t<script>(function(d, s, id) {
\t\t\t  var js, fjs = d.getElementsByTagName(s)[0];
\t\t\t  if (d.getElementById(id)) return;
\t\t\t  js = d.createElement(s); js.id = id;
\t\t\t  js.src = \"//connect.facebook.net/en_US/all.js#xfbml=1\";
\t\t\t  fjs.parentNode.insertBefore(js, fjs);
\t\t\t}(document, 'script', 'facebook-jssdk'));</script>
\t\t\t
\t\t\t<div class=\"fb-like-box fb_iframe_widget\" profile_id=\"";
                // line 467
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_facebook_id"], "method", false, false, false, 467);
                echo "\" data-colorscheme=\"light\" data-height=\"370\" data-connections=\"16\" fb-xfbml-state=\"rendered\"></div>
\t\t</div>
\t\t
\t\t<script type=\"text/javascript\">    
\t\t\$(function() {  
\t\t\t\$(\".facebook_right\").hover(function() {            
\t\t\t\t\$(\".facebook_right\").stop(true, false).animate({right: \"0\"}, 800, 'easeOutQuint');        
\t\t\t}, function() {            
\t\t\t\t\$(\".facebook_right\").stop(true, false).animate({right: \"-308\"}, 800, 'easeInQuint');        
\t\t\t}, 1000);    
\t\t
\t\t\t\$(\".facebook_left\").hover(function() {            
\t\t\t\t\$(\".facebook_left\").stop(true, false).animate({left: \"0\"}, 800, 'easeOutQuint');        
\t\t\t}, function() {            
\t\t\t\t\$(\".facebook_left\").stop(true, false).animate({left: \"-308\"}, 800, 'easeInQuint');        
\t\t\t}, 1000);    
\t\t});  
\t\t</script>
\t</div>
\t";
            }
            // line 486
            echo " 

\t";
            // line 488
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_twitter_status"], "method", false, false, false, 488) == 1)) {
                echo " 
\t<div class=\"twitter_";
                // line 489
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_twitter_position"], "method", false, false, false, 489) == 1)) {
                    echo "left";
                } else {
                    echo "right";
                }
                echo " hidden-xs hidden-sm\">
\t\t<div class=\"twitter-icon\"></div>
\t\t<div class=\"twitter-content\">
\t\t\t<a class=\"twitter-timeline\" href=\"https://twitter.com/";
                // line 492
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_twitter_user_name"], "method", false, false, false, 492);
                echo "\" data-tweet-limit=\"";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_twitter_limit"], "method", false, false, false, 492);
                echo "\">Tweets by ";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_twitter_user_name"], "method", false, false, false, 492);
                echo "</a> <script async src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script>
\t\t</div>
\t\t
\t\t<script type=\"text/javascript\">    
\t\t\$(function() {  
\t\t\t\$(\".twitter_right\").hover(function() {            
\t\t\t\t\$(\".twitter_right\").stop(true, false).animate({right: \"0\"}, 800, 'easeOutQuint');        
\t\t\t}, function() {            
\t\t\t\t\$(\".twitter_right\").stop(true, false).animate({right: \"-308\"}, 800, 'easeInQuint');        
\t\t\t}, 1000);    
\t\t
\t\t\t\$(\".twitter_left\").hover(function() {            
\t\t\t\t\$(\".twitter_left\").stop(true, false).animate({left: \"0\"}, 800, 'easeOutQuint');        
\t\t\t}, function() {            
\t\t\t\t\$(\".twitter_left\").stop(true, false).animate({left: \"-308\"}, 800, 'easeInQuint');        
\t\t\t}, 1000);    
\t\t});  
\t\t</script>
\t</div>
\t";
            }
            // line 512
            echo "
\t";
            // line 513
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_custom_status"], "method", false, false, false, 513) == 1)) {
                echo " 
\t<div class=\"custom_";
                // line 514
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_custom_position"], "method", false, false, false, 514) == 1)) {
                    echo "left";
                } else {
                    echo "right";
                }
                echo " hidden-xs hidden-sm\">
\t\t<div class=\"custom-icon\"></div>
\t\t<div class=\"custom-content\">
\t\t\t";
                // line 517
                $context["lang_id"] = twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 517);
                echo " 
\t\t\t";
                // line 518
                $context["custom_content"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "widget_custom_content"], "method", false, false, false, 518);
                echo " 
\t\t\t";
                // line 519
                if (twig_get_attribute($this->env, $this->source, ($context["custom_content"] ?? null), ($context["lang_id"] ?? null), [], "array", true, true, false, 519)) {
                    echo " ";
                    echo (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = ($context["custom_content"] ?? null)) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd[($context["lang_id"] ?? null)] ?? null) : null);
                }
                echo " 
\t\t</div>
\t\t
\t\t<script type=\"text/javascript\">    
\t\t\$(function() {  
\t\t\t\$(\".custom_right\").hover(function() {            
\t\t\t\t\$(\".custom_right\").stop(true, false).animate({right: \"0\"}, 800, 'easeOutQuint');        
\t\t\t}, function() {            
\t\t\t\t\$(\".custom_right\").stop(true, false).animate({right: \"-308\"}, 800, 'easeInQuint');        
\t\t\t}, 1000);    
\t\t
\t\t\t\$(\".custom_left\").hover(function() {            
\t\t\t\t\$(\".custom_left\").stop(true, false).animate({left: \"0\"}, 800, 'easeOutQuint');        
\t\t\t}, function() {            
\t\t\t\t\$(\".custom_left\").stop(true, false).animate({left: \"-308\"}, 800, 'easeInQuint');        
\t\t\t}, 1000);    
\t\t});  
\t\t</script>
\t\t
\t</div>
\t";
            }
            // line 539
            echo " 

\t";
            // line 541
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quick_view"], "method", false, false, false, 541) == 1)) {
                echo " 
\t<script type=\"text/javascript\">
\t\$(window).load(function(){
\t     \$('.quickview a').magnificPopup({
\t          preloader: true,
\t          tLoading: '',
\t          type: 'iframe',
\t          mainClass: 'quickview',
\t          removalDelay: 200,
\t          gallery: {
\t           enabled: true
\t          }
\t     });
\t});
\t</script>
\t";
            }
            // line 556
            echo " 

\t";
            // line 558
            $context["popup"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "popup"], "method", false, false, false, 558);
            // line 559
            echo "\t";
            if ((twig_length_filter($this->env, ($context["popup"] ?? null)) > 0)) {
                echo " 
\t\t";
                // line 560
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["popup"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t";
                    // line 561
                    echo $context["module"];
                    echo "
\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 563
                echo "\t";
            }
            echo " 


\t";
            // line 566
            $context["header_notice"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "header_notice"], "method", false, false, false, 566);
            // line 567
            echo "\t";
            if ((twig_length_filter($this->env, ($context["header_notice"] ?? null)) > 0)) {
                echo " 
\t\t";
                // line 568
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["header_notice"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t";
                    // line 569
                    echo $context["module"];
                    echo "
\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 571
                echo "\t";
            }
            // line 572
            echo "

\t";
            // line 574
            $context["cookie"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "cookie"], "method", false, false, false, 574);
            // line 575
            echo "\t";
            if ((twig_length_filter($this->env, ($context["cookie"] ?? null)) > 0)) {
                echo " 
\t\t";
                // line 576
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["cookie"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t";
                    // line 577
                    echo $context["module"];
                    echo "
\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 579
                echo "\t";
            }
            echo " 


\t <div class=\"";
            // line 582
            if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 582) == 1) || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 582) == 5))) {
                echo " ";
                echo "standard-body";
                echo " ";
            } else {
                echo " ";
                echo "fixed-body";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 582) == 7)) {
                echo " ";
                echo " fixed-body-shoes";
                echo " ";
            }
            echo " ";
            if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 582) == 4) || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 582) == 6))) {
                echo " ";
                echo " fixed-body-2";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 582) == 5)) {
                echo " ";
                echo " fixed-body-2-2";
                echo " ";
            }
            echo " ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 582) == 3)) {
                echo " ";
                echo " with-shadow";
                echo " ";
            }
            echo "\">
\t\t<div id=\"main\" class=\"";
            // line 583
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 583) == 4)) {
                echo " ";
                echo "main-fixed2 main-fixed";
                echo " ";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 583) == 6)) {
                echo " ";
                echo "main-fixed2 main-fixed3 main-fixed";
                echo " ";
            } elseif (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 583) != 1) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_layout"], "method", false, false, false, 583) != 5))) {
                echo " ";
                echo "main-fixed";
                echo " ";
            }
            echo "\">

\t\t\t";
            // line 585
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 585) == 2)) {
                // line 586
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_02.twig", "kofi/template/common/header.twig", 586)->display($context);
                // line 587
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 587) == 3)) {
                // line 588
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_03.twig", "kofi/template/common/header.twig", 588)->display($context);
                // line 589
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 589) == 4)) {
                // line 590
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_04.twig", "kofi/template/common/header.twig", 590)->display($context);
                // line 591
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 591) == 5)) {
                // line 592
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_05.twig", "kofi/template/common/header.twig", 592)->display($context);
                // line 593
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 593) == 6)) {
                // line 594
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_06.twig", "kofi/template/common/header.twig", 594)->display($context);
                // line 595
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 595) == 7)) {
                // line 596
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_07.twig", "kofi/template/common/header.twig", 596)->display($context);
                // line 597
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 597) == 8)) {
                // line 598
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_08.twig", "kofi/template/common/header.twig", 598)->display($context);
                // line 599
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 599) == 9)) {
                // line 600
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_09.twig", "kofi/template/common/header.twig", 600)->display($context);
                // line 601
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 601) == 10)) {
                // line 602
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_10.twig", "kofi/template/common/header.twig", 602)->display($context);
                // line 603
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 603) == 11)) {
                // line 604
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_11.twig", "kofi/template/common/header.twig", 604)->display($context);
                // line 605
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 605) == 12)) {
                // line 606
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_12.twig", "kofi/template/common/header.twig", 606)->display($context);
                // line 607
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 607) == 13)) {
                // line 608
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_13.twig", "kofi/template/common/header.twig", 608)->display($context);
                // line 609
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 609) == 14)) {
                // line 610
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_14.twig", "kofi/template/common/header.twig", 610)->display($context);
                // line 611
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 611) == 15)) {
                // line 612
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_15.twig", "kofi/template/common/header.twig", 612)->display($context);
                // line 613
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 613) == 16)) {
                // line 614
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_16.twig", "kofi/template/common/header.twig", 614)->display($context);
                // line 615
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 615) == 17)) {
                // line 616
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_17.twig", "kofi/template/common/header.twig", 616)->display($context);
                // line 617
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 617) == 18)) {
                // line 618
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_18.twig", "kofi/template/common/header.twig", 618)->display($context);
                // line 619
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 619) == 19)) {
                // line 620
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_19.twig", "kofi/template/common/header.twig", 620)->display($context);
                // line 621
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 621) == 20)) {
                // line 622
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_20.twig", "kofi/template/common/header.twig", 622)->display($context);
                // line 623
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 623) == 21)) {
                // line 624
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_21.twig", "kofi/template/common/header.twig", 624)->display($context);
                // line 625
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 625) == 22)) {
                // line 626
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_22.twig", "kofi/template/common/header.twig", 626)->display($context);
                // line 627
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 627) == 23)) {
                // line 628
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_23.twig", "kofi/template/common/header.twig", 628)->display($context);
                // line 629
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 629) == 24)) {
                // line 630
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_24.twig", "kofi/template/common/header.twig", 630)->display($context);
                // line 631
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 631) == 25)) {
                // line 632
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_25.twig", "kofi/template/common/header.twig", 632)->display($context);
                // line 633
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 633) == 26)) {
                // line 634
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_26.twig", "kofi/template/common/header.twig", 634)->display($context);
                // line 635
                echo "\t\t\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 635) == 27)) {
                // line 636
                echo "\t\t\t\t";
                $this->loadTemplate("kofi/template/common/header/header_27.twig", "kofi/template/common/header.twig", 636)->display($context);
                // line 637
                echo "\t\t\t";
            } else {
                echo " 
\t\t\t\t";
                // line 638
                $this->loadTemplate("kofi/template/common/header/header_01.twig", "kofi/template/common/header.twig", 638)->display($context);
                // line 639
                echo "\t\t\t";
            }
            echo " 
";
        }
        // line 641
        echo "
";
    }

    public function getTemplateName()
    {
        return "kofi/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1784 => 641,  1778 => 639,  1776 => 638,  1771 => 637,  1768 => 636,  1765 => 635,  1762 => 634,  1759 => 633,  1756 => 632,  1753 => 631,  1750 => 630,  1747 => 629,  1744 => 628,  1741 => 627,  1738 => 626,  1735 => 625,  1732 => 624,  1729 => 623,  1726 => 622,  1723 => 621,  1720 => 620,  1717 => 619,  1714 => 618,  1711 => 617,  1708 => 616,  1705 => 615,  1702 => 614,  1699 => 613,  1696 => 612,  1693 => 611,  1690 => 610,  1687 => 609,  1684 => 608,  1681 => 607,  1678 => 606,  1675 => 605,  1672 => 604,  1669 => 603,  1666 => 602,  1663 => 601,  1660 => 600,  1657 => 599,  1654 => 598,  1651 => 597,  1648 => 596,  1645 => 595,  1642 => 594,  1639 => 593,  1636 => 592,  1633 => 591,  1630 => 590,  1627 => 589,  1624 => 588,  1621 => 587,  1618 => 586,  1616 => 585,  1599 => 583,  1563 => 582,  1556 => 579,  1548 => 577,  1542 => 576,  1537 => 575,  1535 => 574,  1531 => 572,  1528 => 571,  1520 => 569,  1514 => 568,  1509 => 567,  1507 => 566,  1500 => 563,  1492 => 561,  1486 => 560,  1481 => 559,  1479 => 558,  1475 => 556,  1456 => 541,  1452 => 539,  1425 => 519,  1421 => 518,  1417 => 517,  1407 => 514,  1403 => 513,  1400 => 512,  1373 => 492,  1363 => 489,  1359 => 488,  1355 => 486,  1332 => 467,  1314 => 456,  1310 => 455,  1112 => 454,  1106 => 450,  1102 => 448,  1097 => 446,  1094 => 445,  1092 => 444,  1087 => 442,  1083 => 441,  1078 => 439,  1073 => 436,  1071 => 435,  1057 => 424,  988 => 358,  954 => 326,  946 => 325,  940 => 324,  936 => 322,  929 => 321,  925 => 320,  921 => 318,  914 => 317,  908 => 314,  893 => 303,  887 => 300,  884 => 299,  879 => 298,  875 => 297,  869 => 296,  859 => 293,  852 => 288,  846 => 286,  840 => 282,  833 => 280,  828 => 277,  825 => 276,  819 => 275,  816 => 273,  813 => 272,  810 => 271,  808 => 270,  803 => 269,  800 => 268,  797 => 267,  794 => 266,  791 => 265,  788 => 264,  785 => 263,  782 => 261,  779 => 260,  776 => 259,  773 => 258,  767 => 257,  764 => 256,  761 => 255,  758 => 254,  755 => 253,  750 => 252,  748 => 251,  743 => 249,  740 => 248,  734 => 244,  729 => 243,  725 => 242,  721 => 241,  715 => 237,  710 => 236,  706 => 235,  702 => 234,  694 => 228,  689 => 227,  685 => 226,  681 => 225,  675 => 222,  669 => 218,  662 => 217,  653 => 216,  649 => 215,  643 => 214,  639 => 213,  633 => 212,  629 => 211,  623 => 210,  619 => 209,  613 => 208,  609 => 206,  602 => 205,  598 => 204,  595 => 203,  592 => 202,  587 => 199,  584 => 198,  578 => 197,  575 => 195,  569 => 194,  560 => 191,  557 => 189,  551 => 188,  548 => 186,  542 => 185,  539 => 183,  533 => 182,  530 => 180,  527 => 179,  524 => 178,  521 => 177,  519 => 176,  516 => 175,  512 => 172,  510 => 164,  506 => 162,  503 => 161,  495 => 159,  489 => 158,  486 => 157,  483 => 156,  480 => 155,  477 => 154,  474 => 153,  471 => 152,  469 => 151,  466 => 150,  463 => 149,  460 => 148,  457 => 147,  454 => 146,  451 => 145,  449 => 144,  446 => 143,  443 => 142,  440 => 141,  437 => 140,  434 => 139,  431 => 138,  429 => 137,  426 => 136,  423 => 135,  420 => 134,  417 => 133,  414 => 132,  411 => 131,  409 => 130,  406 => 129,  403 => 128,  400 => 127,  397 => 126,  394 => 125,  391 => 124,  389 => 123,  386 => 122,  383 => 121,  380 => 120,  377 => 119,  374 => 118,  371 => 117,  369 => 116,  366 => 115,  363 => 114,  360 => 113,  357 => 112,  354 => 111,  351 => 110,  349 => 109,  346 => 108,  343 => 107,  340 => 106,  337 => 105,  334 => 104,  331 => 103,  329 => 102,  326 => 101,  323 => 100,  320 => 99,  317 => 98,  314 => 97,  311 => 96,  309 => 95,  306 => 94,  303 => 93,  300 => 92,  297 => 91,  294 => 90,  291 => 89,  289 => 88,  286 => 87,  283 => 86,  280 => 85,  277 => 84,  274 => 83,  271 => 82,  269 => 81,  262 => 76,  252 => 75,  246 => 74,  242 => 72,  237 => 71,  233 => 70,  230 => 69,  225 => 68,  221 => 67,  218 => 66,  212 => 64,  204 => 59,  200 => 58,  185 => 56,  171 => 55,  157 => 54,  143 => 53,  137 => 51,  135 => 50,  130 => 49,  128 => 48,  125 => 47,  122 => 46,  120 => 45,  115 => 44,  112 => 43,  109 => 42,  106 => 41,  103 => 40,  100 => 39,  97 => 38,  94 => 37,  91 => 36,  89 => 35,  84 => 33,  79 => 32,  76 => 31,  73 => 30,  70 => 29,  57 => 19,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/common/header.twig", "");
    }
}
