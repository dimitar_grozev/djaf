<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/information/information.twig */
class __TwigTemplate_848a145ba2ac5998a3c771e06d228eaf752dc7d16868c3376f1ac5dd85669008 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $this->loadTemplate("kofi/template/new_elements/wrapper_top.twig", "kofi/template/information/information.twig", 2)->display($context);
        // line 3
        echo "
<div style=\"padding-bottom: 10px\">";
        // line 4
        echo ($context["description"] ?? null);
        echo "</div>

";
        // line 6
        $this->loadTemplate("kofi/template/new_elements/wrapper_bottom.twig", "kofi/template/information/information.twig", 6)->display($context);
        // line 7
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "kofi/template/information/information.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 7,  51 => 6,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/information/information.twig", "");
    }
}
