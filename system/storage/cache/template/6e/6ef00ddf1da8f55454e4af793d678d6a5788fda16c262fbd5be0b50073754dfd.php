<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/total/order_discount.twig */
class __TwigTemplate_fb7b7d228ecaf2d1585871fe2ddffaab4910cd1091320539d5545a4852e08b8e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        $context["links"] = ["Opencart Marketplace" => "https://www.opencart.com/index.php?route=marketplace/extension/info&extension_id=33296", "Homepage" => "https://underr.space/en/notes/projects/project-0001.html", "Github" => "https://git.io/JvumR", "LICENSE" => "https://git.io/Jvuma"];
        // line 13
        echo "
";
        // line 14
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-total\" data-toggle=\"tooltip\" title=\"";
        // line 19
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\">
          <i class=\"fa fa-save\"></i>
        </button>
        <a href=\"";
        // line 22
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\">
          <i class=\"fa fa-reply\"></i>
        </a>
      </div>
      <h1>";
        // line 26
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 30
            echo "<li>
          <a href=\"";
            // line 31
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 31);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 31);
            echo "</a>
        </li>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "</ul>
    </div>
  </div>
  <div class=\"container-fluid\">";
        // line 39
        if (($context["error_permission"] ?? null)) {
            // line 41
            echo "<div class=\"alert alert-danger alert-dismissible\">
      <i class=\"fa fa-exclamation-circle\"></i>
      ";
            // line 43
            echo ($context["error_permission"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>";
        }
        // line 48
        echo "<div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\">
          <i class=\"fa fa-pencil\"></i>
          ";
        // line 52
        echo ($context["text_edit"] ?? null);
        echo "
        </h3>
      </div>
      <div class=\"panel-body\">
        <form
          action=\"";
        // line 57
        echo ($context["action"] ?? null);
        echo "\"
          method=\"post\"
          enctype=\"multipart/form-data\"
          id=\"form-total\"
          class=\"form-horizontal\">

          <div class=\"form-group\">
            <div
              class=\"btn-group btn-group-toggle col-sm-12 text-left\"
              data-toggle=\"buttons\"
              id=\"input-status\">
              <label class=\"btn btn-default";
        // line 68
        if (($context["status"] ?? null)) {
            echo " active btn-success";
        }
        echo "\">
                <input
                  autocomplete=\"off\"
                  name=\"total_order_discount_status\"
                  type=\"radio\"
                  value=\"1\"
                  ";
        // line 74
        if (($context["status"] ?? null)) {
            echo "checked";
        }
        echo "/>
                  ";
        // line 75
        echo ($context["text_enabled"] ?? null);
        echo "
              </label>
              <label class=\"btn btn-default";
        // line 77
        if ( !($context["status"] ?? null)) {
            echo " active btn-success";
        }
        echo "\">
                <input
                  autocomplete=\"off\"
                  name=\"total_order_discount_status\"
                  type=\"radio\"
                  value=\"0\"
                  ";
        // line 83
        if ( !($context["status"] ?? null)) {
            echo "checked";
        }
        echo ">
                  ";
        // line 84
        echo ($context["text_disabled"] ?? null);
        echo "
              </label>
            </div>
          </div>

          <ul class=\"nav nav-tabs\" id=\"tabs\">
              <li class=\"active\">
                  <a data-toggle=\"tab\" href=\"#tab-parameters\">
                      ";
        // line 92
        echo ($context["entry_tab_parameters"] ?? null);
        echo "
                  </a>
              </li>
              <li ";
        // line 95
        if (($context["error_discount_title"] ?? null)) {
            echo "style=\"border: 1px solid red;\"";
        }
        echo ">
                  <a data-toggle=\"tab\" href=\"#tab-text\">
                      ";
        // line 97
        echo ($context["entry_tab_text"] ?? null);
        echo "
                  </a>
              </li>
              <li>
                  <a data-toggle=\"tab\" href=\"#tab-info\">
                      <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>
                  </a>
              </li>
          </ul>

          <div class=\"tab-content\">
            <div class=\"tab-pane active\" id=\"tab-parameters\">
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"select-condition\">";
        // line 110
        echo ($context["entry_condition"] ?? null);
        echo "</label>
                <div class=\"col-sm-6\">
                  <select name=\"total_order_discount[condition]\" id=\"select-condition\" class=\"form-control\">";
        // line 113
        if ((twig_get_attribute($this->env, $this->source, ($context["discount"] ?? null), "condition", [], "any", false, false, false, 113) == "quantity")) {
            // line 115
            echo "<option value=\"price\">";
            echo ($context["text_price"] ?? null);
            echo "</option>
                    <option value=\"quantity\" selected=\"selected\">";
            // line 116
            echo ($context["text_quantity"] ?? null);
            echo "</option>";
        } else {
            // line 120
            echo "<option value=\"price\" selected=\"selected\">";
            echo ($context["text_price"] ?? null);
            echo "</option>
                    <option value=\"quantity\">";
            // line 121
            echo ($context["text_quantity"] ?? null);
            echo "</option>";
        }
        // line 124
        echo "</select>
                </div>
              </div>

              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"select-type\">";
        // line 129
        echo ($context["entry_type"] ?? null);
        echo "</label>
                <div class=\"col-sm-6\">
                  <select name=\"total_order_discount[type]\" id=\"select-type\" class=\"form-control\">
                    ";
        // line 132
        if ((twig_get_attribute($this->env, $this->source, ($context["discount"] ?? null), "type", [], "any", false, false, false, 132) == "fixed")) {
            // line 133
            echo "                    <option value=\"percentage\">";
            echo ($context["text_percentage"] ?? null);
            echo "</option>
                    <option value=\"fixed\" selected=\"selected\">";
            // line 134
            echo ($context["text_fixed"] ?? null);
            echo "</option>
                    ";
        } else {
            // line 136
            echo "                    <option value=\"percentage\" selected=\"selected\">";
            echo ($context["text_percentage"] ?? null);
            echo "</option>
                    <option value=\"fixed\">";
            // line 137
            echo ($context["text_fixed"] ?? null);
            echo "</option>
                    ";
        }
        // line 139
        echo "                  </select>
                </div>
              </div>

              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-key\">";
        // line 144
        echo ($context["entry_key"] ?? null);
        echo "</label>
                <div class=\"col-sm-2\">
                  <input
                    class=\"form-control\"
                    id=\"input-key\"
                    min=\"0\"
                    name=\"total_order_discount[key]\"
                    placeholder=\"";
        // line 151
        echo ($context["entry_key"] ?? null);
        echo "\"
                    type=\"number\"
                    step=\"any\"
                    value=\"";
        // line 154
        if (twig_get_attribute($this->env, $this->source, ($context["discount"] ?? null), "key", [], "any", false, false, false, 154)) {
            echo twig_get_attribute($this->env, $this->source, ($context["discount"] ?? null), "key", [], "any", false, false, false, 154);
        } else {
            echo "0";
        }
        echo "\"/>
                </div>

                <label class=\"col-sm-2 control-label\" for=\"input-value\">
                  ";
        // line 158
        echo ($context["entry_value"] ?? null);
        echo "
                  <span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 159
        echo ($context["help_discount_value"] ?? null);
        echo "\"></span>
                </label>
                <div class=\"col-sm-2\">
                  <input
                    class=\"form-control\"
                    id=\"input-value\"
                    name=\"total_order_discount[value]\"
                    placeholder=\"";
        // line 166
        echo ($context["entry_value"] ?? null);
        echo "\"
                    type=\"number\"
                    value=\"";
        // line 168
        if (twig_get_attribute($this->env, $this->source, ($context["discount"] ?? null), "value", [], "any", false, false, false, 168)) {
            echo twig_get_attribute($this->env, $this->source, ($context["discount"] ?? null), "value", [], "any", false, false, false, 168);
        } else {
            echo "0";
        }
        echo "\"
                    step=\"any\"/>
                </div>
              </div>

              <div class=\"form-group\">
                <label
                  class=\"col-sm-2 control-label\"
                  for=\"input-sort-order\">
                  ";
        // line 177
        echo ($context["entry_sort_order"] ?? null);
        echo "
                </label>
                <div class=\"col-sm-6\">
                  <input
                    class=\"form-control\"
                    id=\"input-sort-order\"
                    min=\"0\"
                    name=\"total_order_discount[sort_order]\"
                    placeholder=\"";
        // line 185
        echo ($context["entry_sort_order"] ?? null);
        echo "\"
                    step=\"1\"
                    type=\"number\"
                    value=\"";
        // line 188
        echo twig_get_attribute($this->env, $this->source, ($context["discount"] ?? null), "sort_order", [], "any", false, false, false, 188);
        echo "\"/>
                </div>
              </div>
            </div>

            <div class=\"tab-pane\" id=\"tab-text\">

              <table class=\"table table-striped table-bordered table-hover\">";
        // line 196
        if (($context["error_discount_title"] ?? null)) {
            // line 198
            echo "<div class=\"alert alert-danger alert-dismissible\">
                  <i class=\"fa fa-exclamation-circle\"></i>
                  ";
            // line 200
            echo ($context["error_discount_title"] ?? null);
            echo "
                  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                </div>";
        }
        // line 206
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 208
            echo "<tr>
                  <td>
                    <div>
                      <label class=\"control-label\">
                        ";
            // line 212
            echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["entry_title"] ?? null) : null);
            echo "
                      </label>
                     <div class=\"input-group\">
                        <span class=\"input-group-addon\">
                          <img
                            src=\"language/";
            // line 217
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 217);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 217);
            echo ".png\"
                            title=\"";
            // line 218
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 218);
            echo "\"/>
                        </span>
                        <input
                          class=\"form-control\"
                          name=\"total_order_discount[title][";
            // line 222
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 222);
            echo "]\"
                          placeholder=\"";
            // line 223
            echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["text_title"] ?? null) : null);
            echo "\"
                          type=\"text\"
                          value=\"";
            // line 225
            echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_get_attribute($this->env, $this->source, ($context["discount"] ?? null), "title", [], "any", false, false, false, 225)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 225)] ?? null) : null);
            echo "\"/>
                      </div>
                    </div>
                  </td>
                </tr>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 232
        echo "</table>
            </div>

            <div class=\"tab-pane\" id=\"tab-info\">
              <div class=\"col-sm-12 text-center\">";
        // line 236
        echo ($context["text_about"] ?? null);
        echo "</div>
              <div class=\"text-center\">";
        // line 238
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["links"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["name"] => $context["url"]) {
            // line 240
            echo "<a href=\"";
            echo $context["url"];
            echo "\" target=\"_blank\">";
            echo $context["name"];
            echo "</a>
                ";
            // line 241
            if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 241)) {
                echo " | ";
            }
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['url'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 244
        echo "</div>
              <hr/>
              <div class=\"text-center\">";
        // line 246
        echo ($context["copyright"] ?? null);
        echo ", ";
        echo ($context["email"] ?? null);
        echo "</div>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>

<script type=\"text/javascript\"><!--

    function setValueRange(type) {
        if (type == 'percentage') {
            \$('#input-value').attr({'max': '100'}).attr({'min': '-100'});
        } else if (type == \"fixed\") {
            \$('#input-value').removeAttr('min').removeAttr('max');
        }
    }

    \$('#select-type').on('change', function() {
        setValueRange(\$(this).val());
    });

    setValueRange(\$('#select-type').val());

    \$('#input-status .btn.btn-default').click(function () {
        if (!\$(this).hasClass('active')) {
            \$('#input-status .btn.btn-default').removeClass('btn-success');

            \$(this).addClass('btn-success');
        }
    });

--></script>
";
        // line 281
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/total/order_discount.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  527 => 281,  487 => 246,  483 => 244,  467 => 241,  460 => 240,  443 => 238,  439 => 236,  433 => 232,  422 => 225,  417 => 223,  413 => 222,  406 => 218,  400 => 217,  392 => 212,  386 => 208,  382 => 206,  376 => 200,  372 => 198,  370 => 196,  360 => 188,  354 => 185,  343 => 177,  327 => 168,  322 => 166,  312 => 159,  308 => 158,  297 => 154,  291 => 151,  281 => 144,  274 => 139,  269 => 137,  264 => 136,  259 => 134,  254 => 133,  252 => 132,  246 => 129,  239 => 124,  235 => 121,  230 => 120,  226 => 116,  221 => 115,  219 => 113,  214 => 110,  198 => 97,  191 => 95,  185 => 92,  174 => 84,  168 => 83,  157 => 77,  152 => 75,  146 => 74,  135 => 68,  121 => 57,  113 => 52,  107 => 48,  101 => 43,  97 => 41,  95 => 39,  90 => 35,  80 => 31,  77 => 30,  73 => 28,  69 => 26,  60 => 22,  54 => 19,  45 => 14,  42 => 13,  40 => 7,  37 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/total/order_discount.twig", "");
    }
}
