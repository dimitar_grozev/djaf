<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/common/header/header_04.twig */
class __TwigTemplate_a32e136413ef96c05bbfab17a2ee669cfdf11bc2e76143a89a567027ed6a9982 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
";
        // line 2
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "fixed_header"], "method", false, false, false, 2) == 1)) {
            echo " 
<!-- HEADER
\t================================================== -->
<div class=\"fixed-header-1 sticky-header header-type-3 header-type-4\">
\t<div class=\"background-header\"></div>
\t<div class=\"slider-header\">
\t\t<!-- Top of pages -->
\t\t<div id=\"top\" class=\"";
            // line 9
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_layout"], "method", false, false, false, 9) == 1)) {
                echo " ";
                echo "full-width";
                echo " ";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_layout"], "method", false, false, false, 9) == 4)) {
                echo " ";
                echo "fixed3 fixed2";
                echo " ";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_layout"], "method", false, false, false, 9) == 3)) {
                echo " ";
                echo "fixed2";
                echo " ";
            } else {
                echo " ";
                echo "fixed";
                echo " ";
            }
            echo "\">
\t\t\t<div class=\"background-top\"></div>
\t\t\t<div class=\"background\">
\t\t\t\t<div class=\"shadow\"></div>
\t\t\t\t<div class=\"pattern\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"row\">\t\t
\t\t\t\t\t\t     ";
            // line 16
            if (($context["logo"] ?? null)) {
                echo " 
\t\t\t\t\t\t\t\t";
                // line 17
                $context["nthumb"] = twig_replace_filter(($context["logo"] ?? null), [" " => "%20"]);
                // line 18
                echo "\t\t\t\t\t\t\t\t";
                $context["adres"] = twig_constant("HTTP_SERVER");
                // line 19
                echo "\t\t\t\t\t\t\t\t";
                $context["nthumb"] = twig_replace_filter(($context["nthumb"] ?? null), ["adres" => ""]);
                // line 20
                echo "\t\t\t\t\t\t\t\t";
                $context["image_size"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getimagesize", [0 => ($context["nthumb"] ?? null)], "method", false, false, false, 20);
                // line 21
                echo "\t\t\t\t\t\t\t     <!-- Header Left -->
\t\t\t\t\t\t\t     <div class=\"col-sm-4\" id=\"header-left\">
\t\t\t\t\t\t\t          <!-- Logo -->
\t\t\t\t\t\t\t          <div class=\"logo\"><a href=\"";
                // line 24
                echo ($context["home"] ?? null);
                echo "\"><img src=\"";
                echo ($context["logo"] ?? null);
                echo "\" title=\"";
                echo ($context["name"] ?? null);
                echo "\" alt=\"";
                echo ($context["name"] ?? null);
                echo "\" /></a></div>
\t\t\t\t\t\t\t     </div>
\t\t\t\t\t\t     ";
            }
            // line 26
            echo " 
\t\t\t\t\t\t     
\t\t\t\t\t\t\t<!-- Header Left -->
\t\t\t\t\t\t\t<div class=\"col-sm-4\" id=\"header-center\">
\t\t\t\t\t\t\t ";
            // line 30
            $context["menu9"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "menu"], "method", false, false, false, 30);
            // line 31
            echo "\t\t\t\t\t\t\t ";
            if ((twig_length_filter($this->env, ($context["menu9"] ?? null)) > 0)) {
                echo " 
                                 <div class=\"megamenu-background\">
                                      <div class=\"\">
                                           <div class=\"overflow-megamenu container\">
\t\t\t\t\t\t\t \t\t\t\t";
                // line 35
                if ((twig_length_filter($this->env, ($context["menu9"] ?? null)) > 1)) {
                    echo " ";
                    echo "<div class=\"row mega-menu-modules\">";
                }
                // line 36
                echo "\t\t\t\t\t\t\t \t\t\t\t";
                $context["i"] = 0;
                // line 37
                echo "\t\t\t\t\t\t\t \t\t\t\t 
\t\t\t\t\t\t\t \t\t\t\t";
                // line 38
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["menu9"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t\t\t\t \t\t\t\t ";
                    // line 39
                    if (((($context["i"] ?? null) == 0) && (twig_length_filter($this->env, ($context["menu9"] ?? null)) > 1))) {
                        echo " ";
                        echo "<div class=\"col-md-3\">";
                    }
                    // line 40
                    echo "\t\t\t\t\t\t\t \t\t\t\t ";
                    if (((($context["i"] ?? null) == 1) && (twig_length_filter($this->env, ($context["menu9"] ?? null)) > 1))) {
                        echo " ";
                        echo "<div class=\"col-md-9\">";
                    }
                    // line 41
                    echo "\t\t\t\t\t\t\t \t\t\t\t\t ";
                    echo $context["module"];
                    echo "
\t\t\t\t\t\t\t \t\t\t\t\t";
                    // line 42
                    if (((twig_length_filter($this->env, ($context["menu9"] ?? null)) > 1) && ((($context["i"] ?? null) == 0) || (($context["i"] ?? null) == 1)))) {
                        echo " ";
                        echo "</div>";
                    }
                    // line 43
                    echo "\t\t\t\t\t\t\t \t\t\t\t\t";
                    if (((twig_length_filter($this->env, ($context["menu9"] ?? null)) > 1) && (($context["i"] ?? null) == 1))) {
                        echo " ";
                        echo "</div>";
                    }
                    // line 44
                    echo "\t\t\t\t\t\t\t \t\t\t\t\t";
                    $context["i"] = (($context["i"] ?? null) + 1);
                    // line 45
                    echo "\t\t\t\t\t\t\t \t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " 
                            \t\t\t</div>
                            \t\t</div>
                            \t</div>
\t                         ";
            } else {
                // line 49
                echo " 
\t\t\t\t\t\t\t \t";
                // line 50
                echo ($context["menu"] ?? null);
                echo "
\t\t\t\t\t\t\t ";
            }
            // line 52
            echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Header Right -->
\t\t\t\t\t\t\t<div class=\"col-sm-4\" id=\"header-right\">
\t\t\t\t\t\t\t       <div class=\"currency-area\">";
            // line 56
            echo (($context["currency"] ?? null) . ($context["language"] ?? null));
            echo "</div>
\t\t\t\t\t\t\t ";
            // line 57
            $context["top_block"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "top_block"], "method", false, false, false, 57);
            // line 58
            echo "\t\t\t\t\t\t\t ";
            if ((twig_length_filter($this->env, ($context["top_block"] ?? null)) > 0)) {
                echo " 
\t\t\t\t\t\t\t \t";
                // line 59
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["top_block"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t\t\t\t \t\t";
                    // line 60
                    echo $context["module"];
                    echo "
\t\t\t\t\t\t\t \t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 62
                echo "\t\t\t\t\t\t\t ";
            }
            echo " 
\t\t\t\t\t\t\t     
\t\t\t\t\t\t\t     <!-- Search --><div class=\"search_form\"><i class=\"fa fa-search\"></i></div>
\t\t\t\t\t\t\t     \t";
            // line 65
            if (($context["logged"] ?? null)) {
                echo " 
\t\t\t\t\t\t\t     \t\t<div class=\"my-account-with-logout\">
\t\t\t\t\t\t\t     \t\t\t<a href=\"";
                // line 67
                echo ($context["account"] ?? null);
                echo "\" onclick=\"window.location.href = '";
                echo ($context["account"] ?? null);
                echo "'\" class=\"my-account\" data-hover=\"dropdown\" data-toggle=\"dropdown\"><i class=\"fa fa-user\"></i></a>
\t\t\t\t\t\t\t     \t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t     \t\t\t\t<li><a href=\"";
                // line 69
                echo ($context["account"] ?? null);
                echo "\">";
                echo ($context["text_account"] ?? null);
                echo "</a></li>
\t\t\t\t\t\t\t     \t\t\t\t<li><a href=\"";
                // line 70
                echo ($context["logout"] ?? null);
                echo "\">";
                echo ($context["text_logout"] ?? null);
                echo "</a></li>
\t\t\t\t\t\t\t     \t\t\t</ul>
\t\t\t\t\t\t\t     \t\t</div>
\t\t\t\t\t\t\t     \t";
            } else {
                // line 73
                echo " 
\t\t\t\t\t\t\t     \t\t   <div href=\"";
                // line 74
                echo ($context["account"] ?? null);
                echo "\" class=\"my-account\"><i class=\"fa fa-user\"></i>
\t\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                // line 76
                echo ($context["account"] ?? null);
                echo "\">";
                echo ($context["text_account"] ?? null);
                echo "</a></li>
\t\t\t\t               \t<li><a href=\"";
                // line 77
                echo ($context["wishlist"] ?? null);
                echo "\" id=\"wishlist-total\">";
                echo ($context["text_wishlist"] ?? null);
                echo "</a></li>
\t\t\t\t              
\t\t\t\t               \t<li><a href=\"";
                // line 79
                echo ($context["checkout"] ?? null);
                echo "\">";
                echo ($context["text_checkout"] ?? null);
                echo "</a></li>
\t\t\t\t               \t\t";
                // line 80
                if (($context["logged"] ?? null)) {
                    echo " 
\t\t\t\t               \t\t<li><a href=\"";
                    // line 81
                    echo ($context["logout"] ?? null);
                    echo "\">";
                    echo ($context["text_logout"] ?? null);
                    echo "</a></li>
\t\t\t\t               \t\t";
                }
                // line 82
                echo " 
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t     \t";
            }
            // line 87
            echo " 
\t\t\t\t\t\t\t\t";
            // line 88
            echo ($context["cart"] ?? null);
            echo " 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t";
            // line 94
            $context["menu92"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "menu2"], "method", false, false, false, 94);
            // line 95
            echo "\t\t\t\t\t";
            if ((twig_length_filter($this->env, ($context["menu92"] ?? null)) > 0)) {
                echo " 
\t\t\t\t\t ";
                // line 96
                echo "<div class=\"overflow-menu2\">";
                echo "
\t\t\t\t\t\t";
                // line 97
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["menu92"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t\t\t\t";
                    // line 98
                    echo $context["module"];
                    echo "
\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 100
                echo "\t\t\t\t\t\t";
                echo "</div>";
                echo "
\t\t\t\t\t";
            }
            // line 101
            echo " 
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
        }
        // line 107
        echo " 



<!-- HEADER
\t================================================== -->
<header class=\"header-type-3 header-type-4\">
\t<div class=\"background-header\"></div>
\t<div class=\"slider-header\">
\t\t<!-- Top of pages -->
\t\t<div id=\"top\" class=\"";
        // line 117
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_layout"], "method", false, false, false, 117) == 1)) {
            echo " ";
            echo "full-width";
            echo " ";
        } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_layout"], "method", false, false, false, 117) == 4)) {
            echo " ";
            echo "fixed3 fixed2";
            echo " ";
        } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_layout"], "method", false, false, false, 117) == 3)) {
            echo " ";
            echo "fixed2";
            echo " ";
        } else {
            echo " ";
            echo "fixed";
            echo " ";
        }
        echo "\">
\t\t\t<div class=\"background-top\"></div>
\t\t\t<div class=\"background\">
\t\t\t\t<div class=\"shadow\"></div>
\t\t\t\t<div class=\"pattern\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"row\">\t\t
\t\t\t\t\t\t     ";
        // line 124
        if (($context["logo"] ?? null)) {
            echo " 
\t\t\t\t\t\t\t\t";
            // line 125
            $context["nthumb"] = twig_replace_filter(($context["logo"] ?? null), [" " => "%20"]);
            // line 126
            echo "\t\t\t\t\t\t\t\t";
            $context["adres"] = twig_constant("HTTP_SERVER");
            // line 127
            echo "\t\t\t\t\t\t\t\t";
            $context["nthumb"] = twig_replace_filter(($context["nthumb"] ?? null), ["adres" => ""]);
            // line 128
            echo "\t\t\t\t\t\t\t\t";
            $context["image_size"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getimagesize", [0 => ($context["nthumb"] ?? null)], "method", false, false, false, 128);
            // line 129
            echo "\t\t\t\t\t\t\t     <!-- Header Left -->
\t\t\t\t\t\t\t     <div class=\"col-sm-4\" id=\"header-left\" >
\t\t\t\t\t\t\t          <!-- Logo -->
\t\t\t\t\t\t\t          <div class=\"logo\"><a href=\"";
            // line 132
            echo ($context["home"] ?? null);
            echo "\"><img src=\"";
            echo ($context["logo"] ?? null);
            echo "\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\" /></a></div>
\t\t\t\t\t\t\t     </div>
\t\t\t\t\t\t     ";
        }
        // line 134
        echo " 
\t\t\t\t\t\t     
\t\t\t\t\t\t\t<!-- Header Left -->
\t\t\t\t\t\t\t<div class=\"col-sm-4\" id=\"header-center\">
\t\t\t\t\t\t\t\t";
        // line 138
        $context["menu9"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "menu"], "method", false, false, false, 138);
        // line 139
        echo "\t\t\t\t\t\t\t\t";
        if ((twig_length_filter($this->env, ($context["menu9"] ?? null)) > 0)) {
            echo " 
\t                                 <div class=\"megamenu-background\">
\t                                      <div class=\"\">
\t                                           <div class=\"overflow-megamenu container\">
\t\t\t\t\t\t\t\t\t \t\t\t\t";
            // line 143
            if ((twig_length_filter($this->env, ($context["menu9"] ?? null)) > 1)) {
                echo " ";
                echo "<div class=\"row mega-menu-modules\">";
            }
            // line 144
            echo "\t\t\t\t\t\t\t\t\t \t\t\t\t";
            $context["i"] = 0;
            // line 145
            echo "\t\t\t\t\t\t\t\t\t \t\t\t\t 
\t\t\t\t\t\t\t\t\t \t\t\t\t";
            // line 146
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["menu9"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                echo " 
\t\t\t\t\t\t\t\t\t \t\t\t\t ";
                // line 147
                if (((($context["i"] ?? null) == 0) && (twig_length_filter($this->env, ($context["menu9"] ?? null)) > 1))) {
                    echo " ";
                    echo "<div class=\"col-md-3\">";
                }
                // line 148
                echo "\t\t\t\t\t\t\t\t\t \t\t\t\t ";
                if (((($context["i"] ?? null) == 1) && (twig_length_filter($this->env, ($context["menu9"] ?? null)) > 1))) {
                    echo " ";
                    echo "<div class=\"col-md-9\">";
                }
                // line 149
                echo "\t\t\t\t\t\t\t\t\t \t\t\t\t\t ";
                echo $context["module"];
                echo "
\t\t\t\t\t\t\t\t\t \t\t\t\t\t";
                // line 150
                if (((twig_length_filter($this->env, ($context["menu9"] ?? null)) > 1) && ((($context["i"] ?? null) == 0) || (($context["i"] ?? null) == 1)))) {
                    echo " ";
                    echo "</div>";
                }
                // line 151
                echo "\t\t\t\t\t\t\t\t\t \t\t\t\t\t";
                if (((twig_length_filter($this->env, ($context["menu9"] ?? null)) > 1) && (($context["i"] ?? null) == 1))) {
                    echo " ";
                    echo "</div>";
                }
                // line 152
                echo "\t\t\t\t\t\t\t\t\t \t\t\t\t\t";
                $context["i"] = (($context["i"] ?? null) + 1);
                // line 153
                echo "\t\t\t\t\t\t\t\t\t \t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
                                \t\t\t</div>
                                \t\t</div>
                                \t</div>
                                ";
        } else {
            // line 157
            echo " 
\t\t\t\t\t\t\t\t\t";
            // line 158
            echo ($context["menu"] ?? null);
            echo "
\t\t\t\t\t\t\t\t";
        }
        // line 160
        echo " 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Header Right -->
\t\t\t\t\t\t\t<div class=\"col-sm-4\" id=\"header-right\">
\t\t\t\t\t\t\t     <div class=\"currency-area\">";
        // line 165
        echo (($context["currency"] ?? null) . ($context["language"] ?? null));
        echo "</div>
\t\t\t\t\t\t\t ";
        // line 166
        $context["top_block"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "top_block"], "method", false, false, false, 166);
        // line 167
        echo "\t\t\t\t\t\t\t ";
        if ((twig_length_filter($this->env, ($context["top_block"] ?? null)) > 0)) {
            echo " 
\t\t\t\t\t\t\t \t";
            // line 168
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["top_block"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                echo " 
\t\t\t\t\t\t\t \t\t";
                // line 169
                echo $context["module"];
                echo "
\t\t\t\t\t\t\t \t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 171
            echo "\t\t\t\t\t\t\t ";
        }
        echo " 
\t\t\t\t\t\t\t     
\t\t\t\t\t\t\t     <!-- Search --><div class=\"search_form\"><i class=\"fa fa-search\"></i></div>
\t\t\t\t\t\t\t     \t";
        // line 174
        if (($context["logged"] ?? null)) {
            echo " 
\t\t\t\t\t\t\t     \t\t<div class=\"my-account-with-logout\">
\t\t\t\t\t\t\t     \t\t\t<a href=\"";
            // line 176
            echo ($context["account"] ?? null);
            echo "\" onclick=\"window.location.href = '";
            echo ($context["account"] ?? null);
            echo "'\" class=\"my-account\" data-hover=\"dropdown\" data-toggle=\"dropdown\"><i class=\"fa fa-user\"></i></a>
\t\t\t\t\t\t\t     \t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t     \t\t\t\t<li><a href=\"";
            // line 178
            echo ($context["account"] ?? null);
            echo "\">";
            echo ($context["text_account"] ?? null);
            echo "</a></li>
\t\t\t\t\t\t\t     \t\t\t\t<li><a href=\"";
            // line 179
            echo ($context["logout"] ?? null);
            echo "\">";
            echo ($context["text_logout"] ?? null);
            echo "</a></li>
\t\t\t\t\t\t\t     \t\t\t</ul>
\t\t\t\t\t\t\t     \t\t</div>
\t\t\t\t\t\t\t     \t";
        } else {
            // line 182
            echo " 
\t\t\t\t\t\t\t     \t\t<div href=\"";
            // line 183
            echo ($context["account"] ?? null);
            echo "\" class=\"my-account\">
\t\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
            // line 185
            echo ($context["account"] ?? null);
            echo "\">";
            echo ($context["text_account"] ?? null);
            echo "</a></li>
\t\t\t\t               \t<li><a href=\"";
            // line 186
            echo ($context["wishlist"] ?? null);
            echo "\" id=\"wishlist-total\">";
            echo ($context["text_wishlist"] ?? null);
            echo "</a></li>
\t\t\t\t              
\t\t\t\t               \t<li><a href=\"";
            // line 188
            echo ($context["checkout"] ?? null);
            echo "\">";
            echo ($context["text_checkout"] ?? null);
            echo "</a></li>
\t\t\t\t               \t\t";
            // line 189
            if (($context["logged"] ?? null)) {
                echo " 
\t\t\t\t               \t\t<li><a href=\"";
                // line 190
                echo ($context["logout"] ?? null);
                echo "\">";
                echo ($context["text_logout"] ?? null);
                echo "</a></li>
\t\t\t\t               \t\t";
            }
            // line 191
            echo " 
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t     \t";
        }
        // line 194
        echo " 
\t\t\t\t\t\t\t\t";
        // line 195
        echo ($context["cart"] ?? null);
        echo " 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t";
        // line 201
        $context["menu92"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "menu2"], "method", false, false, false, 201);
        // line 202
        echo "\t\t\t\t\t";
        if ((twig_length_filter($this->env, ($context["menu92"] ?? null)) > 0)) {
            echo " 
\t\t\t\t\t ";
            // line 203
            echo "<div class=\"overflow-menu2\">";
            echo "
\t\t\t\t\t\t";
            // line 204
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["menu92"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                echo " 
\t\t\t\t\t\t\t";
                // line 205
                echo $context["module"];
                echo "
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 207
            echo "\t\t\t\t\t\t";
            echo "</div>";
            echo "
\t\t\t\t\t";
        }
        // line 208
        echo " 
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t
\t";
        // line 214
        $context["slideshow"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "slideshow"], "method", false, false, false, 214);
        echo " 
\t ";
        // line 215
        if ((twig_length_filter($this->env, ($context["slideshow"] ?? null)) > 0)) {
            echo " 
\t<!-- Slider -->
\t<div id=\"slider\" class=\"";
            // line 217
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "slideshow_layout"], "method", false, false, false, 217) == 1)) {
                echo " ";
                echo "full-width";
                echo " ";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "slideshow_layout"], "method", false, false, false, 217) == 4)) {
                echo " ";
                echo "fixed3 fixed2";
                echo " ";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "slideshow_layout"], "method", false, false, false, 217) == 3)) {
                echo " ";
                echo "fixed2";
                echo " ";
            } else {
                echo " ";
                echo "fixed";
                echo " ";
            }
            echo "\">
\t\t<div class=\"background-slider\"></div>
\t\t<div class=\"background\">
\t\t\t<div class=\"shadow\"></div>
\t\t\t<div class=\"pattern\">
\t\t\t\t";
            // line 222
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["slideshow"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                echo " 
\t\t\t\t";
                // line 223
                echo $context["module"];
                echo " 
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 224
            echo " 
\t\t\t</div>
\t\t</div>
\t</div>
\t";
        }
        // line 228
        echo " 
</header>";
    }

    public function getTemplateName()
    {
        return "kofi/template/common/header/header_04.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  722 => 228,  715 => 224,  707 => 223,  701 => 222,  677 => 217,  672 => 215,  668 => 214,  660 => 208,  654 => 207,  646 => 205,  640 => 204,  636 => 203,  631 => 202,  629 => 201,  620 => 195,  617 => 194,  611 => 191,  604 => 190,  600 => 189,  594 => 188,  587 => 186,  581 => 185,  576 => 183,  573 => 182,  564 => 179,  558 => 178,  551 => 176,  546 => 174,  539 => 171,  531 => 169,  525 => 168,  520 => 167,  518 => 166,  514 => 165,  507 => 160,  502 => 158,  499 => 157,  487 => 153,  484 => 152,  478 => 151,  473 => 150,  468 => 149,  462 => 148,  457 => 147,  451 => 146,  448 => 145,  445 => 144,  440 => 143,  432 => 139,  430 => 138,  424 => 134,  412 => 132,  407 => 129,  404 => 128,  401 => 127,  398 => 126,  396 => 125,  392 => 124,  366 => 117,  354 => 107,  345 => 101,  339 => 100,  331 => 98,  325 => 97,  321 => 96,  316 => 95,  314 => 94,  305 => 88,  302 => 87,  294 => 82,  287 => 81,  283 => 80,  277 => 79,  270 => 77,  264 => 76,  259 => 74,  256 => 73,  247 => 70,  241 => 69,  234 => 67,  229 => 65,  222 => 62,  214 => 60,  208 => 59,  203 => 58,  201 => 57,  197 => 56,  191 => 52,  186 => 50,  183 => 49,  171 => 45,  168 => 44,  162 => 43,  157 => 42,  152 => 41,  146 => 40,  141 => 39,  135 => 38,  132 => 37,  129 => 36,  124 => 35,  116 => 31,  114 => 30,  108 => 26,  96 => 24,  91 => 21,  88 => 20,  85 => 19,  82 => 18,  80 => 17,  76 => 16,  50 => 9,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/common/header/header_04.twig", "/home/prod85fb/public_html/catalog/view/theme/kofi/template/common/header/header_04.twig");
    }
}
