<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/new_elements/wrapper_top.twig */
class __TwigTemplate_a472e3bb2306141af1d0ed4de7de412e40c9729edb9bd6853f80dde67932e74c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
\t";
            // line 2
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
            // line 3
            echo "\t";
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
            // line 4
            echo "
\t<!-- BREADCRUMB
\t\t================================================== -->
\t<div class=\"breadcrumb ";
            // line 7
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "breadcrumb_layout"], "method", false, false, false, 7) == 1)) {
                echo " ";
                echo "full-width";
                echo " ";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "breadcrumb_layout"], "method", false, false, false, 7) == 4)) {
                echo " ";
                echo "fixed3 fixed2";
                echo " ";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "breadcrumb_layout"], "method", false, false, false, 7) == 3)) {
                echo " ";
                echo "fixed2";
                echo " ";
            } else {
                echo " ";
                echo "fixed";
                echo " ";
            }
            echo "\">
\t\t<div class=\"background-breadcrumb\"></div>
\t\t<div class=\"background\" ";
            // line 9
            $context["breadcrumb"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "breadcrumb"], "method", false, false, false, 9);
            echo " ";
            if ((twig_length_filter($this->env, ($context["breadcrumb"] ?? null)) > 0)) {
                echo " ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["breadcrumb"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " ";
                    echo $context["module"];
                    echo " ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " ";
            }
            echo ">
\t\t\t<div class=\"shadow\"></div>
\t\t\t<div class=\"pattern\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"clearfix\">
\t\t\t\t\t     ";
            // line 14
            if ((array_key_exists("product_page", $context) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_breadcrumb"], "method", false, false, false, 14) != "2"))) {
                echo " 
\t\t\t\t\t     ";
                // line 15
                $context["product_prev_next"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getNextPrevProduct", [0 => ($context["product_id"] ?? null)], "method", false, false, false, 15);
                echo " 
\t\t\t\t\t          <div class=\"row\">
\t\t\t\t\t               <div class=\"col-md-3 hidden-xs hidden-sm\">
\t\t\t\t\t                    ";
                // line 18
                if (twig_test_iterable((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["product_prev_next"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["prev"] ?? null) : null))) {
                    echo " 
\t\t\t\t\t                         ";
                    // line 19
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_breadcrumb"], "method", false, false, false, 19) == "1")) {
                        echo " 
\t     \t\t\t\t                    <div class=\"next-product-2 clearfix\">
\t     \t\t\t\t                         <a href=\"";
                        // line 21
                        echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["product_prev_next"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["prev"] ?? null) : null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["href"] ?? null) : null);
                        echo "\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                        echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["product_prev_next"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["prev"] ?? null) : null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["name"] ?? null) : null);
                        echo "\" class=\"button-previous-next\">";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "previous_product_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 21)], "method", false, false, false, 21) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "previous_product_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 21)], "method", false, false, false, 21);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Previous product";
                            echo " ";
                        }
                        echo "</a>
\t     \t\t\t\t                    </div>
\t     \t\t\t\t                    ";
                    } else {
                        // line 23
                        echo " 
\t     \t\t\t\t                    <div class=\"next-product clearfix\">
\t     \t\t\t\t                         <div class=\"image\"><a href=\"";
                        // line 25
                        echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["product_prev_next"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["prev"] ?? null) : null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["href"] ?? null) : null);
                        echo "\"><img src=\"";
                        echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["product_prev_next"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["prev"] ?? null) : null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["thumb"] ?? null) : null);
                        echo "\" alt=\"";
                        echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["product_prev_next"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["prev"] ?? null) : null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["name"] ?? null) : null);
                        echo "\"></a></div>
\t     \t\t\t\t                         <div class=\"right\">
\t     \t\t\t\t                              <div class=\"name\"><a href=\"";
                        // line 27
                        echo (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = ($context["product_prev_next"] ?? null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["prev"] ?? null) : null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["href"] ?? null) : null);
                        echo "\">";
                        echo (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = ($context["product_prev_next"] ?? null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["prev"] ?? null) : null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["name"] ?? null) : null);
                        echo "</a></div>
\t     \t\t\t\t                              <div class=\"price\">";
                        // line 28
                        if ((($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = ($context["product_prev_next"] ?? null)) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["prev"] ?? null) : null)) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["special"] ?? null) : null)) {
                            echo " ";
                            echo (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = ($context["product_prev_next"] ?? null)) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["prev"] ?? null) : null)) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["special"] ?? null) : null);
                            echo " ";
                        } else {
                            echo " ";
                            echo (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = ($context["product_prev_next"] ?? null)) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["prev"] ?? null) : null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["price"] ?? null) : null);
                            echo " ";
                        }
                        echo "</div>
\t     \t\t\t\t                         </div>
\t     \t\t\t\t                    </div>
\t     \t\t\t\t                    ";
                    }
                    // line 31
                    echo " 
\t\t\t\t\t                    ";
                }
                // line 32
                echo " 
\t\t\t\t\t               </div>
\t\t\t\t\t               
\t\t\t\t\t               <div class=\"col-md-6\">
\t\t\t\t\t               \t\t";
                // line 36
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
                    echo " 
\t\t\t\t\t               \t\t\t";
                    // line 37
                    $context["heading_title"] = (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["breadcrumb"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["text"] ?? null) : null);
                    echo " 
\t\t\t\t\t               \t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 38
                echo " 
\t\t\t\t\t               \t\t
\t\t\t\t\t                    <h1 id=\"title-page\">";
                // line 40
                echo ($context["heading_title"] ?? null);
                echo " 
\t\t\t\t\t                    \t";
                // line 41
                if (array_key_exists("weight", $context)) {
                    echo " ";
                    if (($context["weight"] ?? null)) {
                        echo " 
\t\t\t\t\t                    \t&nbsp;(";
                        // line 42
                        echo ($context["weight"] ?? null);
                        echo ")
\t\t\t\t\t                    \t";
                    }
                    // line 43
                    echo " ";
                }
                echo " 
\t\t\t\t\t                    </h1>
\t\t\t\t\t                    
\t\t\t\t\t                    <ul>
\t\t\t\t\t                    \t";
                // line 47
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
                    echo " 
\t\t\t\t\t                    \t<li><a href=\"";
                    // line 48
                    echo (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = $context["breadcrumb"]) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["href"] ?? null) : null);
                    echo "\">";
                    if (((($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = $context["breadcrumb"]) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["text"] ?? null) : null) != "<i class=\"fa fa-home\"></i>")) {
                        echo " ";
                        echo (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = $context["breadcrumb"]) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["text"] ?? null) : null);
                        echo " ";
                    } else {
                        echo " ";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "home_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 48)], "method", false, false, false, 48) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "home_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 48)], "method", false, false, false, 48);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Home";
                            echo " ";
                        }
                        echo " ";
                    }
                    echo "</a></li>
\t\t\t\t\t                    \t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 49
                echo " 
\t\t\t\t\t                    </ul>
\t\t\t\t\t               </div>
\t\t\t\t\t               
\t     \t\t\t\t\t     <div class=\"col-md-3 hidden-xs hidden-sm\">
\t     \t\t\t\t\t          ";
                // line 54
                if (twig_test_iterable((($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = ($context["product_prev_next"] ?? null)) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["next"] ?? null) : null))) {
                    echo " 
\t          \t\t\t\t\t          ";
                    // line 55
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_breadcrumb"], "method", false, false, false, 55) == "1")) {
                        echo " 
\t          \t\t\t\t\t          <div class=\"next-product-2 right clearfix\">
\t          \t\t\t\t\t               <a href=\"";
                        // line 57
                        echo (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = ($context["product_prev_next"] ?? null)) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["next"] ?? null) : null)) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["href"] ?? null) : null);
                        echo "\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                        echo (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = ($context["product_prev_next"] ?? null)) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["next"] ?? null) : null)) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["name"] ?? null) : null);
                        echo "\" class=\"button-previous-next\">";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "next_product_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 57)], "method", false, false, false, 57) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "next_product_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 57)], "method", false, false, false, 57);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Next product";
                            echo " ";
                        }
                        echo "</a>
\t          \t\t\t\t\t          </div>
\t          \t\t\t\t\t          ";
                    } else {
                        // line 59
                        echo " 
\t          \t\t\t\t\t          <div class=\"next-product right clearfix\">
\t          \t\t\t\t\t               <div class=\"image\"><a href=\"";
                        // line 61
                        echo (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = ($context["product_prev_next"] ?? null)) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["next"] ?? null) : null)) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["href"] ?? null) : null);
                        echo "\"><img src=\"";
                        echo (($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = (($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = ($context["product_prev_next"] ?? null)) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["next"] ?? null) : null)) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["thumb"] ?? null) : null);
                        echo "\" alt=\"";
                        echo (($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = (($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = ($context["product_prev_next"] ?? null)) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["next"] ?? null) : null)) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["name"] ?? null) : null);
                        echo "\"></a></div>
\t          \t\t\t\t\t               <div class=\"right\">
\t          \t\t\t\t\t                    <div class=\"name\"><a href=\"";
                        // line 63
                        echo (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = (($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = ($context["product_prev_next"] ?? null)) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["next"] ?? null) : null)) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["href"] ?? null) : null);
                        echo "\">";
                        echo (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = ($context["product_prev_next"] ?? null)) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["next"] ?? null) : null)) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["name"] ?? null) : null);
                        echo "</a></div>
\t          \t\t\t\t\t                    <div class=\"price\">";
                        // line 64
                        if ((($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = (($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = ($context["product_prev_next"] ?? null)) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["next"] ?? null) : null)) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["special"] ?? null) : null)) {
                            echo " ";
                            echo (($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = (($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = ($context["product_prev_next"] ?? null)) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54["next"] ?? null) : null)) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["special"] ?? null) : null);
                            echo " ";
                        } else {
                            echo " ";
                            echo (($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = (($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 = ($context["product_prev_next"] ?? null)) && is_array($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327) || $__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 instanceof ArrayAccess ? ($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327["next"] ?? null) : null)) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f["price"] ?? null) : null);
                            echo " ";
                        }
                        echo "</div>
\t          \t\t\t\t\t               </div>
\t          \t\t\t\t\t          </div>
\t          \t\t\t\t\t          ";
                    }
                    // line 67
                    echo " 
\t     \t\t\t\t\t          ";
                }
                // line 68
                echo " 
\t     \t\t\t\t\t     </div>
\t     \t\t\t\t\t</div>
\t\t\t\t\t\t";
            } else {
                // line 71
                echo " 
\t\t\t\t\t\t\t ";
                // line 72
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
                    echo " 
\t\t\t\t\t\t\t \t";
                    // line 73
                    $context["heading_title"] = (($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 = $context["breadcrumb"]) && is_array($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412) || $__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 instanceof ArrayAccess ? ($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412["text"] ?? null) : null);
                    echo " 
\t\t\t\t\t\t\t ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 74
                echo " 
\t\t\t\t\t\t\t 
\t\t\t\t\t\t     <h1 id=\"title-page\">";
                // line 76
                echo ($context["heading_title"] ?? null);
                echo " 
\t\t\t\t\t\t     \t";
                // line 77
                if (array_key_exists("weight", $context)) {
                    echo " ";
                    if (($context["weight"] ?? null)) {
                        echo " 
\t\t\t\t\t\t     \t&nbsp;(";
                        // line 78
                        echo ($context["weight"] ?? null);
                        echo ")
\t\t\t\t\t\t     \t";
                    }
                    // line 79
                    echo " ";
                }
                echo " 
\t\t\t\t\t\t     </h1>
\t\t\t\t\t\t     
\t\t\t\t\t\t     <ul>
\t\t\t\t\t\t     \t";
                // line 83
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
                    echo " 
\t\t\t\t\t\t     \t<li><a href=\"";
                    // line 84
                    echo (($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 = $context["breadcrumb"]) && is_array($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9) || $__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 instanceof ArrayAccess ? ($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9["href"] ?? null) : null);
                    echo "\">";
                    if (((($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e = $context["breadcrumb"]) && is_array($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e) || $__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e instanceof ArrayAccess ? ($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e["text"] ?? null) : null) != "<i class=\"fa fa-home\"></i>")) {
                        echo " ";
                        echo (($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 = $context["breadcrumb"]) && is_array($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5) || $__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 instanceof ArrayAccess ? ($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5["text"] ?? null) : null);
                        echo " ";
                    } else {
                        echo " ";
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "home_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 84)], "method", false, false, false, 84) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "home_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 84)], "method", false, false, false, 84);
                            echo " ";
                        } else {
                            echo " ";
                            echo "Home";
                            echo " ";
                        }
                        echo " ";
                    }
                    echo "</a></li>
\t\t\t\t\t\t     \t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 85
                echo " 
\t\t\t\t\t\t     </ul> 
\t\t\t\t\t\t";
            }
            // line 87
            echo " 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<!-- MAIN CONTENT
\t\t================================================== -->
\t<div class=\"main-content ";
            // line 96
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "content_layout"], "method", false, false, false, 96) == 1)) {
                echo " ";
                echo "full-width";
                echo " ";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "content_layout"], "method", false, false, false, 96) == 4)) {
                echo " ";
                echo "fixed3 fixed2";
                echo " ";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "content_layout"], "method", false, false, false, 96) == 3)) {
                echo " ";
                echo "fixed2";
                echo " ";
            } else {
                echo " ";
                echo "fixed";
                echo " ";
            }
            echo " inner-page\">
\t\t<div class=\"background-content\"></div>
\t\t<div class=\"background\">
\t\t\t<div class=\"shadow\"></div>
\t\t\t<div class=\"pattern\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t
\t\t\t\t\t";
            // line 103
            $context["preface_left"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_left"], "method", false, false, false, 103);
            // line 104
            echo "\t\t\t\t\t";
            $context["preface_right"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_right"], "method", false, false, false, 104);
            // line 105
            echo "\t\t\t\t\t
\t\t\t\t\t";
            // line 106
            if (((twig_length_filter($this->env, ($context["preface_left"] ?? null)) > 0) || (twig_length_filter($this->env, ($context["preface_right"] ?? null)) > 0))) {
                echo " 
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 109
                if ((twig_length_filter($this->env, ($context["preface_left"] ?? null)) > 0)) {
                    // line 110
                    echo "\t\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["preface_left"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t\t\t\t\t";
                        // line 111
                        echo $context["module"];
                        echo "
\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 113
                    echo "\t\t\t\t\t\t\t";
                }
                echo " 
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 117
                if ((twig_length_filter($this->env, ($context["preface_right"] ?? null)) > 0)) {
                    // line 118
                    echo "\t\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["preface_right"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t\t\t\t\t";
                        // line 119
                        echo $context["module"];
                        echo "
\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 121
                    echo "\t\t\t\t\t\t\t";
                }
                echo " 
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t";
            }
            // line 124
            echo " 
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t";
            // line 127
            $context["preface_fullwidth"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_fullwidth"], "method", false, false, false, 127);
            // line 128
            echo "\t\t\t\t\t";
            if ((twig_length_filter($this->env, ($context["preface_fullwidth"] ?? null)) > 0)) {
                // line 129
                echo "\t\t\t\t\t\t";
                echo "<div class=\"row\"><div class=\"col-sm-12\">";
                echo "
\t\t\t\t\t\t";
                // line 130
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["preface_fullwidth"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t\t\t\t";
                    // line 131
                    echo $context["module"];
                    echo "
\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 133
                echo "\t\t\t\t\t\t";
                echo "</div></div>";
                echo "
\t\t\t\t\t";
            }
            // line 134
            echo " 
\t\t\t\t\t
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t";
            // line 137
            $context["columnleft"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_left"], "method", false, false, false, 137);
            // line 138
            echo "\t\t\t\t\t\t";
            if ((twig_length_filter($this->env, ($context["columnleft"] ?? null)) > 0)) {
                echo " 
\t\t\t\t\t\t<div class=\"col-md-3\" id=\"column-left\">
\t\t\t\t\t\t\t";
                // line 140
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["columnleft"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t\t\t\t\t";
                    // line 141
                    echo $context["module"];
                    echo "
\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 143
                echo "\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            // line 144
            echo " 
\t\t\t\t\t\t
\t\t\t\t\t\t";
            // line 146
            $context["grid_center"] = 12;
            echo " ";
            if ((twig_length_filter($this->env, ($context["columnleft"] ?? null)) > 0)) {
                echo " ";
                $context["grid_center"] = 9;
                echo " ";
            }
            echo " 
\t\t\t\t\t\t<div class=\"col-md-";
            // line 147
            echo ($context["grid_center"] ?? null);
            echo "\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
            // line 149
            $context["content_big_column"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_big_column"], "method", false, false, false, 149);
            // line 150
            echo "\t\t\t\t\t\t\t";
            if ((twig_length_filter($this->env, ($context["content_big_column"] ?? null)) > 0)) {
                echo " 
\t\t\t\t\t\t\t\t";
                // line 151
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["content_big_column"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 152
                    echo $context["module"];
                    echo "
\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 154
                echo "\t\t\t\t\t\t\t";
            }
            echo " 
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
            // line 157
            $context["content_top"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_top"], "method", false, false, false, 157);
            // line 158
            echo "\t\t\t\t\t\t\t";
            if ((twig_length_filter($this->env, ($context["content_top"] ?? null)) > 0)) {
                echo " 
\t\t\t\t\t\t\t\t";
                // line 159
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["content_top"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 160
                    echo $context["module"];
                    echo "
\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 162
                echo "\t\t\t\t\t\t\t";
            }
            echo " 
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
            // line 166
            $context["grid_content_top"] = 12;
            echo " 
\t\t\t\t\t\t\t\t";
            // line 167
            $context["grid_content_right"] = 3;
            // line 168
            echo "\t\t\t\t\t\t\t\t";
            $context["column_right"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_right"], "method", false, false, false, 168);
            echo " 
\t\t\t\t\t\t\t\t";
            // line 169
            if ((twig_length_filter($this->env, ($context["column_right"] ?? null)) > 0)) {
                // line 170
                echo "\t\t\t\t\t\t\t\t\t";
                if ((($context["grid_center"] ?? null) == 9)) {
                    // line 171
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    $context["grid_content_top"] = 8;
                    // line 172
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    $context["grid_content_right"] = 4;
                    // line 173
                    echo "\t\t\t\t\t\t\t\t\t";
                } else {
                    echo " 
\t\t\t\t\t\t\t\t\t\t";
                    // line 174
                    $context["grid_content_top"] = 9;
                    // line 175
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    $context["grid_content_right"] = 3;
                    // line 176
                    echo "\t\t\t\t\t\t\t\t\t";
                }
                // line 177
                echo "\t\t\t\t\t\t\t\t";
            }
            // line 178
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"col-md-";
            // line 179
            echo ($context["grid_content_top"] ?? null);
            echo " center-column ";
            if (array_key_exists("background_status", $context)) {
                echo " ";
                echo "content-without-background";
                echo " ";
            } else {
                echo " ";
                echo "content-with-background";
                echo " ";
            }
            echo "\" id=\"content\">

\t\t\t\t\t\t\t\t\t";
            // line 181
            if ((array_key_exists("error_warning", $context) &&  !array_key_exists("checkoutPage", $context))) {
                echo " 
\t\t\t\t\t\t\t\t\t\t";
                // line 182
                if (($context["error_warning"] ?? null)) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t<div class=\"warning\">
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 185
                    echo ($context["error_warning"] ?? null);
                    echo " 
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 187
                echo " 
\t\t\t\t\t\t\t\t\t";
            }
            // line 188
            echo " 
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
            // line 190
            if (array_key_exists("success", $context)) {
                echo " 
\t\t\t\t\t\t\t\t\t\t";
                // line 191
                if (($context["success"] ?? null)) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t<div class=\"success\">
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 194
                    echo ($context["success"] ?? null);
                    echo " 
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 196
                echo " 
\t\t\t\t\t\t\t\t\t";
            }
        }
    }

    public function getTemplateName()
    {
        return "kofi/template/new_elements/wrapper_top.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  747 => 196,  741 => 194,  735 => 191,  731 => 190,  727 => 188,  723 => 187,  717 => 185,  711 => 182,  707 => 181,  692 => 179,  689 => 178,  686 => 177,  683 => 176,  680 => 175,  678 => 174,  673 => 173,  670 => 172,  667 => 171,  664 => 170,  662 => 169,  657 => 168,  655 => 167,  651 => 166,  643 => 162,  635 => 160,  629 => 159,  624 => 158,  622 => 157,  615 => 154,  607 => 152,  601 => 151,  596 => 150,  594 => 149,  589 => 147,  579 => 146,  575 => 144,  571 => 143,  563 => 141,  557 => 140,  551 => 138,  549 => 137,  544 => 134,  538 => 133,  530 => 131,  524 => 130,  519 => 129,  516 => 128,  514 => 127,  509 => 124,  501 => 121,  493 => 119,  486 => 118,  484 => 117,  476 => 113,  468 => 111,  461 => 110,  459 => 109,  453 => 106,  450 => 105,  447 => 104,  445 => 103,  419 => 96,  408 => 87,  403 => 85,  377 => 84,  371 => 83,  363 => 79,  358 => 78,  352 => 77,  348 => 76,  344 => 74,  336 => 73,  330 => 72,  327 => 71,  321 => 68,  317 => 67,  302 => 64,  296 => 63,  287 => 61,  283 => 59,  265 => 57,  260 => 55,  256 => 54,  249 => 49,  223 => 48,  217 => 47,  209 => 43,  204 => 42,  198 => 41,  194 => 40,  190 => 38,  182 => 37,  176 => 36,  170 => 32,  166 => 31,  151 => 28,  145 => 27,  136 => 25,  132 => 23,  114 => 21,  109 => 19,  105 => 18,  99 => 15,  95 => 14,  72 => 9,  51 => 7,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/new_elements/wrapper_top.twig", "/home/prod85fb/public_html/catalog/view/theme/kofi/template/new_elements/wrapper_top.twig");
    }
}
