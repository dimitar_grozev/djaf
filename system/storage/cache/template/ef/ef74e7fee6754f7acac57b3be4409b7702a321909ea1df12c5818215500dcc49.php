<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* common/footer.twig */
class __TwigTemplate_87774761be16180bb59e36f490e736825b5e2def025f6eb115dbd656e8093930 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<footer id=\"footer\">";
        echo ($context["text_footer"] ?? null);
        echo "<br />";
        echo ($context["text_version"] ?? null);
        echo "<br>
<center>
<a target=\"_blank\" href=\"https://kuberthemes.com/topic/288-how-to-install-opencart-kofi-theme/\" class=\"btn btn-success\" role=\"button\">Create Free Support Ticket</a>
<a target=\"_blank\" href=\"http://kofi.dedu.live/\" class=\"btn btn-danger\" role=\"button\"><img src=\"../admin/view/image/small-thumb.jpg\" style=\"height:40px\"> Click here For Kofi Theme Documentation (Video Guide)</a>
</center>

</footer>

</div>


</body></html>";
    }

    public function getTemplateName()
    {
        return "common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "common/footer.twig", "");
    }
}
