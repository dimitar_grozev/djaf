<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/new_elements/wrapper_bottom.twig */
class __TwigTemplate_65a5833de8be195efc42b18fbf4d2db902c2845aa140590a9373e18313eb903d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
\t";
            // line 2
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
            // line 3
            echo "\t";
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
            // line 4
            echo "
\t";
            // line 5
            $context["columnleft"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_left"], "method", false, false, false, 5);
            // line 6
            echo "\t";
            $context["grid_center"] = 12;
            echo " 
\t";
            // line 7
            if ((twig_length_filter($this->env, ($context["columnleft"] ?? null)) > 0)) {
                echo " 
\t\t";
                // line 8
                $context["grid_center"] = 9;
                echo " 
\t";
            }
            // line 9
            echo " 

\t";
            // line 11
            $context["grid_content_right"] = 3;
            // line 12
            echo "\t";
            $context["column_right"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_right"], "method", false, false, false, 12);
            echo " 
\t";
            // line 13
            if ((twig_length_filter($this->env, ($context["column_right"] ?? null)) > 0)) {
                // line 14
                echo "\t\t";
                if ((($context["grid_center"] ?? null) == 9)) {
                    // line 15
                    echo "\t\t\t";
                    $context["grid_content_right"] = 4;
                    // line 16
                    echo "\t\t";
                } else {
                    echo " 
\t\t\t";
                    // line 17
                    $context["grid_content_right"] = 3;
                    // line 18
                    echo "\t\t";
                }
                // line 19
                echo "\t";
            }
            // line 20
            echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
            // line 23
            if ((twig_length_filter($this->env, ($context["column_right"] ?? null)) > 0)) {
                echo " 
\t\t\t\t\t\t\t<div class=\"col-md-";
                // line 24
                echo ($context["grid_content_right"] ?? null);
                echo "\" id=\"column-right\">
\t\t\t\t\t\t\t\t";
                // line 25
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["column_right"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 26
                    echo $context["module"];
                    echo "
\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 27
                echo " 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
            }
            // line 29
            echo " 
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t";
            // line 32
            if ((array_key_exists("products", $context) && array_key_exists("product_page", $context))) {
                echo " ";
                if ((($context["products"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_related_status"], "method", false, false, false, 32) != "0"))) {
                    echo " 
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                    // line 34
                    $context["class"] = 3;
                    echo " 
\t\t\t\t\t\t\t";
                    // line 35
                    $context["id"] = (twig_random($this->env, 5000) * twig_random($this->env, 500000));
                    echo " 
\t\t\t\t\t\t\t";
                    // line 36
                    $context["all"] = 4;
                    echo " 
\t\t\t\t\t\t\t";
                    // line 37
                    $context["row"] = 4;
                    echo " 
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                    // line 39
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 39) == 6)) {
                        echo " ";
                        $context["class"] = 2;
                        echo " ";
                    }
                    // line 40
                    echo "\t\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 40) == 5)) {
                        echo " ";
                        $context["class"] = 25;
                        echo " ";
                    }
                    // line 41
                    echo "\t\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 41) == 3)) {
                        echo " ";
                        $context["class"] = 4;
                        echo " ";
                    }
                    // line 42
                    echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                    // line 43
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 43) > 1)) {
                        echo " ";
                        $context["row"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 43);
                        echo " ";
                        $context["all"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 43);
                        echo " ";
                    }
                    echo " 
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"box clearfix box-with-products box-no-advanced ";
                    // line 45
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_scroll_related"], "method", false, false, false, 45) != "0")) {
                        echo " ";
                        echo "with-scroll";
                        echo " ";
                    }
                    echo "\">
\t\t\t\t\t\t\t  ";
                    // line 46
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_scroll_related"], "method", false, false, false, 46) != "0")) {
                        echo " 
\t\t\t\t\t\t\t  <!-- Carousel nav -->
\t\t\t\t\t\t\t  <a class=\"next\" href=\"#myCarousel";
                        // line 48
                        echo ($context["id"] ?? null);
                        echo "\" id=\"myCarousel";
                        echo ($context["id"] ?? null);
                        echo "_next\"><span></span></a>
\t\t\t\t\t\t\t  <a class=\"prev\" href=\"#myCarousel";
                        // line 49
                        echo ($context["id"] ?? null);
                        echo "\" id=\"myCarousel";
                        echo ($context["id"] ?? null);
                        echo "_prev\"><span></span></a>
\t\t\t\t\t\t\t  ";
                    }
                    // line 50
                    echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t  <div class=\"box-heading\">";
                    // line 52
                    echo ($context["text_related"] ?? null);
                    echo "</div>
\t\t\t\t\t\t\t  <div class=\"strip-line\"></div>
\t\t\t\t\t\t\t  <div class=\"clear\"></div>
\t\t\t\t\t\t\t  <div class=\"box-content products related-products\">
\t\t\t\t\t\t\t    <div class=\"box-product\">
\t\t\t\t\t\t\t    \t<div id=\"myCarousel";
                    // line 57
                    echo ($context["id"] ?? null);
                    echo "\" ";
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_scroll_related"], "method", false, false, false, 57) != "0")) {
                        echo "class=\"carousel slide\"";
                    }
                    echo ">
\t\t\t\t\t\t\t    \t\t<!-- Carousel items -->
\t\t\t\t\t\t\t    \t\t<div class=\"carousel-inner\">
\t\t\t\t\t\t\t    \t\t\t";
                    // line 60
                    $context["i"] = 0;
                    echo " ";
                    $context["row_fluid"] = 0;
                    echo " ";
                    $context["item"] = 0;
                    echo " ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                        echo " ";
                        $context["row_fluid"] = (($context["row_fluid"] ?? null) + 1);
                        echo " 
\t\t\t\t\t\t\t\t    \t\t\t";
                        // line 61
                        if ((($context["i"] ?? null) == 0)) {
                            echo " ";
                            $context["item"] = (($context["item"] ?? null) + 1);
                            echo " ";
                            echo "<div class=\"active item\"><div class=\"product-grid\"><div class=\"row\">";
                            echo " ";
                        }
                        echo " 
\t\t\t\t\t\t\t\t    \t\t\t";
                        // line 62
                        $context["r"] = (($context["row_fluid"] ?? null) - (twig_round((($context["row_fluid"] ?? null) / ($context["all"] ?? null)), 0, "floor") * ($context["all"] ?? null)));
                        echo " ";
                        if (((($context["row_fluid"] ?? null) > ($context["all"] ?? null)) && (($context["r"] ?? null) == 1))) {
                            echo " ";
                            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_scroll_related"], "method", false, false, false, 62) != "0")) {
                                echo " ";
                                echo "</div></div></div><div class=\"item\"><div class=\"product-grid\"><div class=\"row\">";
                                echo " ";
                                $context["item"] = (($context["item"] ?? null) + 1);
                                echo " ";
                            } else {
                                echo " ";
                                echo "</div><div class=\"row\">";
                                echo " ";
                            }
                            echo " ";
                        } else {
                            echo " ";
                            $context["r"] = (($context["row_fluid"] ?? null) - (twig_round((($context["row_fluid"] ?? null) / ($context["row"] ?? null)), 0, "floor") * ($context["row"] ?? null)));
                            echo " ";
                            if (((($context["row_fluid"] ?? null) > ($context["row"] ?? null)) && (($context["r"] ?? null) == 1))) {
                                echo " ";
                                echo "</div><div class=\"row\">";
                                echo " ";
                            }
                            echo " ";
                        }
                        echo " 
\t\t\t\t\t\t\t\t    \t\t\t<div class=\"col-sm-";
                        // line 63
                        echo ($context["class"] ?? null);
                        echo " col-xs-6\">
\t\t\t\t\t\t\t\t    \t\t\t\t";
                        // line 64
                        $this->loadTemplate("kofi/template/new_elements/product.twig", "kofi/template/new_elements/wrapper_bottom.twig", 64)->display($context);
                        // line 65
                        echo "\t\t\t\t\t\t\t\t    \t\t\t</div>
\t\t\t\t\t\t\t    \t\t\t";
                        // line 66
                        $context["i"] = (($context["i"] ?? null) + 1);
                        echo " ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    echo " 
\t\t\t\t\t\t\t    \t\t\t";
                    // line 67
                    if ((($context["i"] ?? null) > 0)) {
                        echo " ";
                        echo "</div></div></div>";
                        echo " ";
                    }
                    echo " 
\t\t\t\t\t\t\t    \t\t</div>
\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                    // line 74
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_scroll_related"], "method", false, false, false, 74) != "0")) {
                        echo " 
\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\$(document).ready(function() {
\t\t\t\t\t\t\t  var owl";
                        // line 77
                        echo ($context["id"] ?? null);
                        echo " = \$(\".box #myCarousel";
                        echo ($context["id"] ?? null);
                        echo " .carousel-inner\");
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t  \$(\"#myCarousel";
                        // line 79
                        echo ($context["id"] ?? null);
                        echo "_next\").click(function(){
\t\t\t\t\t\t\t      owl";
                        // line 80
                        echo ($context["id"] ?? null);
                        echo ".trigger('owl.next');
\t\t\t\t\t\t\t      return false;
\t\t\t\t\t\t\t    })
\t\t\t\t\t\t\t  \$(\"#myCarousel";
                        // line 83
                        echo ($context["id"] ?? null);
                        echo "_prev\").click(function(){
\t\t\t\t\t\t\t      owl";
                        // line 84
                        echo ($context["id"] ?? null);
                        echo ".trigger('owl.prev');
\t\t\t\t\t\t\t      return false;
\t\t\t\t\t\t\t  });
\t\t\t\t\t\t\t    
\t\t\t\t\t\t\t  owl";
                        // line 88
                        echo ($context["id"] ?? null);
                        echo ".owlCarousel({
\t\t\t\t\t\t\t  \t  slideSpeed : 500,
\t\t\t\t\t\t\t      singleItem:true
\t\t\t\t\t\t\t   });
\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t";
                    }
                    // line 94
                    echo " 
\t\t\t\t\t\t";
                }
            }
            // line 95
            echo " 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"row\">\t
\t\t\t\t\t<div class=\"col-sm-12\">\t
\t\t\t\t\t\t
\t\t\t\t\t\t";
            // line 102
            $context["contentbottom"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_bottom"], "method", false, false, false, 102);
            // line 103
            echo "\t\t\t\t\t\t";
            if ((twig_length_filter($this->env, ($context["contentbottom"] ?? null)) > 0)) {
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 104
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["contentbottom"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t\t\t\t\t";
                    // line 105
                    echo $context["module"];
                    echo "
\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 107
                echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t";
            }
            // line 108
            echo " 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>\t\t\t\t  
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "kofi/template/new_elements/wrapper_bottom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  430 => 108,  426 => 107,  418 => 105,  412 => 104,  407 => 103,  405 => 102,  396 => 95,  391 => 94,  381 => 88,  374 => 84,  370 => 83,  364 => 80,  360 => 79,  353 => 77,  347 => 74,  333 => 67,  316 => 66,  313 => 65,  311 => 64,  307 => 63,  277 => 62,  267 => 61,  240 => 60,  230 => 57,  222 => 52,  218 => 50,  211 => 49,  205 => 48,  200 => 46,  192 => 45,  181 => 43,  178 => 42,  171 => 41,  164 => 40,  158 => 39,  153 => 37,  149 => 36,  145 => 35,  141 => 34,  134 => 32,  129 => 29,  124 => 27,  116 => 26,  110 => 25,  106 => 24,  102 => 23,  97 => 20,  94 => 19,  91 => 18,  89 => 17,  84 => 16,  81 => 15,  78 => 14,  76 => 13,  71 => 12,  69 => 11,  65 => 9,  60 => 8,  56 => 7,  51 => 6,  49 => 5,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/new_elements/wrapper_bottom.twig", "/home/prod85fb/public_html/catalog/view/theme/kofi/template/new_elements/wrapper_bottom.twig");
    }
}
