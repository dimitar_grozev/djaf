<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/error/not_found.twig */
class __TwigTemplate_eff491f00e8cd44c7186a1afb5ba0423171287515da29f94a1814224fde2c8a7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $this->loadTemplate("kofi/template/new_elements/wrapper_top.twig", "kofi/template/error/not_found.twig", 2)->display($context);
        // line 3
        echo "
";
        // line 4
        if (array_key_exists("text_error", $context)) {
            echo "<p>";
            echo ($context["text_error"] ?? null);
            echo "</p>";
        }
        echo " 
<div class=\"buttons\">
  <div class=\"pull-right\"><a href=\"";
        // line 6
        echo ($context["continue"] ?? null);
        echo "\" class=\"btn btn-primary\">";
        echo ($context["button_continue"] ?? null);
        echo "</a></div>
</div>
  
";
        // line 9
        $this->loadTemplate("kofi/template/new_elements/wrapper_bottom.twig", "kofi/template/error/not_found.twig", 9)->display($context);
        // line 10
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "kofi/template/error/not_found.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 10,  63 => 9,  55 => 6,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/error/not_found.twig", "");
    }
}
