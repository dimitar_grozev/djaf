<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/account/return_form.twig */
class __TwigTemplate_00cef944cae1ff6ebe2dca8ee87513f93bb3b99441e096ed65245f32ea50c7a2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $this->loadTemplate("kofi/template/new_elements/wrapper_top.twig", "kofi/template/account/return_form.twig", 2)->display($context);
        // line 3
        echo "
<p>";
        // line 4
        echo ($context["text_description"] ?? null);
        echo "</p>
<form action=\"";
        // line 5
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">
  <fieldset>
    <legend>";
        // line 7
        echo ($context["text_order"] ?? null);
        echo "</legend>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"input-firstname\">";
        // line 9
        echo ($context["entry_firstname"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\">
        <input type=\"text\" name=\"firstname\" value=\"";
        // line 11
        echo ($context["firstname"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_firstname"] ?? null);
        echo "\" id=\"input-firstname\" class=\"form-control\" />
        ";
        // line 12
        if (($context["error_firstname"] ?? null)) {
            // line 13
            echo "        <div class=\"text-danger\">";
            echo ($context["error_firstname"] ?? null);
            echo "</div>
        ";
        }
        // line 14
        echo " </div>
    </div>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"input-lastname\">";
        // line 17
        echo ($context["entry_lastname"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\">
        <input type=\"text\" name=\"lastname\" value=\"";
        // line 19
        echo ($context["lastname"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_lastname"] ?? null);
        echo "\" id=\"input-lastname\" class=\"form-control\" />
        ";
        // line 20
        if (($context["error_lastname"] ?? null)) {
            // line 21
            echo "        <div class=\"text-danger\">";
            echo ($context["error_lastname"] ?? null);
            echo "</div>
        ";
        }
        // line 22
        echo " </div>
    </div>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"input-email\">";
        // line 25
        echo ($context["entry_email"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\">
        <input type=\"text\" name=\"email\" value=\"";
        // line 27
        echo ($context["email"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_email"] ?? null);
        echo "\" id=\"input-email\" class=\"form-control\" />
        ";
        // line 28
        if (($context["error_email"] ?? null)) {
            // line 29
            echo "        <div class=\"text-danger\">";
            echo ($context["error_email"] ?? null);
            echo "</div>
        ";
        }
        // line 30
        echo " </div>
    </div>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"input-telephone\">";
        // line 33
        echo ($context["entry_telephone"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\">
        <input type=\"text\" name=\"telephone\" value=\"";
        // line 35
        echo ($context["telephone"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_telephone"] ?? null);
        echo "\" id=\"input-telephone\" class=\"form-control\" />
        ";
        // line 36
        if (($context["error_telephone"] ?? null)) {
            // line 37
            echo "        <div class=\"text-danger\">";
            echo ($context["error_telephone"] ?? null);
            echo "</div>
        ";
        }
        // line 38
        echo " </div>
    </div>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"input-order-id\">";
        // line 41
        echo ($context["entry_order_id"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\">
        <input type=\"text\" name=\"order_id\" value=\"";
        // line 43
        echo ($context["order_id"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_order_id"] ?? null);
        echo "\" id=\"input-order-id\" class=\"form-control\" />
        ";
        // line 44
        if (($context["error_order_id"] ?? null)) {
            // line 45
            echo "        <div class=\"text-danger\">";
            echo ($context["error_order_id"] ?? null);
            echo "</div>
        ";
        }
        // line 46
        echo " </div>
    </div>
    <div class=\"form-group\">
      <label class=\"col-sm-2 control-label\" for=\"input-date-ordered\">";
        // line 49
        echo ($context["entry_date_ordered"] ?? null);
        echo "</label>
      <div class=\"col-sm-3\">
        <div class=\"input-group date\">
          <input type=\"text\" name=\"date_ordered\" value=\"";
        // line 52
        echo ($context["date_ordered"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_date_ordered"] ?? null);
        echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-date-ordered\" class=\"form-control\" />
          <span class=\"input-group-btn\">
          <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
          </span></div>
      </div>
    </div>
  </fieldset>
  <fieldset>
    <legend>";
        // line 60
        echo ($context["text_product"] ?? null);
        echo "</legend>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"input-product\">";
        // line 62
        echo ($context["entry_product"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\">
        <input type=\"text\" name=\"product\" value=\"";
        // line 64
        echo ($context["product"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_product"] ?? null);
        echo "\" id=\"input-product\" class=\"form-control\" />
        ";
        // line 65
        if (($context["error_product"] ?? null)) {
            // line 66
            echo "        <div class=\"text-danger\">";
            echo ($context["error_product"] ?? null);
            echo "</div>
        ";
        }
        // line 67
        echo " </div>
    </div>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\" for=\"input-model\">";
        // line 70
        echo ($context["entry_model"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\">
        <input type=\"text\" name=\"model\" value=\"";
        // line 72
        echo ($context["model"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_model"] ?? null);
        echo "\" id=\"input-model\" class=\"form-control\" />
        ";
        // line 73
        if (($context["error_model"] ?? null)) {
            // line 74
            echo "        <div class=\"text-danger\">";
            echo ($context["error_model"] ?? null);
            echo "</div>
        ";
        }
        // line 75
        echo " </div>
    </div>
    <div class=\"form-group\">
      <label class=\"col-sm-2 control-label\" for=\"input-quantity\">";
        // line 78
        echo ($context["entry_quantity"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\">
        <input type=\"text\" name=\"quantity\" value=\"";
        // line 80
        echo ($context["quantity"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_quantity"] ?? null);
        echo "\" id=\"input-quantity\" class=\"form-control\" />
      </div>
    </div>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\">";
        // line 84
        echo ($context["entry_reason"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\"> ";
        // line 85
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["return_reasons"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["return_reason"]) {
            // line 86
            echo "        ";
            if ((twig_get_attribute($this->env, $this->source, $context["return_reason"], "return_reason_id", [], "any", false, false, false, 86) == ($context["return_reason_id"] ?? null))) {
                // line 87
                echo "        <div class=\"radio\">
          <label>
            <input type=\"radio\" name=\"return_reason_id\" value=\"";
                // line 89
                echo twig_get_attribute($this->env, $this->source, $context["return_reason"], "return_reason_id", [], "any", false, false, false, 89);
                echo "\" checked=\"checked\" />
            ";
                // line 90
                echo twig_get_attribute($this->env, $this->source, $context["return_reason"], "name", [], "any", false, false, false, 90);
                echo "</label>
        </div>
        ";
            } else {
                // line 93
                echo "        <div class=\"radio\">
          <label>
            <input type=\"radio\" name=\"return_reason_id\" value=\"";
                // line 95
                echo twig_get_attribute($this->env, $this->source, $context["return_reason"], "return_reason_id", [], "any", false, false, false, 95);
                echo "\" />
            ";
                // line 96
                echo twig_get_attribute($this->env, $this->source, $context["return_reason"], "name", [], "any", false, false, false, 96);
                echo "</label>
        </div>
        ";
            }
            // line 99
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['return_reason'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "        ";
        if (($context["error_reason"] ?? null)) {
            // line 101
            echo "        <div class=\"text-danger\">";
            echo ($context["error_reason"] ?? null);
            echo "</div>
        ";
        }
        // line 102
        echo " </div>
    </div>
    <div class=\"form-group required\">
      <label class=\"col-sm-2 control-label\">";
        // line 105
        echo ($context["entry_opened"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\">
        <label class=\"radio-inline\"> ";
        // line 107
        if (($context["opened"] ?? null)) {
            // line 108
            echo "          <input type=\"radio\" name=\"opened\" value=\"1\" checked=\"checked\" />
          ";
        } else {
            // line 110
            echo "          <input type=\"radio\" name=\"opened\" value=\"1\" />
          ";
        }
        // line 112
        echo "          ";
        echo ($context["text_yes"] ?? null);
        echo "</label>
        <label class=\"radio-inline\"> ";
        // line 113
        if ( !($context["opened"] ?? null)) {
            // line 114
            echo "          <input type=\"radio\" name=\"opened\" value=\"0\" checked=\"checked\" />
          ";
        } else {
            // line 116
            echo "          <input type=\"radio\" name=\"opened\" value=\"0\" />
          ";
        }
        // line 118
        echo "          ";
        echo ($context["text_no"] ?? null);
        echo "</label>
      </div>
    </div>
    <div class=\"form-group\">
      <label class=\"col-sm-2 control-label\" for=\"input-comment\">";
        // line 122
        echo ($context["entry_fault_detail"] ?? null);
        echo "</label>
      <div class=\"col-sm-10\">
        <textarea name=\"comment\" rows=\"10\" placeholder=\"";
        // line 124
        echo ($context["entry_fault_detail"] ?? null);
        echo "\" id=\"input-comment\" class=\"form-control\">";
        echo ($context["comment"] ?? null);
        echo "</textarea>
      </div>
    </div>
    ";
        // line 127
        echo ($context["captcha"] ?? null);
        echo "
  </fieldset>
  ";
        // line 129
        if (($context["text_agree"] ?? null)) {
            // line 130
            echo "  <div class=\"buttons clearfix\">
    <div class=\"pull-left\"><a href=\"";
            // line 131
            echo ($context["back"] ?? null);
            echo "\" class=\"btn btn-danger\">";
            echo ($context["button_back"] ?? null);
            echo "</a></div>
    <div class=\"pull-right\">";
            // line 132
            echo ($context["text_agree"] ?? null);
            echo "
      ";
            // line 133
            if (($context["agree"] ?? null)) {
                // line 134
                echo "      <input type=\"checkbox\" name=\"agree\" value=\"1\" checked=\"checked\" />
      ";
            } else {
                // line 136
                echo "      <input type=\"checkbox\" name=\"agree\" value=\"1\" />
      ";
            }
            // line 138
            echo "      <input type=\"submit\" value=\"";
            echo ($context["button_submit"] ?? null);
            echo "\" class=\"btn btn-primary\" />
    </div>
  </div>
  ";
        } else {
            // line 142
            echo "  <div class=\"buttons clearfix\">
    <div class=\"pull-left\"><a href=\"";
            // line 143
            echo ($context["back"] ?? null);
            echo "\" class=\"btn btn-default\">";
            echo ($context["button_back"] ?? null);
            echo "</a></div>
    <div class=\"pull-right\">
      <input type=\"submit\" value=\"";
            // line 145
            echo ($context["button_submit"] ?? null);
            echo "\" class=\"btn btn-primary\" />
    </div>
  </div>
  ";
        }
        // line 149
        echo "</form>

<script type=\"text/javascript\"><!--
\$('.date').datetimepicker({
  language: '";
        // line 153
        echo ($context["datepicker"] ?? null);
        echo "',
  pickTime: false
});
//--></script> 

";
        // line 158
        $this->loadTemplate("kofi/template/new_elements/wrapper_bottom.twig", "kofi/template/account/return_form.twig", 158)->display($context);
        // line 159
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "kofi/template/account/return_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  447 => 159,  445 => 158,  437 => 153,  431 => 149,  424 => 145,  417 => 143,  414 => 142,  406 => 138,  402 => 136,  398 => 134,  396 => 133,  392 => 132,  386 => 131,  383 => 130,  381 => 129,  376 => 127,  368 => 124,  363 => 122,  355 => 118,  351 => 116,  347 => 114,  345 => 113,  340 => 112,  336 => 110,  332 => 108,  330 => 107,  325 => 105,  320 => 102,  314 => 101,  311 => 100,  305 => 99,  299 => 96,  295 => 95,  291 => 93,  285 => 90,  281 => 89,  277 => 87,  274 => 86,  270 => 85,  266 => 84,  257 => 80,  252 => 78,  247 => 75,  241 => 74,  239 => 73,  233 => 72,  228 => 70,  223 => 67,  217 => 66,  215 => 65,  209 => 64,  204 => 62,  199 => 60,  186 => 52,  180 => 49,  175 => 46,  169 => 45,  167 => 44,  161 => 43,  156 => 41,  151 => 38,  145 => 37,  143 => 36,  137 => 35,  132 => 33,  127 => 30,  121 => 29,  119 => 28,  113 => 27,  108 => 25,  103 => 22,  97 => 21,  95 => 20,  89 => 19,  84 => 17,  79 => 14,  73 => 13,  71 => 12,  65 => 11,  60 => 9,  55 => 7,  50 => 5,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/account/return_form.twig", "");
    }
}
