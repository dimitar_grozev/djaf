<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* kofi/template/new_elements/product.twig */
class __TwigTemplate_20a63d7f1cc2bb5526aed41b0809bda88a41d6b8397225f9aa99c18a4800c14e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 1);
        // line 2
        $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 2);
        // line 3
        echo "
<!-- Product -->
<div class=\"product clearfix product-hover\">
\t<div class=\"left\">
\t\t";
        // line 7
        if ((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["product"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["thumb"] ?? null) : null)) {
            echo " 
\t\t\t";
            // line 8
            if (((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["product"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["special"] ?? null) : null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_text_sale"], "method", false, false, false, 8) != "0"))) {
                echo " 
\t\t\t\t";
                // line 9
                $context["text_sale"] = "Sale";
                // line 10
                echo "\t\t\t\t";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 10)], "method", false, false, false, 10) != "")) {
                    // line 11
                    echo "\t\t\t\t\t";
                    $context["text_sale"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 11)], "method", false, false, false, 11);
                    // line 12
                    echo "\t\t\t\t";
                }
                echo " 

\t\t\t\t";
                // line 14
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "type_sale"], "method", false, false, false, 14) == "1")) {
                    echo " 
\t\t\t\t\t";
                    // line 15
                    $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["product"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["product_id"] ?? null) : null)], "method", false, false, false, 15);
                    // line 16
                    echo "\t\t\t\t\t";
                    $context["roznica_ceny"] = ((($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["product_detail"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["price"] ?? null) : null) - (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["product_detail"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["special"] ?? null) : null));
                    // line 17
                    echo "\t\t\t\t\t";
                    $context["procent"] = ((($context["roznica_ceny"] ?? null) * 100) / (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["product_detail"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["price"] ?? null) : null));
                    echo " 
\t\t\t\t\t<div class=\"sale\">-";
                    // line 18
                    echo twig_round(($context["procent"] ?? null));
                    echo "%</div>
\t\t\t\t";
                } else {
                    // line 19
                    echo " 
\t\t\t\t\t<div class=\"sale\">";
                    // line 20
                    echo ($context["text_sale"] ?? null);
                    echo "</div>
\t\t\t\t";
                }
                // line 22
                echo "\t\t\t\t
\t\t\t ";
            } elseif (((twig_get_attribute($this->env, $this->source,             // line 23
($context["theme_options"] ?? null), "get", [0 => "display_text_new"], "method", false, false, false, 23) != "0") && twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "isLatestProduct", [0 => (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["product"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["product_id"] ?? null) : null)], "method", false, false, false, 23))) {
                echo " 
\t\t\t\t <div class=\"new\">";
                // line 24
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "new_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 24)], "method", false, false, false, 24) != "")) {
                    echo " ";
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "new_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 24)], "method", false, false, false, 24);
                    echo " ";
                } else {
                    echo " ";
                    echo "New";
                    echo " ";
                }
                echo "</div>
\t\t\t ";
            }
            // line 25
            echo " 
\t\t\t
\t\t\t<div class=\"image ";
            // line 27
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_image_effect"], "method", false, false, false, 27) == "1")) {
                echo " ";
                echo "image-swap-effect";
                echo " ";
            }
            echo "\">
\t\t\t\t<a href=\"";
            // line 28
            echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["product"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["href"] ?? null) : null);
            echo "\">
\t\t\t\t\t";
            // line 29
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_image_effect"], "method", false, false, false, 29) == "1")) {
                // line 30
                echo "\t\t\t\t\t\t";
                $context["nthumb"] = twig_replace_filter((($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["product"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["thumb"] ?? null) : null), [" " => "%20"]);
                // line 31
                echo "\t\t\t\t\t\t";
                $context["adres"] = twig_constant("HTTP_SERVER");
                // line 32
                echo "\t\t\t\t\t\t";
                $context["nthumb"] = twig_replace_filter(($context["nthumb"] ?? null), ["adres" => ""]);
                // line 33
                echo "\t\t\t\t\t\t";
                $context["image_size"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getimagesize", [0 => ($context["nthumb"] ?? null)], "method", false, false, false, 33);
                // line 34
                echo "\t\t\t\t\t\t";
                $context["image_swap"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "productImageSwap", [0 => (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["product"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["product_id"] ?? null) : null), 1 => (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["image_size"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9[0] ?? null) : null), 2 => (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["image_size"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae[1] ?? null) : null)], "method", false, false, false, 34);
                // line 35
                echo "\t\t\t\t\t\t";
                if ((($context["image_swap"] ?? null) != "")) {
                    echo " ";
                    echo (((("<img src=\"" . ($context["image_swap"] ?? null)) . "\" alt=\"") . (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = ($context["product"] ?? null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["name"] ?? null) : null)) . "\" class=\"swap-image\" />");
                }
                // line 36
                echo "\t\t\t\t\t";
            }
            echo " 
\t\t\t\t\t
\t\t\t\t\t";
            // line 38
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "lazy_loading_images"], "method", false, false, false, 38) != "0")) {
                echo " 
\t\t\t\t\t<img src=\"image/catalog/blank.gif\" data-echo=\"";
                // line 39
                echo (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = ($context["product"] ?? null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["thumb"] ?? null) : null);
                echo "\" alt=\"";
                echo (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = ($context["product"] ?? null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["name"] ?? null) : null);
                echo "\" class=\"";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_image_effect"], "method", false, false, false, 39) == "2")) {
                    echo " ";
                    echo "zoom-image-effect";
                    echo " ";
                }
                echo "\" />
\t\t\t\t\t";
            } else {
                // line 40
                echo " 
\t\t\t\t\t<img src=\"";
                // line 41
                echo (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = ($context["product"] ?? null)) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["thumb"] ?? null) : null);
                echo "\" alt=\"";
                echo (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = ($context["product"] ?? null)) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["name"] ?? null) : null);
                echo "\" class=\"";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_image_effect"], "method", false, false, false, 41) == "2")) {
                    echo " ";
                    echo "zoom-image-effect";
                    echo " ";
                }
                echo "\" />
\t\t\t\t\t";
            }
            // line 42
            echo " 
\t\t\t\t</a>
\t\t\t</div>
\t\t";
        } else {
            // line 45
            echo " 
\t\t\t<div class=\"image\">
\t\t\t\t<a href=\"";
            // line 47
            echo (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = ($context["product"] ?? null)) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["href"] ?? null) : null);
            echo "\"><img src=\"image/no_image.jpg\" alt=\"";
            echo (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = ($context["product"] ?? null)) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["name"] ?? null) : null);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_image_effect"], "method", false, false, false, 47) == "2")) {
                echo " ";
                echo "class=\"zoom-image-effect\"";
                echo " ";
            }
            echo " /></a>
\t\t\t</div>
\t\t";
        }
        // line 49
        echo " 
\t\t
\t\t";
        // line 51
        if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_specials_countdown"], "method", false, false, false, 51) == "1") && (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = ($context["product"] ?? null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["special"] ?? null) : null))) {
            echo " 
\t\t\t";
            // line 52
            $context["countdown"] = (twig_random($this->env, 5000) * twig_random($this->env, 50000));
            echo " 
\t\t ";
            // line 53
            $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = ($context["product"] ?? null)) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["product_id"] ?? null) : null)], "method", false, false, false, 53);
            // line 54
            echo "\t\t ";
            $context["date_end"] = (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = ($context["product_detail"] ?? null)) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["date_end"] ?? null) : null);
            // line 55
            echo "\t\t ";
            if (((($context["date_end"] ?? null) != "0000-00-00") && ($context["date_end"] ?? null))) {
                echo " 
               \t\t<script>
               \t\t\$(function () {
               \t\t\tvar austDay = new Date();
               \t\t\taustDay = new Date(";
                // line 59
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "Y", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 59)], "method", false, false, false, 59);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "m", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 59)], "method", false, false, false, 59);
                echo " - 1, ";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "d", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => ($context["date_end"] ?? null)], "method", false, false, false, 59)], "method", false, false, false, 59);
                echo ");
               \t\t\t\$('#countdown";
                // line 60
                echo ($context["countdown"] ?? null);
                echo "').countdown({until: austDay});
               \t\t});
               \t\t</script>
               \t\t<div id=\"countdown";
                // line 63
                echo ($context["countdown"] ?? null);
                echo "\" class=\"clearfix\"></div>
     \t\t     ";
            }
            // line 64
            echo " 
\t\t";
        }
        // line 65
        echo "  
\t</div>
\t<div class=\"right\">
\t\t<div class=\"name\"><a href=\"";
        // line 68
        echo (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = ($context["product"] ?? null)) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["href"] ?? null) : null);
        echo "\">";
        echo (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = ($context["product"] ?? null)) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["name"] ?? null) : null);
        echo "</a></div>
\t\t";
        // line 69
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 69) == "7")) {
            echo " 
\t\t\t";
            // line 70
            $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = ($context["product"] ?? null)) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["product_id"] ?? null) : null)], "method", false, false, false, 70);
            echo " 
\t\t\t<div class=\"brand\">";
            // line 71
            echo (($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = ($context["product_detail"] ?? null)) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["manufacturer"] ?? null) : null);
            echo "</div>
\t\t";
        }
        // line 72
        echo " 
\t\t
\t\t";
        // line 74
        if ((($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = ($context["product"] ?? null)) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["price"] ?? null) : null)) {
            echo " 
\t\t<div class=\"price\">
\t\t\t";
            // line 76
            if ( !(($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = ($context["product"] ?? null)) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["special"] ?? null) : null)) {
                echo " 
\t\t\t";
                // line 77
                echo (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = ($context["product"] ?? null)) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["price"] ?? null) : null);
                echo " 
\t\t\t";
            } else {
                // line 78
                echo " 
\t\t\t<span class=\"price-old\">";
                // line 79
                echo (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = ($context["product"] ?? null)) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["price"] ?? null) : null);
                echo "</span> <span class=\"price-new\">";
                echo (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = ($context["product"] ?? null)) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["special"] ?? null) : null);
                echo "</span>
\t\t\t";
            }
            // line 80
            echo " 
\t\t</div>
\t\t";
        }
        // line 82
        echo " 
\t\t
\t\t
\t\t";
        // line 85
        if ((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_compare"], "method", false, false, false, 85) != "0") || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_wishlist"], "method", false, false, false, 85) != "0")) || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_cart"], "method", false, false, false, 85) != "0"))) {
            echo " 
\t\t<div class=\"only-hover\">
\t\t     <ul>
\t\t          ";
            // line 88
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_cart"], "method", false, false, false, 88) != "0")) {
                echo " 
\t\t               ";
                // line 89
                $context["enquiry"] = twig_constant("false");
                echo " 
\t\t               \t";
                // line 90
                if ((twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "product_blocks_module"], "method", false, false, false, 90) != "")) {
                    echo " ";
                    $context["enquiry"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "productIsEnquiry", [0 => (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = ($context["product"] ?? null)) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["product_id"] ?? null) : null)], "method", false, false, false, 90);
                    echo " ";
                }
                // line 91
                echo "\t\t \t\t\t\t";
                if (twig_test_iterable(($context["enquiry"] ?? null))) {
                    echo " 
\t\t               \t\t<li><a href=\"javascript:openPopup('";
                    // line 92
                    echo (($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = ($context["enquiry"] ?? null)) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["popup_module"] ?? null) : null);
                    echo "', '";
                    echo (($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = ($context["product"] ?? null)) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["product_id"] ?? null) : null);
                    echo "')\" data-toggle=\"tooltip\" data-original-title=\"";
                    echo (($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = ($context["enquiry"] ?? null)) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["block_name"] ?? null) : null);
                    echo "\"><i class=\"fa fa-question\"></i></a></li>
\t\t               ";
                } else {
                    // line 93
                    echo " 
\t\t               \t\t<li class=\"add-to-cart\"><a onclick=\"cart.add('";
                    // line 94
                    echo (($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = ($context["product"] ?? null)) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["product_id"] ?? null) : null);
                    echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                    echo ($context["button_cart"] ?? null);
                    echo "\"></a></li>
\t\t               ";
                }
                // line 95
                echo " 
\t\t          ";
            }
            // line 96
            echo " 
\t\t          
\t\t          ";
            // line 98
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quick_view"], "method", false, false, false, 98) == 1)) {
                echo " 
\t\t          <li class=\"quickview\"><a href=\"index.php?route=product/quickview&amp;product_id=";
                // line 99
                echo (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = ($context["product"] ?? null)) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["product_id"] ?? null) : null);
                echo "\" data-toggle=\"tooltip\" data-original-title=\"";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quickview_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 99)], "method", false, false, false, 99) != "")) {
                    echo " ";
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "html_entity_decode", [0 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quickview_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 99)], "method", false, false, false, 99)], "method", false, false, false, 99);
                    echo " ";
                } else {
                    echo " ";
                    echo "Quickview";
                    echo " ";
                }
                echo "\"></a></li>
\t\t          ";
            }
            // line 100
            echo " 
\t\t     
\t\t\t\t";
            // line 102
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_compare"], "method", false, false, false, 102) != "0")) {
                echo " 
\t\t\t\t<li class=\"compare\"><a onclick=\"compare.add('";
                // line 103
                echo (($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = ($context["product"] ?? null)) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["product_id"] ?? null) : null);
                echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 103)], "method", false, false, false, 103) != "")) {
                    echo " ";
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 103)], "method", false, false, false, 103);
                    echo " ";
                } else {
                    echo " ";
                    echo "Add to compare";
                    echo " ";
                }
                echo "\"></a></li>
\t\t\t\t";
            }
            // line 104
            echo " 
\t\t\t\t
\t\t\t\t";
            // line 106
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_wishlist"], "method", false, false, false, 106) != "0")) {
                echo " 
\t\t\t\t<li class=\"wishlist\"><a onclick=\"wishlist.add('";
                // line 107
                echo (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = ($context["product"] ?? null)) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["product_id"] ?? null) : null);
                echo "');\" data-toggle=\"tooltip\" data-original-title=\"";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_wishlist_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 107)], "method", false, false, false, 107) != "")) {
                    echo " ";
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_wishlist_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 107)], "method", false, false, false, 107);
                    echo " ";
                } else {
                    echo " ";
                    echo "Add to wishlist";
                    echo " ";
                }
                echo "\"></a></li>
\t\t\t\t";
            }
            // line 108
            echo " 
\t\t\t</ul>
\t\t</div>
\t\t";
        }
        // line 111
        echo " 
\t\t
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "kofi/template/new_elements/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  451 => 111,  445 => 108,  430 => 107,  426 => 106,  422 => 104,  407 => 103,  403 => 102,  399 => 100,  384 => 99,  380 => 98,  376 => 96,  372 => 95,  365 => 94,  362 => 93,  353 => 92,  348 => 91,  342 => 90,  338 => 89,  334 => 88,  328 => 85,  323 => 82,  318 => 80,  311 => 79,  308 => 78,  303 => 77,  299 => 76,  294 => 74,  290 => 72,  285 => 71,  281 => 70,  277 => 69,  271 => 68,  266 => 65,  262 => 64,  257 => 63,  251 => 60,  243 => 59,  235 => 55,  232 => 54,  230 => 53,  226 => 52,  222 => 51,  218 => 49,  204 => 47,  200 => 45,  194 => 42,  181 => 41,  178 => 40,  165 => 39,  161 => 38,  155 => 36,  149 => 35,  146 => 34,  143 => 33,  140 => 32,  137 => 31,  134 => 30,  132 => 29,  128 => 28,  120 => 27,  116 => 25,  103 => 24,  99 => 23,  96 => 22,  91 => 20,  88 => 19,  83 => 18,  78 => 17,  75 => 16,  73 => 15,  69 => 14,  63 => 12,  60 => 11,  57 => 10,  55 => 9,  51 => 8,  47 => 7,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "kofi/template/new_elements/product.twig", "/home/prod85fb/public_html/catalog/view/theme/kofi/template/new_elements/product.twig");
    }
}
