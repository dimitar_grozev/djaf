<?php
// Heading
$_['heading_title']      = 'Cash or Card in Store';

// Text
$_['text_extension']     = 'Extensions';
$_['text_success']       = 'Success: You have modified Cash or Card in Store payment module!';
$_['text_edit']          = 'Edit Cash or Card in Store';

// Entry
$_['entry_total']        = 'Total';
$_['entry_order_status'] = 'Order Status';
$_['entry_geo_zone']     = 'Geo Zone';
$_['entry_status']       = 'Status';
$_['entry_sort_order']   = 'Sort Order';

// Help
$_['help_total']         = 'The checkout total the order must reach before this payment method becomes active. Leave it blank, if it is not needed';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment Cash or Card in Store!';