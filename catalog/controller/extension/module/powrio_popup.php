<?php
class ControllerExtensionModulePowrioPopup extends Controller {
	public function index($setting) {
		$data['id'] = $setting['id'];
		return $this->load->view('extension/module/powrio_popup', $data);
	}
}
